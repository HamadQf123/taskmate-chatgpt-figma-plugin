import { Select, Text, Popover, LoadingOverlay, Button } from '@mantine/core'
import {} from '@mantine/core'
import React from 'react'
import { TabProps } from '.'
import { useGetProject } from '../../../shared/service'
import { statusOptions } from '../../../shared/data'
import AccordionTemplate from '../accordion'
import Row from './row'
import { DayPicker } from 'react-day-picker'
import 'react-day-picker/dist/style.css'
import { Icon } from '@iconify/react'
import { format } from 'date-fns'
import EditRow from './edit-row'

const Status = ({
  updateIssue,
  issue,
  loading,
  editAll,
  projectId,
}: TabProps) => {
  const {
    data,
    loading: usersLoading,
    error,
    refetch,
  } = useGetProject(projectId)

  return (
    <>
      <LoadingOverlay visible={usersLoading} />
      {!data && !usersLoading && error && (
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            gap: 7,
            height: '150px',
          }}
        >
          <Text size={'md'} color={'red'}>
            {error?.message}
          </Text>
          <Button variant="light" onClick={() => refetch()}>
            Retry
          </Button>
        </div>
      )}
      {data && !usersLoading && (
        <AccordionTemplate title="Issue Status Information">
          <Row title="Status" name="status" required info="Status">
            {(!issue?.__typename || editAll) && (
              <Select
                disabled={loading}
                onChange={(data) => {
                  updateIssue({
                    status: data,
                  })
                }}
                id="status"
                name="status"
                style={{
                  border: 'none',
                  borderRadius: 0,
                  display: 'block',
                  width: '100%',
                  height: '100%',
                }}
                defaultValue={issue?.status ?? 'New'}
                nothingFound={
                  <div
                    style={{
                      padding: '1rem',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text color={'gray'}>No data found</Text>
                  </div>
                }
                searchable
                placeholder="Select Issue status"
                data={statusOptions}
              />
            )}
            {issue?.__typename && !editAll && (
              <EditRow
                selectOptions={statusOptions}
                name="status"
                type="select"
                placeholder="Select Issue status"
                required
                initialValue={issue?.status}
                projectId={issue?.project?.id}
                issueId={issue?.id}
              />
            )}
          </Row>
          {data && (
            <Row title="Assigned To" name="assignee" required info="Assignee">
              {(!issue?.__typename || editAll) && (
                <Select
                  disabled={loading}
                  onChange={(data) => {
                    updateIssue({
                      assignee: { id: data },
                    })
                  }}
                  defaultValue={
                    data?.getProject?.access
                      ?.map((user: { user: { id: string; name: string } }) => {
                        return {
                          label: user.user?.name,
                          value: user.user?.id,
                        }
                      })
                      ?.find(
                        (option: { value: string; label: string }) =>
                          option.label === issue?.assignee?.id ||
                          option.value === issue?.assignee?.id
                      )?.value
                  }
                  id="assignee"
                  name="assignee"
                  style={{
                    border: 'none',
                    borderRadius: 0,
                    display: 'block',
                    width: '100%',
                    height: '100%',
                  }}
                  nothingFound={
                    <div
                      style={{
                        padding: '1rem',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}
                    >
                      <Text color={'gray'}>No data found</Text>
                    </div>
                  }
                  searchable
                  placeholder="Select Assignee"
                  data={data?.getProject?.access?.map(
                    (user: { user: { id: string; name: string } }) => {
                      return {
                        label: user.user?.name,
                        value: user.user?.id,
                      }
                    }
                  )}
                />
              )}
              {issue?.__typename && !editAll && (
                <EditRow
                  selectOptions={data?.getProject?.access?.map(
                    (user: { user: { id: string; name: string } }) => {
                      return {
                        label: user.user?.name,
                        value: user.user?.id,
                      }
                    }
                  )}
                  name="assignee"
                  type="select"
                  placeholder="Select Assignee"
                  required
                  initialValue={
                    data?.getProject?.access
                      ?.map((user: { user: { id: string; name: string } }) => {
                        return {
                          label: user.user?.name,
                          value: user.user?.id,
                        }
                      })
                      .find(
                        (user: { value: string; label: string }) =>
                          user.value === issue?.assignee?.id
                      )?.value ?? ''
                  }
                  projectId={issue?.project?.id}
                  issueId={issue?.id}
                />
              )}
            </Row>
          )}
          <Row title="Date Started" name="started" info="Start date">
            <div
              style={{
                padding: !issue?.__typename || editAll ? '0 7px' : '0',
                position: 'relative',
              }}
            >
              {(!issue?.__typename || editAll) && (
                <Popover
                  width={'100%'}
                  position="bottom"
                  withArrow
                  shadow="md"
                  disabled={loading}
                >
                  <Popover.Target>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: issue?.started
                          ? 'space-between'
                          : 'flex-end',
                        alignItems: 'center',
                        width: '100%',
                        height: '100%',
                      }}
                    >
                      {issue?.started && (
                        <span>
                          {format(new Date(issue?.started ?? ''), 'dd-MM-yyyy')}
                        </span>
                      )}
                      <Icon icon="ph:calendar" />
                    </div>
                  </Popover.Target>
                  <Popover.Dropdown
                    style={{
                      width: '100%',
                    }}
                  >
                    <div
                      style={{
                        position: 'relative',
                        width: '100%',
                        cursor: 'pointer',
                        display: 'flex',
                        justifyContent: issue?.started
                          ? 'space-between'
                          : 'flex-end',
                      }}
                    >
                      <DayPicker
                        mode="single"
                        selected={issue?.started ?? null}
                        onSelect={(selected) => {
                          updateIssue({
                            started: selected,
                          })
                        }}
                      />
                    </div>
                  </Popover.Dropdown>
                </Popover>
              )}
              {issue?.__typename && !editAll && (
                <EditRow
                  name="started"
                  type="date"
                  placeholder="Select start date"
                  initialValue={issue.started}
                  projectId={issue?.project?.id}
                  issueId={issue?.id}
                />
              )}
            </div>
          </Row>
          <Row
            title="Date Completed"
            name="completed"
            isLastChild
            info="Date completed"
          >
            <div
              style={{
                padding: !issue?.__typename || editAll ? '0 7px' : '0',
                position: 'relative',
              }}
            >
              {(!issue?.__typename || editAll) && (
                <Popover
                  width={'100%'}
                  position="bottom"
                  withArrow
                  shadow="md"
                  disabled={loading}
                >
                  <Popover.Target>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: issue?.completedAt
                          ? 'space-between'
                          : 'flex-end',
                        alignItems: 'center',
                        width: '100%',
                        height: '100%',
                      }}
                    >
                      {issue?.completedAt && (
                        <span>
                          {format(
                            new Date(issue?.completedAt ?? ''),
                            'dd-MM-yyyy'
                          )}
                        </span>
                      )}
                      <Icon icon="ph:calendar" />
                    </div>
                  </Popover.Target>
                  <Popover.Dropdown
                    style={{
                      width: '100%',
                    }}
                  >
                    <div
                      style={{
                        position: 'relative',
                        width: '100%',
                        cursor: 'pointer',
                      }}
                    >
                      <DayPicker
                        mode="single"
                        selected={issue?.completedAt ?? null}
                        onSelect={(selected) => {
                          updateIssue({
                            completedAt: selected,
                          })
                        }}
                      />
                    </div>
                  </Popover.Dropdown>
                </Popover>
              )}
              {issue?.__typename && !editAll && (
                <EditRow
                  name="completedAt"
                  type="date"
                  placeholder="Select complete date"
                  initialValue={issue.completedAt}
                  projectId={issue?.project?.id}
                  issueId={issue?.id}
                />
              )}
            </div>
          </Row>
        </AccordionTemplate>
      )}
    </>
  )
}
export default Status
