import PasswordValidator from 'password-validator'

export const passwordRequirements = new PasswordValidator()
export const usernameRequirements = new PasswordValidator()

passwordRequirements
  .is()
  .min(8)
  .has()
  .uppercase()
  .has()
  .lowercase()
  .has()
  .digits()
  .has()
  .symbols()

  usernameRequirements.has().not().spaces() 
export const capitalizeSentence = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1)
}
