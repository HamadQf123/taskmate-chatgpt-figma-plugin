import React, {
  Dispatch,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from 'react'
import { StateProviderProps } from '../types'

const ProjectIdContext = createContext(
  {} as {
    projectId: string
    setProjectId: Dispatch<SetStateAction<string>>
  }
)

const ProjectIdProvider = ({ children }: StateProviderProps) => {
  const [projectId, setProjectId] = useState<string>('')

  return (
    <ProjectIdContext.Provider value={{ projectId, setProjectId }}>
      {children}
    </ProjectIdContext.Provider>
  )
}

export default ProjectIdProvider
export const useProjectIdContext = () => useContext(ProjectIdContext)
