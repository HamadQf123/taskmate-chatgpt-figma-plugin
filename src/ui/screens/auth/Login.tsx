import { useLazyQuery } from '@apollo/client'
import {
  TextInput,
  Button,
  Group,
  Paper,
  PasswordInput,
  Text,
  Anchor,
  Notification,
  Space,
} from '@mantine/core'
import { useForm } from '@mantine/form'
import React, { useEffect } from 'react'
import { LOGIN } from '../../../graphql/queries'
import { passwordRequirements } from './../../../shared/utils'
import { AuthToken, LoginData, LoginInput } from './../../../shared/types'
import { useRoutingState } from './../../../shared/context/routing/RoutingState'
import { useAuthTokenState } from '../../../shared/context/authToken/AuthTokenState'
import { Icon } from '@iconify/react'

const Login = () => {
  const { dispatch: authTokenDispatch } = useAuthTokenState()
  const [executeQuery, { loading, data, error }] =
    useLazyQuery<LoginData>(LOGIN)
  const { dispatch } = useRoutingState()

  useEffect(() => {
    parent.postMessage(
      {
        pluginMessage: {
          type: 'resize-plugin',
          size: { width: 500, height: 400 },
        },
      },
      '*'
    )
  }, [])

  const handleSubmit = (login: LoginInput) => {
    executeQuery({ variables: { login } })
  }

  useEffect(() => {
    if (data) {
      const token: AuthToken = data.login

      authTokenDispatch({
        type: 'save_auth_token',
        payload: { tokenType: 'access', token: token },
      })

      dispatch({ type: 'routing', payload: { route: 'dashboard' } })
    }
  }, [data])

  const form = useForm({
    initialValues: {
      email: '',
      password: '',
    },

    validate: {
      email: (value) =>
        value.length < 2 ? 'Must have at least 2 letters' : null,
      password: (value) =>
        passwordRequirements.validate(value)
          ? null
          : 'Password must contain atleast 8 characters including a number, a capital letter, a small letter, and a special character',
    },
  })

  return (
    <Paper
      sx={{
        minHeight: 'calc(100% - 63px)',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        gap: '20px',
        padding: '2rem 15%',
      }}
    >
      <Text size={'lg'} fw={'bold'}>
        Welcome Back!
      </Text>

      {error && (
        <Notification
          icon={<Icon width={18} height={18} icon={'ph:x'} />}
          color="red"
          disallowClose
        >
          {error.message}
        </Notification>
      )}

      <form
        style={{
          width: '100%',
        }}
        onSubmit={form.onSubmit((values) => {
          handleSubmit(values)
        })}
      >
        <TextInput
          withAsterisk
          label="Email/Username"
          placeholder="Email/username"
          type={'text'}
          {...form.getInputProps('email')}
        />

        <Space h="sm" />

        <Group
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-end',
            justifyContent: 'center',
          }}
        >
          <PasswordInput
            withAsterisk
            label="Password"
            placeholder="Password"
            {...form.getInputProps('password')}
            style={{ width: '100%' }}
          />

          {/* <Anchor href="#">Forgot Password</Anchor> */}
        </Group>

        <Group position="center" mt="20px" style={{ flexDirection: 'column' }}>
          <Button type="submit" style={{ width: '50%' }} loading={loading}>
            Login
          </Button>
          <Text>
            Don't have an account?&nbsp;
            <Anchor
              href="#"
              onClick={() =>
                dispatch({ type: 'routing', payload: { route: 'register' } })
              }
            >
              Register
            </Anchor>
          </Text>
        </Group>
      </form>
    </Paper>
  )
}

export default Login
