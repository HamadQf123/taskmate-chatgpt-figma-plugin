import { Button, Stack, TextInput } from '@mantine/core'
import React, { useState } from 'react'

interface NewItemProps {
  name: 'New Category' | 'New Subcategory' | 'New Feature'
  loading: boolean
  dispatch: (value: string) => void
}

function NewItem({ name, loading, dispatch }: NewItemProps) {
  const [writingMode, setWritingMode] = useState(false)
  const [value, setValue] = useState('')

  return (
    <form
      style={{ width: '100%' }}
      onSubmit={(e) => {
        e.preventDefault()
        if (writingMode) {
          dispatch(value.trim())
          setWritingMode(false)
          setValue('')
          return
        }
        setWritingMode(true)
      }}
    >
      <Stack align="center">
        {writingMode && (
          <TextInput
            w={'100%'}
            placeholder={name}
            value={value}
            onChange={(e) => setValue(e.currentTarget.value)}
          />
        )}
        <Button
          variant="light"
          type="submit"
          loading={loading}
          disabled={(writingMode && value.length <= 0) || loading}
        >
          {writingMode ? 'Save' : name}
        </Button>
      </Stack>
    </form>
  )
}
export default NewItem
