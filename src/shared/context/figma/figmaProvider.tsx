import React, { Dispatch, createContext, useContext, useReducer } from 'react'
import {
  FigmaReducerActions,
  FigmaStateType,
  StateProviderProps,
} from './../../types'

const FigmaContext = createContext(
  {} as {
    state: { fileKey: string; fileName: string }
    dispatch: Dispatch<FigmaReducerActions>
  }
)

const figmaReducer = (state: FigmaStateType, action: FigmaReducerActions) => {
  const { type, payload } = action

  switch (type) {
    case 'ATTACH_FILE_KEY':
      return {
        ...state,
        fileKey: payload.fileKey,
      }
    case 'ATTACH_FILE_NAME':
      return {
        ...state,
        fileName: payload.fileName,
      }
    default:
      return state
  }
}

const FigmaProvider = ({ children }: StateProviderProps) => {
  const initialState = {
    fileKey: '',
    fileName: '',
  }
  const [state, dispatch] = useReducer(figmaReducer, initialState)

  return (
    <FigmaContext.Provider value={{ state, dispatch }}>
      {children}
    </FigmaContext.Provider>
  )
}
export default FigmaProvider

export const useFigmaContext = () => useContext(FigmaContext)
