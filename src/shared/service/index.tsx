import { useLazyQuery, useMutation, useQuery } from "@apollo/client";
import {
  GET_ISSUE,
  GET_ISSUES,
  GET_ME,
  GET_PROJECT,
  GET_PROJECTS,
  GET_USERS,
  GET_USER_STORY,
} from "../../graphql/queries";
import {
  ACCEPT_INVITE,
  CREATE_INVITE,
  CREATE_ISSUE,
  CREATE_PROJECT,
  DELETE_ISSUE,
  DELETE_PROJECT,
  EDIT_ISSUE,
  REJECT_INVITE,
  REMOVE_ACCESS,
  REVOKE_INVITE,
  UPDATE_PASSWORD,
  UPDATE_PROJECT,
  UPDATE_ROLE,
  UPDATE_USER,
} from "./../../graphql/mutations";

export const useMe = () => {
  const { data, loading, error, refetch } = useQuery(GET_ME);
  return {
    data,
    loading,
    error,
    refetch,
  };
};

export const useUpdateMe = () => {
  const [updateMe, { loading, data, error }] = useMutation(UPDATE_USER);

  return { updateMe, loading, data, error };
};

export const useUpdatePassword = () => {
  const [updatePassword, { loading, data, error }] =
    useMutation(UPDATE_PASSWORD);

  return { updatePassword, loading, data, error };
};

export const useCreateProject = () => {
  const [createProject, { loading, data, error }] = useMutation(CREATE_PROJECT);
  return {
    createProject,
    loading,
    error,
    data,
  };
};

export const useGetProjects = ({
  params,
}: {
  params: {
    page: number;
    pageSize: number;
    paging: true;
    search?: string;
    property?: string;
    direction?: "asc" | "desc";
  };
}) => {
  const { loading, data, error, refetch, fetchMore } = useQuery(GET_PROJECTS, {
    variables: { params },
    // notifyOnNetworkStatusChange: true,
    fetchPolicy: "network-only",
  });
  return { loading, data, error, refetch, fetchMore };
};

export const useGetProject = (projectId: string) => {
  const { loading, data, error, refetch } = useQuery(GET_PROJECT, {
    variables: { getProjectId: parseInt(projectId) },
    // notifyOnNetworkStatusChange: true,
    // fetchPolicy: 'network-only'
  });
  return { loading, data, error, refetch };
};

export const useLazyGetProject = (projectId: string) => {
  const [getProject, { loading, data, error, refetch }] = useLazyQuery(
    GET_PROJECT,
    {
      variables: { getProjectId: parseInt(projectId) },
    }
  );
  return { loading, data, error, refetch, getProject };
};

export const useUpdateProject = () => {
  const [updateProject, { loading, data, error }] = useMutation(UPDATE_PROJECT);

  return { updateProject, loading, data, error };
};

export const useDeleteProject = () => {
  const [deleteProject, { loading, data, error }] = useMutation(DELETE_PROJECT);
  return { deleteProject, loading, data, error };
};

export const useDeleteIssue = () => {
  const [deleteIssue, { loading, data, error }] = useMutation(DELETE_ISSUE);
  return { deleteIssue, loading, data, error };
};

export const useCreateIssue = () => {
  const [createIssue, { loading, data, error }] = useMutation(CREATE_ISSUE);
  return {
    createIssue,
    loading,
    error,
    data,
  };
};

export const useEditIssue = () => {
  const [editIssue, { loading, data, error }] = useMutation(EDIT_ISSUE);
  return {
    editIssue,
    loading,
    error,
    data,
  };
};

export const useGetIssues = ({
  params,
}: {
  params: {
    page: number;
    pageSize: number;
    paging: true;
    id: string;
    search?: string;
    property?: string;
    direction?: "asc" | "desc";
    filter?: any;
  };
}) => {
  const { loading, data, error, refetch } = useQuery(GET_ISSUES, {
    variables: {
      params,
    },
    notifyOnNetworkStatusChange: true,
    fetchPolicy: "network-only",
  });

  return {
    loading,
    data,
    error,
    refetch,
  };
};

export const useGetIssue = (getIssueId: any) => {
  const { loading, data, error, refetch } = useQuery(GET_ISSUE, {
    variables: {
      getIssueId,
    },
    skip: !getIssueId,
    fetchPolicy: 'network-only'
    // notifyOnNetworkStatusChange: true,
  });

  return {
    loading,
    data,
    error,
    refetch,
  };
};

export const useGetUsers = () => {
  const [getUsers, { loading, data, error, refetch }] = useLazyQuery(
    GET_USERS
    // { notifyOnNetworkStatusChange: true }
  );

  return {
    getUsers,
    loading,
    data,
    error,
    refetch,
  };
};

export const useCreateInvite = () => {
  const [createInvite, { loading, data, error }] = useMutation(CREATE_INVITE);
  return {
    createInvite,
    loading,
    data,
    error,
  };
};

export const useRevokeInvite = () => {
  const [revokeInvite, { loading, data, error }] = useMutation(REVOKE_INVITE);
  return {
    revokeInvite,
    loading,
    data,
    error,
  };
};

export const useAcceptInvite = () => {
  const [acceptInvite, { loading, data, error }] = useMutation(ACCEPT_INVITE);
  return {
    acceptInvite,
    loading,
    data,
    error,
  };
};

export const useRejectInvite = () => {
  const [rejectInvite, { loading, data, error }] = useMutation(REJECT_INVITE);
  return {
    rejectInvite,
    loading,
    data,
    error,
  };
};

export const useUpdateUserRole = () => {
  const [updateRole, { loading, data, error }] = useMutation(UPDATE_ROLE);
  return {
    updateRole,
    loading,
    data,
    error,
  };
};

export const useRevokeAccess = () => {
  const [revokeAccess, { loading, data, error }] = useMutation(REMOVE_ACCESS);
  return {
    revokeAccess,
    loading,
    data,
    error,
  };
};

export const useGetUserStory = () => {
  const [loadStory, { loading, error, data, refetch }] = useLazyQuery(
    GET_USER_STORY,
    {
      notifyOnNetworkStatusChange: true,
      fetchPolicy: "network-only",
    }
  );
  return {
    loadStory,
    loading,
    error,
    data,
    refetch,
  };
};
