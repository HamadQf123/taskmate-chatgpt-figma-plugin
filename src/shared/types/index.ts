import { ReactNode } from "react";

export type { IssueTypesType, IssueOptionType, IssueType } from "./issues";

export type {
  LoginInput,
  LoginData,
  LoginResponse,
  RegisterInput,
  AuthToken,
  AuthTokenStateType,
  AuthTokenAction,
  AuthTokenContextType,
} from "./auth";

export type {
  Route,
  RoutingAction,
  RoutingContextType,
  RouteStateType,
} from "./routing";

export type {
  FigmaReducerActions,
  FigmaStateType,
  SelectedNodeType,
  ElementNameType,
} from "./figma";

export interface StateProviderProps {
  children: ReactNode;
}

type Crumb = {
  id: string;
  name: string;
  icon?: ReactNode;
};

export interface BreadcrumbPros {
  crumbs: Crumb[];
}

export interface ProjectType {
  description: string;
  name: string;
  type: string;
}

export interface ProjectInfoType {
  id: string;
  desc: string;
}

export interface ProjectQueryReturn {
  created: Date;
  createdBy: { name: string };
  id: string;
  name: string;
  description?: string;
}

export interface ProjectRecordType {
  created: Date;
  createdBy: string;
  id: string;
  name: string;
}

export interface ProjectScreenProps extends ProjectType {
  id: string;
}

export interface TheProject {
  id: string;
  name: string;
  description: string;
}

export interface ProjectFormProps {
  project?: TheProject;
  close?: (data?: TheProject) => void;
  done?: () => void;
  refetch?: () => void;
}

export interface TabContent {
  value: string;
  title: string;
  icon?: string;
  content: ReactNode;
}

export interface TabActionsStateType {
  issues: ReactNode | undefined;
  users: ReactNode | undefined;
}

export interface TabReducerActions {
  type: "ATTACH_ISSUE" | "ATTACH_USER";
  payload: TabActionsStateType;
}

export type Item = {
  title: string;
  id: string;
};
