import { Textarea, Select } from "@mantine/core";
import { MultiSelect } from "@mantine/core";
import React from "react";
import { TabProps } from ".";
import AccordionTemplate from "../accordion";
import EditRow from "./edit-row";
import Row from "./row";

const Notes = ({ updateIssue, loading, issue, editAll }: TabProps) => {
  return (
    <>
      <AccordionTemplate title="Issue Notes">
        <Row title="Notes" name="notes" isLastChild info="Issue notes">
          {(!issue?.__typename || editAll) && (
            <Textarea
              disabled={loading}
              name="notes"
              id="notes"
              value={issue?.notes ?? ""}
              style={{ backgroundColor: "red" }}
              placeholder="Notes"
              onChange={(e) => {
                updateIssue({
                  notes: e.target.value,
                });
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="notes"
              type="textarea"
              placeholder="notes"
              initialValue={issue?.notes}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
      </AccordionTemplate>
      <AccordionTemplate title="User Story Persona">
        <Row title="Persona" name="persona" required info="Persona">
          {(!issue?.__typename || editAll) && (
            <MultiSelect
              disabled={loading}
              name="persona"
              id="persona"
              value={issue?.persona ? issue?.persona.split(", ") : ""}
              nothingFound={
                <div
                  style={{
                    padding: "1rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                ></div>
              }
              searchable
              placeholder="Select Persona"
              data={["Basic User", "Sales User", "Cutsomer", "Administrator"]}
              onChange={(value) => {
                updateIssue({
                  persona: value.join(", ")
                });
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="persona"
              type="select"
              placeholder="Persona"
              required
              initialValue={issue?.persona}
              selectOptions={[
                { label: "Basic User", value: "Basic User" },
                { label: "Sales User", value: "Sales User" },
                { label: "Customer", value: "Customer" },
                { label: "Administrator", value: "Administrator" },
              ]}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
      </AccordionTemplate>
    </>
  );
};
export default Notes;
