import { Icon } from "@iconify/react";
import {
  Text,
  Anchor,
  ActionIcon,
  TextInput,
  Group,
  Loader,
} from "@mantine/core";
import React, { useState, useEffect } from "react";
import { showNotification } from "@mantine/notifications";
import { useMe, useUpdateMe, useUpdatePassword } from "../../../shared/service";
import Row from "../issue/row";

const EditProfile = () => {
  return (
    <div>
      <Row title="Name" name={"name"}>
        <ProfileItem type="name" />
      </Row>
      <Row title="Username" name={"username"}>
        <ProfileItem type="username" />
      </Row>
      <Row title="Email" name={"email"}>
        <ProfileItem type="email" />
      </Row>
      <Row title="Password" name={"password"} isLastChild>
        <ProfileItem type="password" />
      </Row>
    </div>
  );
};
export default EditProfile;

const ProfileItem = ({
  type,
}: {
  type: "name" | "username" | "password" | "email";
}) => {
  const { data: me, refetch } = useMe();
  const [value, setValue] = useState(me?.me[type] ?? "");
  const [currentPassword, setCurrentPassword] = useState("");
  const { data, loading, error, updateMe } = useUpdateMe();
  const {
    data: passwordData,
    loading: passwordLoading,
    error: passwordError,
    updatePassword,
  } = useUpdatePassword();
  const [edit, setEdit] = useState(false);

  const submitEdits = () => {
    if (type !== "password")
      updateMe({
        variables: {
          user: {
            [type]: value,
            ...(type === "email"
              ? {
                  password: currentPassword,
                }
              : {}),
          },
        },
      });
    if (type === "password")
      updatePassword({
        variables: {
          user: {
            oldPassword: currentPassword,
            newPassword: value,
          },
        },
      });
  };

  useEffect(() => {
    if (data) {
      refetch();
      showNotification({
        message: "Successfully update the profile",
        color: "green",
      });
      setValue("");
      setCurrentPassword("");
      setEdit(false);
    }
    if (error) {
      showNotification({
        title: "Failed to update profile",
        message: error.message,
        color: "red",
      });
    }
  }, [data, error]);

  useEffect(() => {
    if (passwordData) {
      refetch();
      showNotification({
        message: "Successfully update your password",
        color: "green",
      });
      setValue("");
      setCurrentPassword("");
      setEdit(false);
    }
    if (passwordError) {
      showNotification({
        title: "Failed to update your password",
        message: passwordError.message,
        color: "red",
      });
    }
  }, [passwordData, passwordError]);

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        submitEdits();
      }}
      className="issue-row"
      style={{
        display: "flex",
        justifyContent: "space-between",
        height: "100%",
        width: "100%",
        gap: 7,
      }}
    >
      {!edit && (
        <div
          style={{
            padding: 7,
            width: "100%",
          }}
        >
          {(type === "name" || type === "username") && (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                gap: 7,
                width: "100%",
              }}
            >
              <Text>{me?.me[type]}</Text>
              <ActionIcon onClick={() => setEdit(true)} className="link">
                <Icon
                  icon="ph:note-pencil"
                  height="14px"
                  width="14px"
                  className="link"
                />
              </ActionIcon>
            </div>
          )}
          {(type === "email" || type === "password") && (
            <div>
              <Anchor onClick={() => setEdit(true)}>Change?</Anchor>
            </div>
          )}
        </div>
      )}
      {edit && (
        <>
          {/* edit field */}
          {(type === "name" || type === "username") && (
            <div>
              <TextInput
                disabled={loading}
                name={type}
                required
                id={type}
                value={value ?? me?.me[type] ?? ""}
                autoFocus
                style={{
                  borderRadius: 0,
                  display: "block",
                  width: "100%",
                  height: "100%",
                }}
                placeholder={
                  type === "name" ? "Enter new name" : "Enter new username"
                }
                onChange={(e) => {
                  setValue(e.target.value);
                }}
              />
            </div>
          )}
          {(type === "email" || type === "password") && (
            <div>
              <TextInput
                disabled={loading || passwordLoading}
                name={type}
                required
                id={type}
                value={value}
                autoFocus
                type={type}
                style={{
                  borderRadius: 0,
                  display: "block",
                  width: "100%",
                  height: "calc( 50% - 3px )",
                  marginBottom: 5,
                }}
                placeholder={
                  type === "email"
                    ? "Enter your new email"
                    : "Enter your new password"
                }
                onChange={(e) => {
                  setValue(e.target.value);
                }}
              />
              <TextInput
                disabled={loading || passwordLoading}
                name={type}
                required
                id={type}
                value={currentPassword}
                autoFocus
                type={"password"}
                style={{
                  borderRadius: 0,
                  display: "block",
                  width: "100%",
                  height: "calc( 50% - 3px )",
                }}
                placeholder={
                  type === "email"
                    ? "Enter your password"
                    : "Enter your current password"
                }
                onChange={(e) => {
                  setCurrentPassword(e.target.value);
                }}
              />
            </div>
          )}
          <Group
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexWrap: "nowrap",
              gap: 5,
              paddingRight: 7,
            }}
          >
            {(loading || passwordLoading) && <Loader size={16} />}
            {!loading && !passwordLoading && (
              <>
                <ActionIcon
                  variant="light"
                  size="sm"
                  color="blue"
                  onClick={() => setEdit(false)}
                  title="Close"
                  className="danger"
                >
                  <Icon
                    icon="ph:x"
                    height="14px"
                    width="14px"
                    className="danger"
                  />
                </ActionIcon>
                <ActionIcon
                  variant="light"
                  size="sm"
                  type="submit"
                  color="green"
                  title="Save"
                  className="link"
                >
                  <Icon
                    icon="ph:check"
                    height="14px"
                    width="14px"
                    className="link"
                  />
                </ActionIcon>
              </>
            )}
          </Group>
        </>
      )}
    </form>
  );
};
