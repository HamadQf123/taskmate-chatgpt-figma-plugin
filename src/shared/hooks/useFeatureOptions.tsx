import { useGetFeatures } from './../service/features'
import { useProjectIdContext } from './../context/ProjectIdProvider'
import { useEffect, useState } from 'react'

const useFeatureOptions = () => {
  const { projectId } = useProjectIdContext()
  const {
    loading: loadingFeatures,
    data: features,
    error: featuresError,
  } = useGetFeatures({
    params: {
      id: parseInt(projectId),
    },
  })

  const [featureOptions, setFeatureOptions] = useState<
    Array<{ value: string; label: string }>
  >([])

  useEffect(() => {
    if (features && features.getFeatures.length > 0) {
      setFeatureOptions([]);
      features.getFeatures.forEach((feature: { id: string; name: string }) => {
        setFeatureOptions((prev) => [
          ...prev,
          { value: feature.id, label: feature.name },
        ])
      })
    }
  }, [features])

  return {
    featuresError,
    loadingFeatures,
    featureOptions,
  }
}

export default useFeatureOptions
