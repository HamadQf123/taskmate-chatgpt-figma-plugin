// export const API = 'https://dash.flexcodelabs.com/graphql'
export const API = 'https://taskmate.life/graphql'
export const API_DOWNLOAD = 'https://taskmate.life/api/downloads'
// export const API = "http://localhost:3000/graphql";
// export const API_DOWNLOAD = "http://localhost:3000/api/downloads";
export const logout = async (refreshToken: string) => {
  try {
    const body = JSON.stringify({
      query: `mutation Logout {
        logout {
          message
        }
      }`,
      variables: {},
    });

    const response = await fetch(`${API}`, {
      method: "post",
      body: body,
      headers: {
        "Content-Type": "application/json",
        authorization: "Bearer " + refreshToken,
      },
    });
    const { data } = await response.json();
    return data;
  } catch (error: any) {
    return "";
  }
};

export const getRefreshToken = async (refreshToken: string) => {
  // graphql request to fetch new accessToken using refresh token
  try {
    const body = JSON.stringify({
      query: `mutation Refresh {
        refresh {
          accessToken
          refreshToken
        }
      }`,
      variables: {},
    });

    const response = await fetch(`${API}`, {
      method: "post",
      body: body,
      headers: {
        "Content-Type": "application/json",
        authorization: "Bearer " + refreshToken,
      },
    });

    const { data } = await response.json();
    return data.refresh.accessToken;
  } catch {
    return "";
  }
};
