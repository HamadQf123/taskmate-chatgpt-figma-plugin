import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import { NotificationsProvider } from "@mantine/notifications";
import AuthTokenState from "./../shared/context/authToken/AuthTokenState";
import ApolloProvider from "./../shared/context/apollo/ApolloProvider";
import RoutingState from "../shared/context/routing/RoutingState";
import FigmaProvider from "../shared/context/figma/figmaProvider";
import TabActionsProvider from "../shared/context/tabActions/TabActionsProvider";
import { MantineProvider } from "@mantine/core";
import ProjectIdProvider from "../shared/context/ProjectIdProvider";
import IssueIdProvider from "../shared/context/IssueIdProvider";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <MantineProvider
      theme={{
        colorScheme: "dark",
        colors: {
          darkCustom: ["#44474B", "#454545", "#1F1F1F", "#3D3D3D"],
          border: ["#707070"],
          primary: ["#8277FF"],
          blueCustom: ["#0082FF"],
          white: ["#ffffff"],
          transparent: ["#ffffff00"],
        },
        globalStyles: (theme) => ({
          "body, .mantine-Container-root ": {
            background: "#1F1F1F",
            "--dark-100": "#44474B",
            "--dark-200": "#454545",
            "--dark-300": "#1F1F1F",
            "--dark-400": "#3D3D3D",
          },
          ".dark-secondary-bg": {
            background: "#3D3D3D",
          },
          "input, .mantine-PasswordInput-input, textarea": {
            background: "#3D3D3D !important",
            backgroundColor: "#3D3D3D !important",
            // border: "none",
            // borderWidth: "0",
            color: "white !important",
          },
          "input:focus, textarea:focus": {
            borderColor: "#8277FF !important",
          },
          ".mantine-PasswordInput-input:focus-within": {
            border: "1px solid #8277FF !important",
          },
          ".mantine-PasswordInput-input input:focus": {
            border: "none !important",
          },
          ".mantine-PasswordInput-input": {
            paddingLeft: 0,
          },
          "input::placeholder": {
            color: "#707070 !important",
          },
          "a, .link": {
            color: "#8277FF !important",
          },
          ".success": {
            color: "rgb(178, 242, 187) !important",
          },
          ".danger": {
            color: "rgb(255, 201, 201) !important",
          },
          ".red": {
            color: "rgb(224, 49, 49) !important",
          },
          ".light": {
            color: "rgb(193, 194, 197) !important",
          },
          "p, h1, h2, h3, h4, h5, h6, li, span, label, svg, div": {
            color: "#ffffff !important",
          },
          ".mantine-InputWrapper-error, .mantine-TextInput-required": {
            color: "rgb(224, 49, 49) !important",
          },
          "button.mantine-UnstyledButton-root, button.mantine-Pagination-item[data-active]":
            {
              background: "#8277FF",
              backgroundColor: "#8277FF !important",
            },
          "button.mantine-UnstyledButton-root:focus": {
            outline: "none !important",
          },

          "button.mantine-Tabs-tab[data-active]": {
            borderColor: "#8277FF !important",
          },
          "button.danger": {
            background: "rgb(224, 49, 49)",
            backgroundColor: "rgb(224, 49, 49) !important",
          },
          "button.light": {
            backgroundColor: "rgba(224, 49, 49, 0.2) !important",
            color: "rgb(255, 201, 201) !important",
          },
          "button.light span": {
            color: "rgb(255, 201, 201) !important",
          },
          "button.cancel": {
            background: "transparent",
            backgroundColor: "rgb(224, 49, 49, 0) !important",
            color: "#8277FF !important",
            borderColor: "#8277FF !important",
          },

          "button.cancel span": {
            color: "#8277FF !important",
          },
          "button.mantine-UnstyledButton-root:hover, button.mantine-Pagination-item[data-active]:hover":
            {
              background: "#8277FF !important",
              opacity: ".8",
            },
          "button.cancel:hover": {
            background: "transparent",
            backgroundColor: "rgb(224, 49, 49, 0) !important",
            color: "#8277FF !important",
            borderColor: "#8277FF !important",
          },
          "button.light:hover": {
            backgroundColor: "rgba(224, 49, 49, 0.2) !important",
            background: "rgba(224, 49, 49, 0.2) !important",
          },
          "button.danger:hover": {
            background: "rgb(224, 49, 49)",
            backgroundColor: "rgb(224, 49, 49) !important",
          },
          "button.mantine-ActionIcon-root, button.mantine-Tabs-tab": {
            background: "transparent",
            backgroundColor: "rgba(0,0,0,0) !important",
          },
          "button.mantine-ActionIcon-root:hover, button.mantine-Tabs-tab:hover":
            {
              background: "transparent !important",
            },
          "button.mantine-Accordion-control, button.mantine-Accordion-control:hover":
            {
              background: "#44474B !important",
              backgroundColor: "#44474B !important",
            },
          "button:disabled": {
            opacity: ".7 !important",
          },
          "thead tr": {
            background: "#353535 !important",
          },
          "tbody tr": {
            background: "transparent",
            backgroundColor: "rgba(0,0,0,0) !important",
          },
          ".issue-table tbody tr:nth-child(1)": {
            background: "#3D3D3D",
            backgroundColor: "#3D3D3D !important",
          },
          ".issue-table tbody tr:nth-child(1) td:not(:first-of-type)": {
            borderColor: "#353535",
          },
          ".issue-table tbody tr:nth-child(1) input": {
            background: "#353535 !important",
          },
          ".no-data": {
            color: "transparent !important",
          },
        }),
      }}
    >
      <AuthTokenState>
        <ApolloProvider>
          <NotificationsProvider>
            <RoutingState>
              <FigmaProvider>
                <TabActionsProvider>
                  <ProjectIdProvider>
                    <IssueIdProvider>
                      <App />
                    </IssueIdProvider>
                  </ProjectIdProvider>
                </TabActionsProvider>
              </FigmaProvider>
            </RoutingState>
          </NotificationsProvider>
        </ApolloProvider>
      </AuthTokenState>
    </MantineProvider>
  </React.StrictMode>
);
