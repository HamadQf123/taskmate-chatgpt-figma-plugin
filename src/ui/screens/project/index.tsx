import React, { useState } from "react";
import { useRoutingState } from "../../../shared/context/routing/RoutingState";
import {
  Modal,
  Paper,
  Button,
  Text,
  LoadingOverlay,
  Anchor,
} from "@mantine/core";
import Banner from "../../components/banner";
import { TabContent } from "../../../shared/types";
import DeleteProject from "../../components/deleteProject";
import ProjectForm from "../../components/forms/ProjectForm";
import TabsTemplate from "../../components/tabs";
import { useGetProject } from "../../../shared/service";
import Users from "../../components/users";
import IssuesTable from "../../components/table/IssuesTable";
import { useDisclosure } from "@mantine/hooks";
import CategorySubcategoryFeature from "../../components/category-subcategory-feature";
import { useProjectIdContext } from "../../../shared/context/ProjectIdProvider";
import useMenu from "./useMenu";

const Project = () => {
  const { dispatch } = useRoutingState();
  const { projectId } = useProjectIdContext();
  const [opened, { open, close }] = useDisclosure(false);

  // get project
  const { data, loading, error, refetch } = useGetProject(projectId);

  const {
    menu,
    defaultValue,
    projectToEdit,
    setProjectToEdit,
    projectToDelete,
    setProjectToDelete,
  } = useMenu(data, open);

  const [desc, setDesc] = useState(false);

  const tabs: TabContent[] = [
    {
      value: "issues",
      title: "Issues",
      content: (
        <IssuesTable
          id={data?.getProject?.id}
          desc={data?.getProject?.description}
        />
      ),
    },
    {
      value: "users",
      title: "Users",
      content: <Users projectId={data?.getProject?.id} />,
    },
  ];

  return (
    <div>
      <LoadingOverlay visible={loading} />
      <Modal
        opened={!!projectToEdit}
        onClose={() => setProjectToEdit(undefined)}
        title="Edit Project"
      >
        <ProjectForm
          project={projectToEdit}
          close={() => {
            setProjectToEdit(undefined);
          }}
          refetch={refetch}
        />
      </Modal>

      <Modal
        opened={!!projectToDelete}
        onClose={() => setProjectToDelete(undefined)}
        title="Delete Project"
      >
        <DeleteProject
          id={projectToDelete?.id ?? ""}
          close={(success) => {
            setProjectToDelete(undefined);
            if (success)
              dispatch({ type: "routing", payload: { route: "dashboard" } });
          }}
          refetch={refetch}
        />
      </Modal>

      <Modal opened={opened} onClose={close} title="Project Settings">
        <CategorySubcategoryFeature defaultValue={defaultValue} />
      </Modal>

      {!data && !loading && error && (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: 7,
            height: "calc( 100% - 150px )",
          }}
        >
          <Text size={"md"} color={"red"}>
            {error?.message}
          </Text>
          <Button variant="light" onClick={() => refetch()}>
            Retry
          </Button>
        </div>
      )}

      {!loading && data && (
        <>
          <Banner
            actions={
              <div
                style={{
                  display: "flex",
                  justifyContent: "flex-end",
                  gap: "10px",
                }}
              >
                {menu}
              </div>
            }
            align={desc ? "flex-start" : "center"}
          >
            <div>
              <Text weight={"bold"}>{data?.getProject?.name} </Text>
              <Text size="sm">
                {data?.getProject?.description?.length < 50 &&
                  data?.getProject?.description}

                {data?.getProject?.description?.length >= 50 && (
                  <>
                    {desc
                      ? data?.getProject?.description
                      : data?.getProject?.description?.slice(0, 50) +
                        "..."}{" "}
                    <Anchor href="#" onClick={() => setDesc(!desc)}>
                      {desc ? "Show less" : "Show more"}
                    </Anchor>
                  </>
                )}
              </Text>
            </div>
          </Banner>
          <Paper
            withBorder={false}
            p={"sm"}
            style={{
              margin: "10px",
            }}
            className="project-container"
          >
            <TabsTemplate tabs={tabs} />
          </Paper>
        </>
      )}
    </div>
  );
};
export default Project;
