import {
  Paper,
  Button,
  Group,
  Text,
  LoadingOverlay,
  Modal,
  Anchor,
  Textarea,
  Notification,
} from "@mantine/core";
import { showNotification } from "@mantine/notifications";
import React, { useEffect, useState } from "react";
import { useFigmaContext } from "../../../shared/context/figma/figmaProvider";
import { useRoutingState } from "../../../shared/context/routing/RoutingState";
import {
  useCreateIssue,
  useEditIssue,
  useGetIssue,
  useGetIssues,
  useGetProject,
  useMe,
} from "../../../shared/service";
import { TabContent } from "../../../shared/types";
import Banner from "../banner";
import DeleteIssue from "../deleteIssue";
import TabsTemplate from "../tabs";
import Details from "./details/details";
import Hierachy from "./hierachy";
import Notes from "./notes";
import Status from "./status";
import { useProjectIdContext } from "../../../shared/context/ProjectIdProvider";
import { useIssueIdContext } from "../../../shared/context/IssueIdProvider";
import useUserStory from "./useUserStory";
import { Icon } from "@iconify/react";
import {
  useCreateCategory,
  useGetCategories,
} from './../../../shared/service/categories';
import {
  useCreateSubCategory,
  useGetSubCategories
} from './../../../shared/service/subCategories';
import {
  useCreateFeature,
  useGetFeatures
} from './../../../shared/service/features';
import useCategoryOptions from '../../../shared/hooks/useCategoryOptions'
import useSubCategoryOptions from '../../../shared/hooks/useSubCategoryOptions'
import useFeatureOptions from '../../../shared/hooks/useFeatureOptions'



export type TabProps = {
  updateIssue: (value: any) => void;
  issue: any;
  loading: boolean;
  editAll: boolean;
  projectId: string;
};

const Issue = () => {
  const { dispatch } = useRoutingState();
  const { projectId } = useProjectIdContext();
  const { issueId } = useIssueIdContext();
  const { state: figmaState } = useFigmaContext();
  const { createCategory } = useCreateCategory()
  const { createSubCategory } = useCreateSubCategory()
  const { createFeature } = useCreateFeature()


  const {
    editIssue,
    loading: editLoading,
    data: editData,
    error: editError,
  } = useEditIssue();

  const {
    data: issueData,
    loading: issueLoading,
    error: issueError,
    refetch: refetchIssue,
  } = useGetIssue(issueId);

  const [issue, setIssue] = useState<any>(
    issueId
      ? issueId
      : {
          status: "New",
          file: figmaState?.fileKey ?? "",
        }
  );

  const {
    loading: loadingCategories,
    data: categories,
    error: categoriesError,
    refetch: refetchCategories,
  } = useGetCategories({
    params: {
      id: parseInt(projectId),
    },
  })

  const {
    loading: loadingSubCategories,
    data: subCategories,
    error: subCategoriesError,
    refetch: refetchSubCategories,
  } = useGetSubCategories({
    params: {
      id: parseInt(projectId),
    },
  })

  const {
    loading: loadingFeatures,
    data: Features,
    error: subFeaturesError,
    refetch: refetchFeatures,
  } = useGetFeatures({
    params: {
      id: parseInt(projectId),
    },
  })

  const [deleteModalOpen, setDeleteModalOpen] = useState(false);
  const [editAll, setEditAll] = useState(false);
  const { loading, createIssue, error, data } = useCreateIssue();

  const updateIssue = (values: any) => {
    setIssue({
      ...issue,
      ...values,
    });
  };

  const {
    // chatGTPLoading,
    error: gptError,
    chatAnswer,
    modal,
    setModal,
    storyloading,
    data: story,
    GetUserStory,
    ExtractDefinition,
    ExtractCategory,
    ExtractSubCategory,
    ExtractFeature,
    ExtractDescription,
  } = useUserStory(issue);

  const { categoryOptions } = useCategoryOptions();
  const { subCategoryOptions } = useSubCategoryOptions();
  const { featureOptions } = useFeatureOptions();


  const { data: projectData } = useGetProject(projectId);
  const { data: me } = useMe();

  let isAdmin = !!projectData?.getProject?.access?.find(
    (user: any) => user?.user?.id === me?.me?.id && user?.role === "ADMIN"
  );

  useEffect(() => {
    if (issueData?.getIssue) setIssue(issueData?.getIssue);
  }, [issueData]);

  const tabs: TabContent[] = [
    {
      value: "details",
      title: "Details",
      content: (
        <>
          {
            <Details
              updateIssue={updateIssue}
              loading={loading || editLoading}
              issue={issue}
              editAll={editAll}
              projectId={projectId}
            />
          }
        </>
      ),
    },
    {
      value: "notes",
      title: "Notes",
      content: (
        <Notes
          updateIssue={updateIssue}
          loading={loading || editLoading}
          issue={issue}
          editAll={editAll}
          projectId={projectId}
        />
      ),
    },
    {
      value: "hierachy",
      title: "Hierachy",
      content: (
        <Hierachy
          updateIssue={updateIssue}
          loading={loading || editLoading}
          issue={issue}
          editAll={editAll}
          projectId={projectId}
        />
      ),
    },
    {
      value: "status",
      title: "Status",
      content: (
        <Status
          updateIssue={updateIssue}
          loading={loading || editLoading}
          issue={issue}
          editAll={editAll}
          projectId={projectId}
        />
      ),
    },
  ];

  useEffect(() => {
    if (data) {
      showNotification({
        message: "Issue created successfully",
        color: "green",
      });
      setIssue({
        ...issue,
        ...data?.createIssue,
      });
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      showNotification({
        message: error.message,
        color: "red",
      });
    }
  }, [error]);

  useEffect(() => {
    if (editData) {
      // refetch();
      refetchIssue();
      showNotification({
        message: "Successfully update the issue",
        color: "green",
      });
      setEditAll(false);
    }
    if (editError) {
      showNotification({
        title: "Failed to update issue",
        message: editError.message,
        color: "red",
      });
    }
  }, [editData, editError]);

  const [desc, setDesc] = useState(false);

  return (
    <Paper
      style={{
        margin: "10px",
        display: "flex",
        flexDirection: "column",
        gap: 12,
      }}
      p={"sm"}
    >
      {/* buttons */}
      <LoadingOverlay visible={issueLoading} />
      {!issueData && !issueLoading && issueError && issueId && (
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "center",
            gap: 7,
            height: "calc( 100% - 150px )",
          }}
        >
          <Text size={"md"} color={"red"}>
            {issueError?.message}
          </Text>
          <Button variant="light" onClick={() => refetchIssue()}>
            Retry
          </Button>
        </div>
      )}
      <Modal
        opened={modal}
        onClose={() => {
          setModal(false);
        }}
        title="UserStory"
      >
        <LoadingOverlay visible={storyloading} />
        {gptError && (
          <Notification
            icon={<Icon width={18} height={18} icon={"ph:x"} />}
            color="red"
            disallowClose
            style={{
              marginBottom: ".3rem",
            }}
          >
            {gptError?.message}
          </Notification>
        )}
        <Textarea
          value={chatAnswer}
          name="User Story"
          id="Description"
          placeholder="Description"
          maxLength={10}
          autosize
        />
        <Group
          align={"flex-end"}
          style={{
            display: "flex",
            marginTop: "10px",
            justifyContent: "flex-end",
          }}
        >
          <Button
            variant="light"
            color={"red"}
            onClick={() => setModal(false)}
            className="cancel"
          >
            Cancel
          </Button>
          <Button
            variant="light"
            color={"blue"}
            onClick={GetUserStory}
            className="light"
          >
            Re-Analyze
          </Button>
          <Button
            variant="filled"
            color={"blue"}
            onClick={async() => {
              let descritption = ExtractDescription(chatAnswer);
              let definition = ExtractDefinition(chatAnswer);

              let category = ExtractCategory(chatAnswer);
              let name = "New Category";
              let categoryOption = categoryOptions;

              let categvalue = categoryOption?.find((option) => option.label === category)?.value;

              if (categvalue === undefined) {
                await createCategory({
                  variables: {
                    category: {
                      name: category,
                      project: projectId,
                    },
                  },
                }).then((result) => {
                  let createdCategory = result.data.createCategory;
                  let categoryId = createdCategory.id;
                  categvalue = categoryId;
                })
                .catch((error) => {
                  console.error(error);
                });
              }              

              let subcategory = ExtractSubCategory(chatAnswer);
              name = "New SubCategory";
              let subcategoryOption = subCategoryOptions;

              let subcategvalue = subcategoryOption?.find((option) => option.label === subcategory)?.value;

              if (subcategvalue === undefined) {
                await createSubCategory({
                  variables: {
                    subcategory: {
                      name: subcategory,
                      project: projectId,
                    },
                  },
                }).then((result) => {
                  let createdsubCategory = result.data.createSubCategory;
                  let subcategoryId = createdsubCategory.id;
                  subcategvalue = subcategoryId;
                })
                .catch((error) => {
                  console.error(error);
                });
              }
              
              let feature = ExtractFeature(chatAnswer);
              name = "New Feature";
              let featureOption = featureOptions;

              let featurevalue = featureOption?.find((option) => option.label === feature)?.value;

              if (featurevalue === undefined) {
                await createFeature({
                  variables: {
                    feature: {
                      name: feature,
                      project: projectId,
                    },
                  },
                }).then((result) => {
                  let createdfeature = result.data.createFeature;
                  let featureId = createdfeature.id;
                  featurevalue = featureId;
                })
                .catch((error) => {
                  console.error(error);
                });
              }

              setModal(false);

              await refetchCategories();
              await refetchSubCategories();
              await refetchFeatures();


              setIssue({
                ...issue,
                description: descritption,
                acceptanceCriteria: definition,
                category: {
                  id: categvalue,
                },
                subCategory: {
                  id: subcategvalue,
                },
                feature: {
                  id: featurevalue,
                },
              });
            }}
          >
            Save
          </Button>
        </Group>
      </Modal>
      <Modal
        opened={deleteModalOpen}
        onClose={() => {
          setDeleteModalOpen(false);
        }}
        tstle="Delete Issue"
      >
        <DeleteIssue
          id={issue?.id ?? data?.createIssue?.id ?? ""}
          // refetch={refetch}
          close={() => {
            setDeleteModalOpen(false);
            dispatch({
              type: "routing",
              payload: {
                route: "project",
              },
            });
          }}
        />
      </Modal>

      {data || issue?.__typename ? (
        <Banner
          iconCode="ph:clipboard-text-fill"
          iconColor={{ bg: "rgb(146 64 14)", color: "rgb(254 243 199)" }}
          margin={0}
          actions={
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                gap: "10px",
              }}
            >
              {editAll && (
                <>
                  <Button
                    variant="light"
                    color={"red"}
                    className="cancel"
                    disabled={editLoading}
                    onClick={() => setEditAll(false)}
                  >
                    Cancel
                  </Button>
                  <Button
                    variant="light"
                    disabled={
                      editLoading ||
                      !issue?.elementName ||
                      !issue?.type ||
                      !issue?.label ||
                      !issue?.status ||
                      !issue?.description ||
                      !issue?.acceptanceCriteria ||
                      !issue?.assignee ||
                      !issue?.category ||
                      !issue?.subCategory ||
                      !issue?.feature
                    }
                    loading={editLoading}
                    onClick={() => {
                      // do the edit thing
                      editIssue({
                        variables: {
                          issue: {
                            elementName: issue?.elementName?.map(
                              (element: any) => {
                                const { __typename, ...rest } = element;
                                return {
                                  ...rest,
                                };
                              }
                            ),
                            label: issue?.label,
                            assignee: {
                              id: issue?.assignee?.id,
                            },
                            ...(issue?.children
                              ? {}
                              : { children: issue?.children }),
                            acceptanceCriteria: issue?.acceptanceCriteria,
                            type: issue?.type,
                            description: issue?.description,
                            ...(issue?.externalLink
                              ? { externalLink: issue?.externalLink }
                              : {}),
                            ...(issue?.associatedCr
                              ? { associatedCr: issue?.associatedCr }
                              : {}),
                            ...(issue?.notes ? { notes: issue?.notes } : {}),
                            ...(issue?.persona ? { persona: issue?.persona } : {}),
                            category: {
                              id: issue?.category?.id,
                            },
                            subCategory: {
                              id: issue?.subCategory?.id,
                            },
                            feature: {
                              id: issue?.feature?.id,
                            },
                            status: issue?.status,
                            started: issue?.started,
                            completedAt: issue?.completedAt,
                          },
                          updateIssueId: issue?.id,
                        },
                      });
                    }}
                  >
                    Save
                  </Button>
                </>
              )}
              {!editAll && (
                <>
                  {/* <Button
                    variant="light"
                    className="light"
                    onClick={() => setEditAll(true)}
                  >
                    Edit
                  </Button> */}
                  {isAdmin && (
                    <Button
                      variant="light"
                      className="danger"
                      color="red"
                      onClick={() => setDeleteModalOpen(true)}
                    >
                      Delete
                    </Button>
                  )}
                </>
              )}
            </div>
          }
        >
          <div>
            <Text weight={"bold"}>{issue?.id}</Text>
            <Text size="sm">
              {issue?.description?.length < 50 && issue?.description}
              {issue?.description?.length >= 50 && (
                <>
                  {desc
                    ? issue?.description
                    : issue?.description?.slice(0, 50) + "...."}{" "}
                  <Anchor href="#" onClick={() => setDesc(!desc)}>
                    {desc ? "Show less" : "Show more"}
                  </Anchor>
                </>
              )}
            </Text>
          </div>
        </Banner>
      ) : (
        <>
          <Group
            align={"flex-end"}
            style={{
              display: "flex",
              justifyContent: "flex-end",
            }}
          >
            {!issueId && (
              <>
                <Button
                  variant="light"
                  color={"red"}
                  onClick={() =>
                    dispatch({
                      type: "routing",
                      payload: {
                        route: "project",
                      },
                    })
                  }
                  className="cancel"
                >
                  Cancel
                </Button>
                <Button
                  variant="light"
                  className="light"
                  loading={loading}
                  disabled={loading || !issue?.elementName || !issue?.notes || !issue?.persona}
                  onClick={() => {
                    // check if there is an error before submitting
                    setModal(true);
                    !chatAnswer ? GetUserStory() : "";
                  }}
                >
                  {!chatAnswer ? "Analyze" : "Show User Story"}
                </Button>

                <Button
                  variant="light"
                  loading={loading}
                  disabled={
                    loading ||
                    !issue?.elementName ||
                    !issue?.type ||
                    !issue?.label ||
                    !issue?.status ||
                    !issue?.description ||
                    !issue?.acceptanceCriteria ||
                    !issue?.assignee ||
                    !issue?.category ||
                    !issue?.subCategory ||
                    !issue?.feature  ||
                    !issue?.persona
                  }
                  onClick={() => {
                    delete issue.fileName;
                    delete issue.children;
                    if (!issue.externalLink) delete issue.externalLink;
                    if (!issue.associatedCr) delete issue.associatedCr;
                    if (!issue.notes) delete issue.notes;

                    createIssue({
                      variables: {
                        issue: {
                          ...issue,
                          project: {
                            id: projectId,
                          },
                        },
                      },
                    });
                  }}
                >
                  Save
                </Button>
              </>
            )}
          </Group>

          {!issueId && (
            <Text
              color={"red"}
              size="xs"
              className="danger"
              style={{
                marginTop: "10px",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <b>Note:</b> To generate a user story, provide project
              description, Name your nodes correctly, add notes and persona.
            </Text>
          )}
        </>
      )}

      {!issueLoading && (issueData || !issueId) && (
        <form>
          <TabsTemplate tabs={tabs} noActions />
        </form>
      )}
    </Paper>
  );
};
export default Issue;
