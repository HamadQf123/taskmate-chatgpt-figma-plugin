export type Route =
  | 'login'
  | 'register'
  | 'dashboard'
  | 'project'
  | 'issue'
  | 'invitations'
  | ''

export interface RoutingAction {
  type: 'routing' | ''
  payload: RouteStateType
}

export interface RoutingContextType {
  state: RouteStateType
  dispatch: (action: RoutingAction) => void
}

export interface RouteStateType {
  route: Route
  loading?: boolean
}
