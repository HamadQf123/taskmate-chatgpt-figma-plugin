import React from "react";
import { Text, Button, Notification } from "@mantine/core";
import { useDeleteIssue } from "../../../shared/service";
import { Icon } from "@iconify/react";

const DeleteIssue = ({
  id,
  close,
  refetch,
}: {
  id: string;
  close: (success?: boolean) => void;
  refetch?: () => void;
}) => {
  const { deleteIssue, loading, error } = useDeleteIssue();

  return (
    <>
      {error && (
        <Notification
          icon={<Icon width={18} height={18} icon={"ph:x"} />}
          color="red"
          disallowClose
        >
          {error.message}
        </Notification>
      )}
      <Text>Are you sure you want to delete this issue?</Text>

      <div
        style={{
          display: "flex",
          width: "100%",
          alignItems: "center",
          justifyContent: "flex-end",
          gap: "10px",
          marginTop: "10px",
        }}
      >
        <Button
          variant="outline"
          onClick={() => close()}
          disabled={loading}
          className="cancel"
        >
          Cancel
        </Button>
        <Button
          color={"red"}
          onClick={() => {
            deleteIssue({
              variables: {
                deleteIssueId: id,
              },
            })?.then(() => {
              refetch?.();
              close(true);
            });
          }}
          loading={loading}
          className="danger"
        >
          Delete
        </Button>
      </div>
    </>
  );
};
export default DeleteIssue;
