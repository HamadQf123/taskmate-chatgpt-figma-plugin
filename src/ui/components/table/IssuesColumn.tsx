import type { DataTableColumn } from 'mantine-datatable'
import {
  IssueStatusType,
  IssueType,
  KeywordType,
} from '../../../shared/types/issues'
import { Icon } from '@iconify/react'
import {
  Group,
  ActionIcon,
  Text,
  TextInput,
  Anchor,
  Select,
  Popover,
} from '@mantine/core'
import React, { useEffect, useState } from 'react'
import { useGetProject, useMe } from '../../../shared/service'
import { ProjectInfoType } from '../../../shared/types'
import { showNotification } from '@mantine/notifications'
import { useFigmaContext } from '../../../shared/context/figma/figmaProvider'
import { issueTypes, statusOptions } from '../../../shared/data'
import { format } from 'date-fns'
import { DateRange, DayPicker } from 'react-day-picker'
import useCategoryOptions from '../../../shared/hooks/useCategoryOptions'
import useFeatureOptions from '../../../shared/hooks/useFeatureOptions'
import useSubCategoryOptions from '../../../shared/hooks/useSubCategoryOptions'

export const useIssueColumns = (projectInfo: ProjectInfoType) => {
  const { state: figmaState } = useFigmaContext()

  const pastMonth = new Date()
  pastMonth.setDate(1)
  const [range, setRange] = useState<DateRange | undefined>()

  const [keywords, setKeywords] = useState<KeywordType>({
    id: '',
    description: '',
    created: undefined,
  })

  const [filter, setFilter] = useState<any>()

  const { data: projectData } = useGetProject(projectInfo.id ?? '')
  const { data: me } = useMe()

  let isAdmin = !!projectData?.getProject?.access?.find(
    (user: any) => user?.user?.id === me?.me?.id && user?.role === 'ADMIN'
  )

  const [issueToDelete, setIssueToDelete] = useState<IssueType>()

  const { categoriesError, categoryOptions } = useCategoryOptions()
  const { subCategoriesError, subCategoryOptions } = useSubCategoryOptions()
  const { featuresError, featureOptions } = useFeatureOptions()

  useEffect(() => {
    {
      categoriesError &&
        showNotification({
          message: categoriesError.message,
          color: 'red',
        })
    }
    {
      subCategoriesError &&
        showNotification({
          message: subCategoriesError.message,
          color: 'red',
        })
    }

    {
      featuresError &&
        showNotification({
          message: featuresError.message,
          color: 'red',
        })
    }
  }, [categoriesError, subCategoriesError, featuresError])

  const IssueColumns: Array<DataTableColumn<IssueType>> = [
    {
      accessor: 'actions',
      title: <Text mr="xs">Actions</Text>,
      textAlignment: 'right',
      render: (record: IssueType) => (
        <Group spacing={4} position="left" noWrap>
          {record?.id !== 'search' && (
            <>
              <ActionIcon color="blue">
                <Icon
                  icon="ph:note-pencil"
                  height="16px"
                  width="16px"
                  className="link"
                />
              </ActionIcon>
              {isAdmin && (
                <ActionIcon
                  color="red"
                  onClick={(e) => {
                    e.stopPropagation()
                    setIssueToDelete(record)
                  }}
                >
                  <Icon
                    icon="ph:trash"
                    height="16px"
                    width="16px"
                    className="danger"
                  />
                </ActionIcon>
              )}
            </>
          )}
          {record?.id === 'search' && (
            <Text
              style={{
                padding: '7px 10px',
              }}
            >
              Search
            </Text>
          )}
        </Group>
      ),
    },
    {
      accessor: 'id',
      width: '60%',
      sortable: true,
      title: 'Issue ID',
      render: (record) => {
        return (
          <>
            {record?.id && record?.id !== 'search' && <Text>{record?.id}</Text>}
            {record?.id === 'search' && (
              <TextInput
                // sx={{ flexBasis: '60%' }}
                placeholder=""
                // icon={<Icon icon="ph:magnifying-glass" />}
                value={keywords.id.toString() ?? ''}
                type="number"
                name="id"
                onChange={(e) =>
                  setKeywords({
                    ...keywords,
                    id: e.target.value,
                  })
                }
                style={{
                  width: '100%',
                  height: '100%',
                }}
              />
            )}
          </>
        )
      },
    },
    {
      accessor: 'label',
      width: '60%',
      sortable: true,
      title: 'Label',
      render(record) {
        return (
          <>
            {record?.label && (
              <Text size="sm" className="ellipsed">
                {record.label}
              </Text>
            )}
            {record?.id === 'search' && (
              <TextInput
                // sx={{ flexBasis: '60%' }}
                placeholder=""
                // icon={<Icon icon="ph:magnifying-glass" />}
                value={keywords.label}
                name="label"
                onChange={(e) =>
                  setKeywords({
                    ...keywords,
                    label: e.target.value,
                  })
                }
                style={{
                  width: '100%',
                  height: '100%',
                  border: 'none',
                  borderRadius: 0,
                }}
              />
            )}
          </>
        )
      },
    },
    {
      accessor: 'type',
      width: '60%',
      sortable: true,
      title: 'Type',
      render(record) {
        return (
          <>
            {record?.type && <Text className="ellipsed">{record?.type}</Text>}
            {record?.id === 'search' && (
              <Select
                onChange={(data: 'Task' | 'Bug' | null) => {
                  data &&
                    setFilter({
                      ...filter,
                      type: data,
                    })
                }}
                value={filter && filter?.type ? filter?.type : ''}
                id="type"
                name="type"
                style={{
                  border: 'none',
                  borderRadius: 0,
                  display: 'block',
                  height: '100%',
                  width: '100%',
                }}
                nothingFound={
                  <div
                    style={{
                      padding: '1rem',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text color={'gray'}>No data found</Text>
                  </div>
                }
                searchable
                placeholder=""
                data={issueTypes}
              />
            )}
          </>
        )
      },
    },
    {
      accessor: 'status',
      width: 170,
      sortable: true,
      title: 'Issue Status',
      render(record) {
        return (
          <>
            {record?.status && (
              <Text className="ellipsed">{record?.status}</Text>
            )}
            {record?.id === 'search' && (
              <Select
                onChange={(data: IssueStatusType) => {
                  data &&
                    setFilter({
                      ...filter,
                      status: data,
                    })
                }}
                id="status"
                name="status"
                value={filter && filter?.status ? filter?.status : ''}
                style={{
                  border: 'none',
                  borderRadius: 0,
                  display: 'block',
                  height: '100%',
                  width: '100%',
                }}
                nothingFound={
                  <div
                    style={{
                      padding: '1rem',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text color={'gray'}>No data found</Text>
                  </div>
                }
                searchable
                placeholder=""
                data={statusOptions}
              />
            )}
          </>
        )
      },
    },
    {
      accessor: 'category',
      width: '60%',
      sortable: true,
      title: 'Category',
      render(record) {
        return (
          <>
            {record?.category && (
              <Text className="ellipsed">{record?.category?.name}</Text>
            )}
            {record?.id === 'search' && (
              <Select
                onChange={(data) => {
                  data &&
                    setFilter({
                      ...filter,
                      category: {
                        id: data.toString(),
                      },
                    })
                }}
                id="category"
                name="category"
                value={filter && filter?.category ? filter?.category : ''}
                style={{
                  border: 'none',
                  borderRadius: 0,
                  display: 'block',
                  height: '100%',
                  width: '100%',
                }}
                nothingFound={
                  <div
                    style={{
                      padding: '1rem',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text color={'gray'}>No categories found</Text>
                  </div>
                }
                searchable
                placeholder=""
                data={categoryOptions}
              />
            )}
          </>
        )
      },
    },
    {
      accessor: 'subCategory',
      width: '60%',
      sortable: true,
      title: 'Subcategory',
      render(record) {
        return (
          <>
            {record?.subCategory && (
              <Text className="ellipsed">{record?.subCategory.name}</Text>
            )}
            {record?.id === 'search' && (
              <Select
                onChange={(data) => {
                  data &&
                    setFilter({
                      ...filter,
                      subCategory: {
                        id: data.toString(),
                      },
                    })
                }}
                id="subCategory"
                name="subCategory"
                value={filter && filter?.subCategory ? filter?.subCategory : ''}
                style={{
                  border: 'none',
                  borderRadius: 0,
                  display: 'block',
                  height: '100%',
                  width: '100%',
                }}
                nothingFound={
                  <div
                    style={{
                      padding: '1rem',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text color={'gray'}>No subcaregories found</Text>
                  </div>
                }
                searchable
                placeholder=""
                data={subCategoryOptions}
              />
            )}
          </>
        )
      },
    },
    {
      accessor: 'feature',
      width: '60%',
      sortable: true,
      title: 'Feature',
      render(record) {
        return (
          <>
            {record?.feature && (
              <Text className="ellipsed">{record?.feature.name}</Text>
            )}
            {record?.id === 'search' && (
              <Select
                onChange={(data) => {
                  data &&
                    setFilter({
                      ...filter,
                      feature: {
                        id: data.toString(),
                      },
                    })
                }}
                id="feature"
                name="feature"
                value={filter && filter?.feature ? filter?.feature : ''}
                style={{
                  border: 'none',
                  borderRadius: 0,
                  display: 'block',
                  height: '100%',
                  width: '100%',
                }}
                nothingFound={
                  <div
                    style={{
                      padding: '1rem',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text color={'gray'}>No features found</Text>
                  </div>
                }
                searchable
                placeholder=""
                data={featureOptions}
              />
            )}
          </>
        )
      },
    },
    {
      accessor: 'created',
      width: '20%',
      sortable: true,
      render(record) {
        return (
          <>
            {record?.created && (
              <Text className="ellipsed">
                {format(new Date(record?.created), 'dd-MM-yyyy')}
              </Text>
            )}
            {record?.id === 'search' && (
              <div
                style={{
                  padding: '7px 10px',
                }}
              >
                <Popover width={'100%'} position="bottom" withArrow shadow="md">
                  <Popover.Target>
                    <div
                      style={{
                        display: 'flex',
                        justifyContent: keywords?.created
                          ? 'space-between'
                          : 'flex-end',
                        alignItems: 'center',
                        width: '100%',
                        height: '100%',
                      }}
                    >
                      {keywords?.created && <span>Date Range</span>}
                      <Icon icon="ph:calendar" />
                    </div>
                  </Popover.Target>
                  <Popover.Dropdown
                    style={{
                      width: '100%',
                      left: '100%',
                    }}
                  >
                    <div
                      style={{
                        position: 'relative',
                        // width: '100%',
                        cursor: 'pointer',
                        display: 'flex',
                        justifyContent: keywords?.created
                          ? 'space-between'
                          : 'flex-end',
                      }}
                    >
                      <DayPicker
                        style={{
                          right: 0,
                          left: '100%',
                        }}
                        mode="range"
                        selected={range}
                        defaultMonth={range?.from ?? pastMonth}
                        onSelect={(selected) => {
                          setRange(selected)

                          if (selected && selected.from && selected.to) {
                            const dateFrom = format(
                              selected?.from,
                              'yyyy-MM-dd'
                            )
                            const dateTo = format(selected?.to, 'yyyy-MM-dd')

                            setKeywords({
                              ...keywords,
                              created: `${dateFrom}:${dateTo}`,
                            })
                          }
                        }}
                      />
                    </div>
                  </Popover.Dropdown>
                </Popover>
              </div>
            )}
          </>
        )
      },
    },
    {
      accessor: 'assignee',
      width: '20%',
      title: 'Assignee',
      render(record) {
        return (
          <>
            {record?.assignee && (
              <Text className="ellipsed">{record?.assignee.name}</Text>
            )}
            {record?.id === 'search' && (
              <Select
                onChange={(data) => {
                  data &&
                    setFilter({
                      ...filter,
                      assignee: {
                        id: data,
                      },
                    })
                }}
                style={{
                  border: 'none',
                  borderRadius: 0,
                  display: 'block',
                  height: '100%',
                  width: '100%',
                }}
                id="assignee"
                name="assignee"
                value={filter && filter?.assignee ? filter?.assignee : ''}
                nothingFound={
                  <div
                    style={{
                      padding: '1rem',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Text color={'gray'}>No data found</Text>
                  </div>
                }
                searchable
                placeholder=""
                data={
                  projectData?.getProject?.access?.map(
                    (user: { user: { id: string; name: string } }) => {
                      return {
                        label: user.user?.name,
                        value: user.user?.id,
                      }
                    }
                  ) ?? []
                }
              />
            )}
          </>
        )
      },
    },
  ]

  return {
    IssueColumns,
    issueToDelete,
    setIssueToDelete,
    keywords,
    setKeywords,
    filter,
    setFilter,
    range,
    setRange,
  }
}
