import React from "react";
import { TabContent } from "../../../shared/types";
import TabsTemplate from "../tabs";
import Categories from "./Categories";
import Subcategories from "./Subcategories";
import Features from "./Features";

const tabs: TabContent[] = [
  {
    value: "categories",
    title: "Categories",
    content: <Categories />,
  },
  {
    value: "subcategories",
    title: "Subcategories",
    content: <Subcategories />,
  },
  {
    value: "features",
    title: "Features",
    content: <Features />,
  },
];

const CategorySubcategoryFeature = ({
  defaultValue,
}: {
  defaultValue?: string;
}) => {
  return <TabsTemplate tabs={tabs} noActions defaultValue={defaultValue} />;
};
export default CategorySubcategoryFeature;
