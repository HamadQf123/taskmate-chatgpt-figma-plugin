import { useMutation, useQuery } from '@apollo/client'
import {
  CREATE_FEATURE,
  DELETE_FEATURE,
  UPDATE_FEATURE,
} from '../../graphql/mutations'
import { GET_FEATURES } from '../../graphql/queries'

export const useCreateFeature = () => {
  const [createFeature, { loading, data, error }] = useMutation(CREATE_FEATURE)

  return {
    createFeature,
    loading,
    data,
    error,
  }
}

export const useUpdateFeature = () => {
  const [updateFeature, { loading, data, error }] = useMutation(UPDATE_FEATURE)

  return {
    updateFeature,
    loading,
    data,
    error,
  }
}

export const useDeleteFeature = () => {
  const [deleteFeature, { loading, data, error }] = useMutation(DELETE_FEATURE)

  return {
    deleteFeature,
    loading,
    data,
    error,
  }
}

export const useGetFeatures = ({
  params,
}: {
  params: {
    id: number
  }
}) => {
  const { loading, data, error, refetch } = useQuery(GET_FEATURES, {
    variables: { params },
    fetchPolicy: 'network-only',
    notifyOnNetworkStatusChange: true,
  })

  return {
    loading,
    data,
    error,
    refetch,
  }
}
