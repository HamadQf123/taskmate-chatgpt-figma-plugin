import { gql } from "@apollo/client";

export const LOGIN = gql`
  query Login($login: Login!) {
    login(login: $login) {
      id
      refreshToken
      accessToken
    }
  }
`;

export const GET_USER = gql`
  query GetUser($getUserId: Float!) {
    getUser(id: $getUserId) {
      username
      id
      name
    }
  }
`;

export const GET_ME = gql`
  query Me {
    me {
      username
      id
      name
      invitations {
        id
        createdBy {
          name
          username
        }
        project {
          name
          id
          # role
        }
      }
    }
  }
`;

export const GET_PROJECTS = gql`
  query GetProjects($params: Params!) {
    getProjects(params: $params) {
      name
      description
      total
      id
      created
      createdBy {
        name
      }
      access {
        id
        created
        role
        user {
          name
          username
          id
        }
      }
    }
  }
`;

export const GET_PROJECT = gql`
  query GetProject($getProjectId: Float!) {
    getProject(id: $getProjectId) {
      id
      created
      createdBy {
        name
        id
      }
      access {
        id
        created
        role
        user {
          name
          username
          id
        }
      }
      invitations {
        id
        user {
          name
          username
          id
        }
      }
      name
      description
    }
  }
`;

export const GET_ISSUES = gql`
  query GetIssues($params: Params!) {
    getIssues(params: $params) {
      assignee {
        id
        name
      }
      associatedCr
      category {
        id
        name
        project
      }
      completedAt
      component
      created
      description
      elementName {
        fileId
        fileName
        id
        name
      }
      feature {
        id
        name
        project
      }
      file
      id
      label
      status
      total
      subCategory {
        name
        id
        project
      }
      type
    }
  }
`;

export const GET_ISSUE = gql`
  query GetIssue($getIssueId: Float!) {
    getIssue(id: $getIssueId) {
      acceptanceCriteria
      assignee {
        name
        id
      }
      associatedCr
      category {
        name
        id
      }
      completedAt
      component
      created
      createdBy {
        name
        id
      }
      description
      elementName {
        fileId
        fileName
        id
        name
      }
      externalLink
      feature {
        name
        id
      }
      id
      label
      notes
      persona
      project {
        id
      }
      started
      subCategory {
        name
        id
      }
      type
      status
    }
  }
`;

export const GET_USERS = gql`
  query GetUsers($params: Params!) {
    getUsers(params: $params) {
      username
      id
      name
    }
  }
`;

export const GET_CATEGORIES = gql`
  query GetCategories($params: Params!) {
    getCategories(params: $params) {
      name
      id
    }
  }
`;

export const GET_SUBCATEGORIES = gql`
  query GetSubCategories($params: Params!) {
    getSubCategories(params: $params) {
      name
      id
    }
  }
`;

export const GET_FEATURES = gql`
  query GetFeatures($params: Params!) {
    getFeatures(params: $params) {
      name
      id
    }
  }
`;

export const GET_USER_STORY = gql`
  query Completions($prompt: ChatGPTPrompt!) {
    completions(prompt: $prompt) {
      response {
        text
      }
      responses {
        text
      }
    }
  }
`;
