import { Icon } from "@iconify/react";
import { Paper, Text } from "@mantine/core";
import React, { ReactNode } from "react";

const Banner = ({
  iconCode,
  iconColor,
  children,
  actions,
  margin,
  align,
}: {
  iconCode?: string;
  iconColor?: { bg: string; color: string };
  children: ReactNode;
  actions: ReactNode;
  margin?: number;
  align?: "center" | "flex-start";
}) => {
  return (
    <Paper
      p="md"
      withBorder
      style={{
        margin: margin ?? 10,
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
      }}
      className="dark-secondary-bg"
    >
      <div
        style={{
          display: "flex",
          alignItems: align ?? "center",
          justifyContent: "space-between",
        }}
      >
        <div
          style={{
            // backgroundColor: iconColor ? iconColor.bg : "rgb(112 26 117)",
            height: "40px",
            width: "40px",
            borderRadius: "5px",
            marginRight: "10px",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            background: "#44474B",
            boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
          }}
        >
          <svg
            width="28"
            height="34"
            viewBox="0 0 28 34"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M14 0C12.1911 0 10.7273 1.5207 10.7273 3.4C10.7273 5.2793 12.1911 6.8 14 6.8C15.8089 6.8 17.2727 5.2793 17.2727 3.4C17.2727 1.5207 15.8089 0 14 0ZM12.0568 8.5C10.0689 8.5 8.34943 9.78164 7.60795 11.5813L7.50568 11.7937L6.38068 16.3625C6.24006 16.8805 5.76705 17.3719 5.10227 17.5844C5.08949 17.591 5.06392 17.5777 5.05114 17.5844L0.5 18.7531L1.31818 22.0469L5.86932 20.8781H5.92045V20.825C7.51847 20.3469 8.98864 19.1648 9.5 17.3188V17.2656H9.55114L9.90909 15.7781V20.4L7.45455 21.0906L11.3409 24.0656L6.32955 22.2594C5.51136 21.9207 4.50781 22.4188 4.18182 23.2687C3.85582 24.1187 4.33523 25.1613 5.15341 25.5L8.98864 26.8281L7.45455 27.3594L8.57955 30.6L22.6932 25.5C23.5114 25.1613 23.9908 24.2914 23.6648 23.2687C23.3388 22.4188 22.5014 21.9207 21.517 22.2594L16.6591 24.0125L20.5455 21.0906L18.0909 20.4V15.7781L18.4489 17.2656H18.5V17.3188C18.9411 18.9324 20.2003 20.493 22.1307 20.825V20.8781L26.6818 22.0469L27.5 18.7531L22.8977 17.5844L22.7955 17.5312H22.6932C22.1115 17.4582 21.8175 17.0996 21.6193 16.4156V16.3625L20.4943 11.7937L20.392 11.5813C19.6506 9.78164 17.9311 8.5 15.9432 8.5H12.0568ZM20.5455 27.3594L15.483 29.2188L19.4205 30.6L20.5455 27.3594ZM0.909091 32.3V34H27.0909V32.3H0.909091Z"
              fill="url(#paint0_linear_84_14958)"
            />
            <defs>
              <linearGradient
                id="paint0_linear_84_14958"
                x1="14"
                y1="0"
                x2="14"
                y2="34"
                gradientUnits="userSpaceOnUse"
              >
                <stop stopColor="#8277FF" />
                <stop offset="1" stop-color="white" stop-opacity="0" />
              </linearGradient>
            </defs>
          </svg>

          {/* <Icon
            icon={iconCode ? iconCode : 'ph:kanban-fill'}
            height="30px"
            width="30px"
            color={iconColor ? iconColor.color : 'rgb(245 208 254)'}
          /> */}
        </div>
        {children}
      </div>

      {actions}
    </Paper>
  );
};
export default Banner;
