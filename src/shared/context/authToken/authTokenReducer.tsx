import { AuthToken, AuthTokenAction, AuthTokenStateType } from '../../types'

export const authTokenReducer = (
  state: AuthTokenStateType,
  action: AuthTokenAction
) => {
  const { type, payload } = action

  const setToken = (tokenPayload: AuthTokenStateType) => {
    if (tokenPayload.tokenType === 'refresh') {
      return {
        accessToken: tokenPayload.token.refreshToken,
        refreshToken: tokenPayload.token.refreshToken,
      }
    }

    return tokenPayload.token
  }

  switch (type) {
    case 'save_auth_token':
      const tokenStr = JSON.stringify(payload.token)

      parent.postMessage(
        {
          pluginMessage: { type: 'new-auth-token', tokenStr },
        },
        '*'
      )

      return {
        ...state,
        token: setToken(payload),
      }

    case 'get_auth_token':
      return {
        ...state,
        token: setToken(payload),
      }

    case 'delete_auth_token':
      parent.postMessage(
        {
          pluginMessage: { type: 'delete-auth-token' },
        },
        '*'
      )
      return {
        ...state,
        token: setToken(payload),
      }

    default:
      return state
  }
}
