import React, { useEffect } from 'react'
import Template from './Template'
import { Stack, Loader } from '@mantine/core'
import NewItem from './NewItem'
import {
  useCreateCategory,
  useGetCategories,
} from './../../../shared/service/categories'
import { useProjectIdContext } from '../../../shared/context/ProjectIdProvider'
import { showNotification } from '@mantine/notifications'

const Categories = () => {
  const { createCategory, loading, data, error } = useCreateCategory()
  const { projectId } = useProjectIdContext()
  const {
    loading: loadingCategories,
    data: categories,
    error: categoriesError,
    refetch: refetchCategories,
  } = useGetCategories({
    params: {
      id: parseInt(projectId),
    },
  })

  useEffect(() => {
    data && refetchCategories()
  }, [data])

  useEffect(() => {
    {
      error &&
        showNotification({
          message: error.message,
          color: 'red',
        })
    }

    {
      data &&
        showNotification({
          message: 'Category created successfully',
          color: 'green',
        })
    }

    {
      categoriesError &&
        showNotification({
          message: categoriesError.message,
          color: 'red',
        })
    }
  }, [error, data, categoriesError])

  return (
    <Stack spacing={'sm'} align="center" justify="center" w={'100%'}>
      {loadingCategories && <Loader size={'sm'} />}

      {categories && !loadingCategories && (
        <Template
          items={categories.getCategories}
          type="Categories"
          refetchCategories={refetchCategories}
        />
      )}

      {!loadingCategories && (
        <NewItem
          name="New Category"
          loading={loading}
          dispatch={(value) => {
            createCategory({
              variables: {
                category: {
                  name: value,
                  project: projectId,
                },
              },
            })
          }}
        />
      )}
    </Stack>
  )
}
export default Categories
