import { Accordion } from "@mantine/core";
import React, { FC, ReactNode } from "react";

interface Props {
  title: string;
  children: ReactNode;
}
const AccordionTemplate: FC<Props> = ({ title, children }) => {
  return (
    <Accordion
      variant="contained"
      defaultValue="accordion"
      className="accordion"
    >
      <Accordion.Item
        style={{
          border: "none",
          padding: "0",
          backgroundColor: "rgb(37, 38, 43)",
        }}
        value="accordion"
      >
        <Accordion.Control p={"xs"}>{title}</Accordion.Control>
        <Accordion.Panel>{children}</Accordion.Panel>
      </Accordion.Item>
    </Accordion>
  );
};

export default AccordionTemplate;
