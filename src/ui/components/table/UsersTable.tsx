import { Icon } from "@iconify/react";
import {
  TextInput,
  Group,
  ActionIcon,
  Text,
  LoadingOverlay,
  Modal,
  Anchor,
} from "@mantine/core";
import { useDebouncedValue } from "@mantine/hooks";
import { format } from "date-fns";
import { DataTable, DataTableSortStatus } from "mantine-datatable";
import React, { useEffect, useState } from "react";
import { useGetProject, useMe } from "../../../shared/service";
import EditUserRole from "../users/EditUserRole";
import RemoveUser from "../users/RemoveUser";

const UsersTable = ({ projectId }: { projectId: string }) => {
  const { loading, data, refetch } = useGetProject(projectId);
  const [keyword, setKeyword] = useState("");
  const [sortStatus, setSortStatus] = useState<DataTableSortStatus>({
    columnAccessor: "created",
    direction: "desc",
  });
  const [users, setUsers] = useState<any[]>(
    data?.getProject.access.map((user: any) => {
      return {
        ...user,
        role:
          data?.getProject?.createdBy?.id === user?.user?.id
            ? "OWNER"
            : user?.role,
        name: user?.user?.name,
        id: user?.user?.id,
        username: user?.user?.username,
        roleId: user?.id,
      };
    }) ?? []
  );
  const [debouncedKeyword] = useDebouncedValue(keyword, 200);
  const { data: me } = useMe();
  const [userToEdit, setUserToEdit] = useState<
    | {
        id: string;
        role: "ADMIN" | "MEMBER";
        name: string;
        roleId: string;
      }
    | undefined
  >();
  const [userToDelete, setUserToDelete] = useState<
    | {
        id: string;
        role: "ADMIN" | "MEMBER";
        name: string;
        roleId: string;
      }
    | undefined
  >();

  let isOwner = data?.getProject?.createdBy?.id === me?.me?.id;

  let isAdmin = !!data?.getProject?.access?.find(
    (user: any) => user?.user?.id === me?.me?.id && user?.role === "ADMIN"
  );

  useEffect(() => {
    let filteredData: any[] =
      data?.getProject?.access?.map((user: any) => {
        return {
          ...user,
          role:
            data?.getProject?.createdBy?.id === user?.user?.id
              ? "OWNER"
              : user?.role,
          name: user?.user?.name,
          id: user?.user?.id,
          username: user?.user?.username,
          roleId: user?.id,
        };
      }) ?? [];
    if (debouncedKeyword) {
      filteredData = filteredData?.filter((data) => {
        return (
          data?.name
            ?.toString()
            .toLowerCase()
            .indexOf(debouncedKeyword?.toString()?.toLowerCase()) !== -1 ||
          data?.username
            ?.toString()
            .toLowerCase()
            .indexOf(debouncedKeyword?.toString()?.toLowerCase()) !== -1
        );
      });
    }
    setUsers(filteredData);
  }, [debouncedKeyword]);

  useEffect(() => {
    // sort by date
    let sortedData = [...users];
    if (sortStatus?.columnAccessor === "created")
      setUsers(
        sortedData?.sort(function (a, b) {
          return sortStatus?.direction === "asc"
            ? new Date(a.created).valueOf() - new Date(b.created).valueOf()
            : new Date(b.created).valueOf() - new Date(a.created).valueOf();
        })
      );
    else
      setUsers(
        sortedData?.sort(function (a, b) {
          return sortStatus?.direction === "asc"
            ? ("" + a[sortStatus.columnAccessor]).localeCompare(
                b[sortStatus.columnAccessor]
              )
            : ("" + b[sortStatus.columnAccessor]).localeCompare(
                a[sortStatus.columnAccessor]
              );
        })
      );
  }, [sortStatus]);

  return (
    <>
      <Modal
        opened={!!userToEdit}
        onClose={() => setUserToEdit(undefined)}
        title={`Change ${userToEdit?.name ?? "user"} role`}
      >
        <EditUserRole
          user={userToEdit}
          refetch={refetch}
          close={() => setUserToEdit(undefined)}
        />
      </Modal>
      <Modal
        opened={!!userToDelete}
        onClose={() => setUserToDelete(undefined)}
        title={`Remove ${
          userToDelete?.id === me?.me?.id
            ? "Your"
            : userToDelete?.name ?? "user"
        } access from this project`}
      >
        <RemoveUser
          user={userToDelete}
          close={() => setUserToDelete(undefined)}
          refetch={refetch}
          projectId={projectId}
        />
      </Modal>
      <LoadingOverlay visible={loading} />
      <TextInput
        placeholder="Search User"
        icon={<Icon icon="ph:magnifying-glass" />}
        value={keyword}
        onChange={(e) => setKeyword(e.target.value)}
        style={{
          marginBottom: 10,
        }}
      />
      <DataTable
        withBorder
        minHeight={160}
        withColumnBorders
        records={users}
        columns={[
          {
            accessor: "name",
            width: "60%",
            sortable: true,
            title: "Name",
          },
          {
            accessor: "username",
            width: "60%",
            sortable: true,
            title: "Username",
          },
          {
            accessor: "role",
            width: "60%",
            sortable: true,
            title: "role",
          },
          {
            accessor: "created",
            width: "60%",
            sortable: true,
            title: "Date Added",
            render: (record) => (
              <span className="ellipsed">
                {format(new Date(record.created), "dd-MM-yyyy")}
              </span>
            ),
          },
          {
            accessor: "actions",
            title: <Text mr="xs">Actions</Text>,
            textAlignment: "right",
            render: (user) => (
              <Group spacing={4} position="center" noWrap>
                {me?.me?.id !== user?.id && (
                  <>
                    {(isOwner || (isAdmin && user?.role !== "ADMIN")) &&
                      user?.id !== data?.getProject?.createdBy?.id && (
                        <ActionIcon
                          color="blue"
                          title="Edit"
                          onClick={(e) => {
                            e.stopPropagation();
                            setUserToEdit(user);
                          }}
                          className="link"
                        >
                          <Icon
                            icon="ph:note-pencil"
                            height="16px"
                            width="16px"
                            className="link"
                          />
                        </ActionIcon>
                      )}
                    {(isOwner || (isAdmin && user?.role !== "ADMIN")) &&
                      user?.id !== data?.getProject?.createdBy?.id && (
                        <ActionIcon
                          color="red"
                          title="Remove"
                          onClick={(e) => {
                            e.stopPropagation();
                            setUserToDelete(user);
                          }}
                          className="danger"
                        >
                          <Icon
                            icon="ph:trash"
                            height="16px"
                            width="16px"
                            className="danger"
                          />
                        </ActionIcon>
                      )}
                  </>
                )}
                {me?.me?.id === user?.id && !isOwner && (
                  <>
                    <Anchor
                      onClick={(e) => {
                        e.stopPropagation();
                        setUserToDelete(user);
                      }}
                      className="danger"
                    >
                      Leave
                    </Anchor>{" "}
                  </>
                )}
              </Group>
            ),
          },
        ]}
        sortStatus={sortStatus}
        onSortStatusChange={setSortStatus}
        onCellClick={({ record }: { record: any }) => {
          // if an admin
        }}
        style={{ height: "fit-content", minHeight: 160 }}
      />
    </>
  );
};
export default UsersTable;
