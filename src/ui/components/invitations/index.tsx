import React, { useEffect } from "react";
import { Text, Paper, Button, Group, Popover } from "@mantine/core";
import { showNotification } from "@mantine/notifications";
import {
  useAcceptInvite,
  useGetProjects,
  useMe,
  useRejectInvite,
} from "../../../shared/service";
import { useDisclosure } from "@mantine/hooks";

const Invitations = ({ close, me }: { close: () => void; me: any }) => {
  return (
    <Group
      style={{
        marginTop: 10,
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
      }}
    >
      {me?.me?.invitations?.map((invitation: any) => (
        <Invitation
          key={invitation?.id}
          invitation={invitation}
          close={close}
          dataLength={me?.me?.invitations?.length}
        />
      ))}
    </Group>
  );
};
export default Invitations;

const Invitation = ({
  invitation,
  dataLength,
  close,
}: {
  invitation: {
    id: any;
    createdBy: {
      name: string;
      username: string;
    };
    project: {
      id: any;
      name: string;
    };
  };
  close: () => void;
  dataLength: number;
}) => {
  const [opened, { close: closePopover, open }] = useDisclosure(false);
  const { acceptInvite, loading, error, data } = useAcceptInvite();
  const {
    rejectInvite,
    loading: rejectLoading,
    error: rejectError,
    data: rejectData,
  } = useRejectInvite();
  const { refetch: refetchUser } = useMe();
  const { refetch } = useGetProjects({
    params: {
      page: 1,
      pageSize: 10,
      paging: true,
    },
  });

  useEffect(() => {
    if (data) {
      refetch();
      refetchUser();
      showNotification({
        message:
          data?.acceptInvite?.message ?? "Successfully accepted the invite",
        color: "green",
      });
      if (dataLength <= 1) close();
    }
    if (error) {
      showNotification({
        message: error.message,
        color: "red",
      });
    }
  }, [data, error]);

  useEffect(() => {
    if (rejectData) {
      refetchUser();
      showNotification({
        message:
          rejectData?.rejectInvite?.message ??
          "Successfully rejected the invite",
        color: "green",
      });
      if (dataLength <= 1) close();
    }
    if (rejectError) {
      showNotification({
        message: rejectError.message,
        color: "red",
      });
    }
  }, [rejectData, rejectError]);

  return (
    <Paper
      withBorder
      style={{
        padding: 7,
        width: "100%",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-between",
        gap: 10,
      }}
    >
      <div
        style={{
          flex: 2,
        }}
      >
        <Text size={"sm"}>
          <Popover
            width={200}
            position="bottom"
            withArrow
            shadow="md"
            opened={opened}
          >
            <Popover.Target>
              <span onMouseEnter={open} onMouseLeave={closePopover}>
                <Text
                  fw={"bold"}
                  style={{
                    display: "inline-block",
                  }}
                >
                  {invitation?.createdBy?.name}
                </Text>
              </span>
            </Popover.Target>
            <Popover.Dropdown
              style={{
                padding: "5px 7px",
              }}
              sx={{ pointerEvents: "none" }}
            >
              <Text size="xs">{invitation?.createdBy?.username}</Text>
            </Popover.Dropdown>
          </Popover>{" "}
          has invited you to contribute in{" "}
          <Text
            fw={"bold"}
            style={{
              display: "inline-block",
            }}
          >
            {invitation.project.name}
          </Text>{" "}
          project
        </Text>
      </div>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          gap: 7,
        }}
      >
        <Button
          variant="light"
          size="sm"
          color={"red"}
          className="danger"
          compact
          disabled={loading || rejectLoading}
          loading={rejectLoading}
          onClick={() => {
            // reject
            rejectInvite({
              variables: {
                declineId: invitation?.id,
              },
            });
          }}
        >
          Reject
        </Button>
        <Button
          variant="light"
          size="sm"
          compact
          disabled={loading || rejectLoading}
          loading={loading}
          onClick={() => {
            // accept
            acceptInvite({
              variables: {
                acceptId: invitation?.id,
              },
            });
          }}
        >
          Accept
        </Button>
      </div>
    </Paper>
  );
};
