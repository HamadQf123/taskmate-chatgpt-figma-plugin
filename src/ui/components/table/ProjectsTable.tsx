import { Icon } from '@iconify/react'
import {
  TextInput,
  Anchor,
  Group,
  ActionIcon,
  Text,
  Modal,
  Paper,
  Button,
} from '@mantine/core'
import { useDebouncedValue, useDisclosure } from '@mantine/hooks'
import { format } from 'date-fns'
import { DataTable, DataTableSortStatus } from 'mantine-datatable'
import React, { useState, MouseEvent } from 'react'
import { useRoutingState } from '../../../shared/context/routing/RoutingState'
import { useGetProjects, useMe } from '../../../shared/service'
import { ProjectQueryReturn, TheProject } from '../../../shared/types'
import DeleteProject from '../deleteProject'
import ProjectForm from '../forms/ProjectForm'
import { useProjectIdContext } from '../../../shared/context/ProjectIdProvider'
import Banner from '../banner'

const ProjectsTable = () => {
  const [opened, { close, open }] = useDisclosure(false)
  const [sortStatus, setSortStatus] = useState<DataTableSortStatus>({
    columnAccessor: 'created',
    direction: 'desc',
  })
  const { dispatch } = useRoutingState()
  const { setProjectId } = useProjectIdContext()
  const [keyword, setKeyword] = useState('')
  const [debouncedKeyword] = useDebouncedValue(keyword, 200)
  const [projectToEdit, setProjectToEdit] = useState<TheProject | undefined>()
  const [projectToDelete, setProjectToDelete] = useState<
    TheProject | undefined
  >()
  const [page, setPage] = useState(1)
  const {
    loading: projectsLoading,
    data: projectsData,
    refetch,
  } = useGetProjects({
    params: {
      page,
      pageSize: 10,
      paging: true,
      ...(debouncedKeyword ? { search: debouncedKeyword } : {}),
      ...(sortStatus
        ? {
            property: sortStatus?.columnAccessor,
            direction: sortStatus?.direction,
          }
        : {}),
    },
  })
  const { data: me } = useMe()

  return (
    <>
      <Modal opened={opened} onClose={close} title="Create a new project">
        <ProjectForm done={close} refetch={refetch} />
      </Modal>
      <Banner
        iconCode="ph:kanban-fill"
        actions={
          <Button variant="light" onClick={open}>
            Create new
          </Button>
        }
      >
        <div>
          <Text weight={'bold'}>Projects</Text>
          <Text size="sm">Create a project to organize your work</Text>
        </div>
      </Banner>
      <Paper
        style={{ padding: '0 10px', backgroundColor: 'rgb(31, 31, 31)' }}
        radius={0}
      >
        <Modal
          opened={!!projectToEdit}
          onClose={() => setProjectToEdit(undefined)}
          title="Edit Project"
        >
          <ProjectForm
            project={projectToEdit}
            close={() => setProjectToEdit(undefined)}
            refetch={refetch}
          />
        </Modal>

        <Modal
          opened={!!projectToDelete}
          onClose={() => setProjectToDelete(undefined)}
          title="Delete Project"
        >
          <DeleteProject
            id={projectToDelete?.id ?? ''}
            close={() => setProjectToDelete(undefined)}
            refetch={refetch}
          />
        </Modal>

        <TextInput
          sx={{ flexBasis: '60%' }}
          placeholder="Search project name"
          icon={<Icon icon="ph:magnifying-glass" />}
          value={keyword}
          onChange={(e) => setKeyword(e.target.value)}
          style={{ paddingBottom: '10px' }}
        />
        <DataTable
          minHeight={160}
          withBorder
          withColumnBorders
          fetching={projectsLoading}
          className={`${
            projectsData?.getProjects?.length > 0 ? '' : 'no-data'
          }`}
          records={
            projectsData?.getProjects.map((p: ProjectQueryReturn) => {
              return {
                ...p,
                createdBy: p?.createdBy?.name,
              }
            }) ?? []
          }
          columns={[
            {
              accessor: 'name',
              width: '60%',
              sortable: true,
              render: (record) => (
                <Anchor href="#" className="ellipsed">
                  {record?.name}
                </Anchor>
              ),
            },
            {
              accessor: 'created',
              width: '20%',
              sortable: true,
              render: (record) => (
                <span className="ellipsed">
                  {format(new Date(record?.created), 'dd-MM-yyyy')}
                </span>
              ),
            },
            { accessor: 'createdBy', width: '20%' },
            {
              accessor: 'actions',
              title: <Text mr="xs">Actions</Text>,
              textAlignment: 'right',
              render: (project) => {
                let isAdmin = !!project?.access?.find(
                  (user: any) =>
                    user?.user?.id === me?.me?.id && user?.role === 'ADMIN'
                )
                return (
                  <Group spacing={4} position="center" noWrap>
                    <ActionIcon
                      color="blue"
                      className="link"
                      onClick={(e: MouseEvent) => {
                        e.stopPropagation()
                        setProjectToEdit(project)
                      }}
                    >
                      <Icon
                        icon="ph:note-pencil"
                        height="16px"
                        width="16px"
                        className="link"
                      />
                    </ActionIcon>
                    {isAdmin && (
                      <ActionIcon
                        color="red"
                        className="danger"
                        onClick={(e: MouseEvent) => {
                          e.stopPropagation()
                          setProjectToDelete(project)
                        }}
                      >
                        <Icon
                          icon="ph:trash"
                          height="16px"
                          width="16px"
                          className="danger"
                        />
                      </ActionIcon>
                    )}
                  </Group>
                )
              },
            },
          ]}
          style={{
            height: 'fit-content',
            ...(projectsData?.getProjects?.length > 0
              ? { minHeight: '100px' }
              : { color: 'transparent' }),
          }}
          page={projectsData?.getProjects?.length > 0 ? page : 0}
          onPageChange={(page) => {
            setPage(page)
          }}
          totalRecords={projectsData?.getProjects[0]?.total}
          recordsPerPage={10}
          onCellClick={({ record }: { record: any }) => {
            dispatch({
              type: 'routing',
              payload: {
                route: 'project',
              },
            })
            setProjectId(record.id)
          }}
          sortStatus={sortStatus}
          onSortStatusChange={setSortStatus}
        />
      </Paper>
    </>
  )
}
export default ProjectsTable
