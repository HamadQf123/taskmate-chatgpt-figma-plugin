export type IssueStatusType =
  | "New"
  | "In Design"
  | "In Develop"
  | "In Testing"
  | "Design Complete"
  | "Develop Complete"
  | "Testing Complete"
  | "Lauched";

type NameAndId = {
  name: string;
  id: string;
};

export type IssueTypesType = {
  label: "Task" | "Bug";
  value: "Task" | "Bug";
};

export type IssueOptionType = {
  label: IssueStatusType;
  value: IssueStatusType;
};

export interface IssueType {
  acceptanceCriteria: string;
  assignee: NameAndId;
  associatedCr: string;
  category: NameAndId;
  completedAt: Date;
  component: string;
  created: Date;
  createdBy: NameAndId;
  description: string;
  elementName: Array<{
    fileId: string;
    fileName: string;
    id: string;
    name: string;
  }>;
  externalLink: string;
  feature: NameAndId;
  id: string;
  label: string;
  notes: string;
  persona: string;
  project: {
    id: number;
  };
  started: Date;
  subCategory: NameAndId;
  type: "Task" | "Bug";
  status: IssueStatusType;
}

export interface KeywordType {
  acceptanceCriteria?: string;
  assignee?: NameAndId;
  associatedCr?: string;
  category?: NameAndId;
  completedAt?: Date;
  component?: string;
  created?: string;
  createdBy?: NameAndId;
  description: string;
  elementName?: Array<{
    fileId: string;
    fileName: string;
    id: string;
    name: string;
  }>;
  externalLink?: string;
  feature?: NameAndId;
  id: string;
  label?: string;
  persona?: string;
  notes?: string;
  project?: {
    id: number;
  };
  started?: Date;
  subCategory?: NameAndId;
  type?: "Task" | "Bug";
  status?: IssueStatusType;
}
