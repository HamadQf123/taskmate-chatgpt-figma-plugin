import {
  manageToken,
  getToken,
  getSelection,
  selectNode,
  getFileKey,
  getSelections,
  getFileName,
  resizePlugin,
  minimizePlugin,
  maximizePlugin,
} from './utils'

const main = () => {
  const zoom = figma.viewport.zoom
  let w = 500
  let h = 400
  let centerX = figma.viewport.center.x - w / zoom / 2
  let centerY = figma.viewport.center.y - h / zoom / 2

  figma.showUI(__html__, {
    themeColors: false,
    width: w,
    height: h,
    position: { x: centerX, y: centerY },
  })

  figma.ui.onmessage = async (msg) => {
    manageToken(msg)
    selectNode(msg)
    getSelection(msg)
    getSelections(msg)
    getFileName(msg)
    resizePlugin(msg)
    minimizePlugin(msg)
    maximizePlugin(msg)
  }

  figma.clientStorage.getAsync('fileName').then((res) => {
    if (res) {
      figma.ui.postMessage({
        type: 'receiveFileName',
        fileName: res,
      })
    }
  })

  getFileKey()
  getToken()
}

main()
