import { Paper, ScrollArea, Text } from "@mantine/core";
import React, { useEffect } from "react";
import Item, { ItemType } from "./Item";
import {
  useDeleteCategory,
  useUpdateCategory,
} from "../../../shared/service/categories";
import {
  useDeleteSubCategory,
  useUpdateSubCategory,
} from "../../../shared/service/subCategories";
import {
  useDeleteFeature,
  useUpdateFeature,
} from "../../../shared/service/features";
import { showNotification } from "@mantine/notifications";

interface TemplateProps {
  items: ItemType[];
  type: "Categories" | "Subcategories" | "Features";
  refetchCategories?: any;
  refetchSubCategories?: any;
  refetchFeatures?: any;
}

const Template = ({
  items,
  type,
  refetchCategories,
  refetchSubCategories,
  refetchFeatures,
}: TemplateProps) => {
  const {
    updateCategory,
    loading: loadingUpdateCategory,
    data: updatedCategory,
    error: updateCategoryError,
  } = useUpdateCategory();

  const {
    deleteCategory,
    loading: loadingDeleteCategory,
    data: deletedCategory,
    error: deleteCategoryError,
  } = useDeleteCategory();

  const {
    updateSubCategory,
    loading: loadingUpdateSubCategory,
    data: updatedSubCategory,
    error: updateSubCategoryError,
  } = useUpdateSubCategory();

  const {
    deleteSubCategory,
    loading: loadingDeleteSubCategory,
    data: deletedSubCategory,
    error: deleteSubCategoryError,
  } = useDeleteSubCategory();

  const {
    updateFeature,
    loading: loadingUpdateFeature,
    data: updatedFeature,
    error: updateFeatureError,
  } = useUpdateFeature();

  const {
    deleteFeature,
    loading: loadingDeleteFeature,
    data: deletedFeature,
    error: deleteFeatureError,
  } = useDeleteFeature();

  useEffect(() => {
    updatedCategory && refetchCategories();
    deletedCategory && refetchCategories();
    updatedSubCategory && refetchSubCategories();
    deletedSubCategory && refetchSubCategories();
    updatedFeature && refetchFeatures();
    deletedFeature && refetchFeatures();
  }, [
    updatedCategory,
    deletedCategory,
    updatedSubCategory,
    deletedSubCategory,
    updatedFeature,
    deletedFeature,
  ]);

  useEffect(() => {
    {
      updateCategoryError &&
        showNotification({
          message: updateCategoryError.message,
          color: "red",
        });
    }
    {
      deleteCategoryError &&
        showNotification({
          message: deleteCategoryError.message,
          color: "red",
        });
    }

    {
      updatedCategory &&
        showNotification({
          message: "Category updated successfully",
          color: "green",
        });
    }

    {
      deletedCategory &&
        showNotification({
          message: "Category deleted successfully",
          color: "green",
        });
    }
  }, [
    updateCategoryError,
    deleteCategoryError,
    deletedCategory,
    updatedCategory,
  ]);

  useEffect(() => {
    {
      updateSubCategoryError &&
        showNotification({
          message: updateSubCategoryError.message,
          color: "red",
        });
    }
    {
      deleteSubCategoryError &&
        showNotification({
          message: deleteSubCategoryError.message,
          color: "red",
        });
    }

    {
      updatedSubCategory &&
        showNotification({
          message: "Subcategory updated successfully",
          color: "green",
        });
    }

    {
      deletedSubCategory &&
        showNotification({
          message: "Subcategory deleted successfully",
          color: "green",
        });
    }
  }, [
    updateSubCategoryError,
    deleteSubCategoryError,
    deletedSubCategory,
    updatedSubCategory,
  ]);

  useEffect(() => {
    {
      updateFeatureError &&
        showNotification({
          message: updateFeatureError.message,
          color: "red",
        });
    }
    {
      deleteFeatureError &&
        showNotification({
          message: deleteFeatureError.message,
          color: "red",
        });
    }

    {
      updatedFeature &&
        showNotification({
          message: "Feature updated successfully",
          color: "green",
        });
    }

    {
      deletedFeature &&
        showNotification({
          message: "Feature deleted successfully",
          color: "green",
        });
    }
  }, [updateFeatureError, deleteFeatureError, deletedFeature, updatedFeature]);

  return (
    <>
      {items && (
        <ScrollArea.Autosize maxHeight={200} offsetScrollbars w={"100%"}>
          <Paper style={{ display: "flex", flexDirection: "column", gap: 8 }}>
            {items.length > 0 &&
              items.map(({ name, id }) => (
                <Item
                  name={name}
                  id={id}
                  key={id}
                  loadingEdit={
                    loadingUpdateCategory ||
                    loadingUpdateSubCategory ||
                    loadingUpdateFeature
                  }
                  loadingDelete={
                    loadingDeleteCategory ||
                    loadingDeleteSubCategory ||
                    loadingDeleteFeature
                  }
                  updatedCategory={updatedCategory}
                  updatedSubCategory={updatedSubCategory}
                  updatedFeature={updatedFeature}
                  editItem={(name, id) => {
                    switch (type) {
                      case "Categories":
                        updateCategory({
                          variables: {
                            updateCategoryId: id,
                            category: {
                              name: name,
                            },
                          },
                        });

                        updatedCategory && refetchCategories();
                        break;
                      case "Subcategories":
                        updateSubCategory({
                          variables: {
                            updateSubCategoryId: id,
                            subcategory: {
                              name: name,
                            },
                          },
                        });

                        updatedSubCategory && refetchSubCategories();
                        break;
                      case "Features":
                        updateFeature({
                          variables: {
                            updateFeatureId: id,
                            feature: {
                              name: name,
                            },
                          },
                        });
                        break;
                    }
                  }}
                  deleteItem={(id) => {
                    switch (type) {
                      case "Categories":
                        deleteCategory({
                          variables: {
                            deleteCategoryId: id,
                          },
                        });

                        deletedCategory && refetchCategories();
                        break;
                      case "Subcategories":
                        deleteSubCategory({
                          variables: {
                            deleteSubCategoryId: id,
                          },
                        });

                        deletedSubCategory && refetchSubCategories();
                        break;
                      case "Features":
                        deleteFeature({
                          variables: {
                            deleteFeatureId: id,
                          },
                        });

                        deletedFeature && refetchFeatures();
                        break;
                    }
                  }}
                />
              ))}

            {items.length <= 0 && (
              <Text
                color="gray"
                style={{ width: "100%", textAlign: "center" }}
                className="light"
              >
                {`No ${type}`}
              </Text>
            )}
          </Paper>
        </ScrollArea.Autosize>
      )}
    </>
  );
};
export default Template;
