import React, { useEffect } from "react";
import { Text, Button, Notification } from "@mantine/core";
import { showNotification } from "@mantine/notifications";
import { Icon } from "@iconify/react";
import {
  useGetProjects,
  useMe,
  useRevokeAccess,
} from "../../../shared/service";
import { useRoutingState } from "../../../shared/context/routing/RoutingState";

const RemoveUser = ({
  user,
  refetch,
  projectId,
  close,
}: {
  user:
    | { id: string; role: "ADMIN" | "MEMBER"; name: string; roleId: string }
    | undefined;
  refetch: any;
  close: () => void;
  projectId: string;
}) => {
  const { data: me } = useMe();
  const { dispatch } = useRoutingState();
  const { data, revokeAccess, error, loading } = useRevokeAccess();

  useEffect(() => {
    if (data && user?.id !== me?.me?.id) {
      showNotification({
        message: "Successfully revoked user access from the project",
        color: "green",
      });
      refetch();
      close();
      return;
    }
    if (data) {
      dispatch({
        type: "routing",
        payload: {
          route: "dashboard",
        },
      });
      showNotification({
        message: "Successfully removed yourself from the project",
        color: "green",
      });

      refetch();
      close();
    }
  }, [data]);

  return (
    <>
      {error && (
        <Notification
          icon={<Icon width={18} height={18} icon={"ph:x"} />}
          color="red"
          disallowClose
        >
          {error.message}
        </Notification>
      )}
      <Text>
        Are you sure you want to remove{" "}
        {user?.id === me?.me?.id ? "Your" : user?.name} access from this
        project?
      </Text>

      <div
        style={{
          display: "flex",
          width: "100%",
          alignItems: "center",
          justifyContent: "flex-end",
          gap: "10px",
          marginTop: "10px",
        }}
      >
        <Button
          variant="light"
          color={"gray"}
          onClick={() => close()}
          disabled={loading}
          className="cancel"
        >
          Cancel
        </Button>
        <Button
          variant="light"
          color={"red"}
          className="danger"
          onClick={() => {
            revokeAccess({
              variables: {
                project: {
                  id: projectId,
                },
                accessId: user?.roleId,
              },
            });
          }}
          loading={loading}
        >
          {user?.id === me?.me?.id ? "Leave" : "Revoke"}
        </Button>
      </div>
    </>
  );
};
export default RemoveUser;
