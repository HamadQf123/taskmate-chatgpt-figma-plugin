import React, { useEffect } from 'react'
import ProjectsTable from '../../components/table/ProjectsTable'

const Dashboard = () => {
  useEffect(() => {
    parent.postMessage(
      {
        pluginMessage: {
          type: 'resize-plugin',
          size: { width: 714, height: 500 },
        },
      },
      '*'
    )
  }, [])

  return <ProjectsTable />
}
export default Dashboard
