import React, { useState, useEffect } from 'react'
import {
  Breadcrumbs,
  Anchor,
  Avatar,
  Popover,
  Text,
  Button,
  Modal,
  Loader,
  LoadingOverlay,
  Paper,
  useMantineTheme,
  ActionIcon,
  Group,
} from '@mantine/core'
import { showNotification } from '@mantine/notifications'
// import { Route } from "../../../shared/types";
import { useRoutingState } from '../../../shared/context/routing/RoutingState'
import { useAuthTokenState } from '../../../shared/context/authToken/AuthTokenState'
import { capitalizeSentence } from '../../../shared/utils'
import Invitations from '../invitations'
import { logout } from '../../../shared/utils/api'
import EditProfile from './EditProfile'
import { useMe } from '../../../shared/service'
import { Icon } from '@iconify/react'

type NavigationType = { title: any; route: any }

const homeItem: NavigationType = { title: 'Home', route: 'dashboard' }

const Header = () => {
  const { data: me } = useMe()
  const [loading] = useState(false)
  const [error, setError] = useState('')
  const [items, setItems] = useState<NavigationType[]>([homeItem])
  const { state, dispatch } = useRoutingState()
  const { state: authTokenState, dispatch: authTokenDispatch } =
    useAuthTokenState()
  const [isOpen, setOpen] = useState(false)
  const [isEditOpen, setEditOpen] = useState(false)
  const theme = useMantineTheme()
  const [minimize, SetMinimize] = useState(false)

  useEffect(() => {
    minimize &&
      parent.postMessage(
        {
          pluginMessage: {
            type: 'minimize-plugin',
            size: {
              width: state.route === 'login' ? 500 : 714,
              height: 63,
            },
          },
        },
        '*'
      )

    !minimize &&
      parent.postMessage(
        {
          pluginMessage: {
            type: 'maximize-plugin',
            size: {
              width: state.route === 'login' ? 500 : 714,
              height: state.route === 'login' ? 400 : 500,
            },
          },
        },
        '*'
      )
  }, [minimize])

  useEffect(() => {
    setItems([
      homeItem,
      ...(state?.route === 'issue' &&
      items?.find((item) => item.route === 'project')
        ? [
            {
              title:
                items?.find((item) => item.route === 'project')?.title ?? '',
              route:
                items?.find((item) => item.route === 'project')?.route ?? '',
            },
          ]
        : []),
      ...(state.route !== 'dashboard'
        ? [
            {
              ...state,
              title: capitalizeSentence(state.route),
            },
          ]
        : []),
    ])
  }, [state])

  useEffect(() => {
    if (error)
      showNotification({
        message: error,
        color: 'red',
      })
  }, [error])

  return (
    <header style={{ marginLeft: 10, marginRight: 10, marginTop: 10 }}>
      <LoadingOverlay visible={loading} title="Logging out" />
      <Modal
        opened={isOpen}
        onClose={() => setOpen(false)}
        title={
          'Pending Projects Invitations (' + me?.me?.invitations?.length + ')'
        }
      >
        {loading ? (
          <Loader size={'sm'} />
        ) : (
          <Invitations me={me} close={() => setOpen(false)} />
        )}
      </Modal>
      <Modal
        opened={isEditOpen}
        onClose={() => setEditOpen(false)}
        title={'Profile'}
      >
        <EditProfile />
      </Modal>
      <Paper
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'space-between',
          borderBottom: `1px solid ${theme.colors.dark[4]}`,
        }}
        className="dark-secondary-bg"
        pl={'sm'}
        pr={'sm'}
      >
        {state.route === 'login' && <Text>Login</Text>}
        {state.route === 'register' && <Text>Register</Text>}

        {state.route !== 'login' && state.route !== 'register' && (
          <Breadcrumbs>
            {items
              ?.filter((item, i) => {
                if (!minimize) {
                  return true
                } else {
                  return i === items?.length - 1
                }
              })
              .map((item, index) =>
                index === items?.length - 1 || minimize ? (
                  <Text>{item?.title}</Text>
                ) : (
                  <Anchor
                    href={'#'}
                    key={index}
                    onClick={() => {
                      dispatch({
                        type: 'routing',
                        payload: {
                          route: item.route,
                        },
                      })
                    }}
                  >
                    {item.title}
                  </Anchor>
                )
              )}
          </Breadcrumbs>
        )}

        {!me && (
          <ActionIcon
            variant="light"
            color="gray"
            onClick={() => {
              SetMinimize(!minimize)
            }}
            title={minimize ? 'Maximize' : 'Minimize'}
          >
            <Icon
              icon={minimize ? 'ph:cards-thin' : 'ph:minus-thin'}
              height="14px"
              width="14px"
            />
          </ActionIcon>
        )}

        {me && (
          <Group spacing={minimize ? 10 : 16}>
            <ActionIcon
              variant="light"
              color="gray"
              onClick={() => {
                SetMinimize(!minimize)
              }}
              title={minimize ? 'Maximize' : 'Minimize'}
            >
              <Icon
                icon={minimize ? 'ph:cards-thin' : 'ph:minus-thin'}
                height="14px"
                width="14px"
              />
            </ActionIcon>
            <Popover
              width={250}
              position="bottom-end"
              shadow="md"
              onOpen={() => minimize && SetMinimize(!minimize)}
            >
              <Popover.Target>
                <div
                  style={{
                    position: 'relative',
                  }}
                >
                  <Avatar radius="xl" />
                  {/* show invitation alert */}
                  {me?.me?.invitations?.length > 0 && (
                    <span
                      style={{
                        height: 7,
                        width: 7,
                        background: 'red',
                        borderRadius: '50%',
                        position: 'absolute',
                        top: 6,
                        right: 0,
                        display: 'block',
                      }}
                    />
                  )}
                </div>
              </Popover.Target>
              <Popover.Dropdown
                style={{
                  width: '250px',
                }}
              >
                <Text size="sm">{me?.me?.username}</Text>
                {/* show invitations button */}
                {me?.me?.invitations?.length > 0 && (
                  <div
                    style={{
                      margin: '7px 0',
                    }}
                  >
                    <Anchor onClick={() => setOpen(true)}>
                      See projects invitations ({me?.me?.invitations?.length})
                    </Anchor>
                  </div>
                )}
                <div
                  style={{
                    margin: '7px 0',
                  }}
                >
                  <Anchor onClick={() => setEditOpen(true)}>Profile</Anchor>
                </div>
                <Button
                  variant="light"
                  className="danger"
                  size="xs"
                  onClick={async () => {
                    setError('')
                    const data = await logout(
                      authTokenState?.token?.refreshToken ?? ''
                    )
                    if (!data) setError('Failed to logout, please try again')
                    if (data?.logout?.message) {
                      authTokenDispatch({
                        type: 'delete_auth_token',
                        payload: {
                          tokenType: 'access',
                          token: { accessToken: null, refreshToken: null },
                        },
                      })
                      return
                    }
                  }}
                  loading={loading}
                >
                  Logout
                </Button>
              </Popover.Dropdown>
            </Popover>
          </Group>
        )}
      </Paper>
    </header>
  )
}
export default Header
