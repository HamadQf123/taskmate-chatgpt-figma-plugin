import React, {
  Dispatch,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from 'react'
import { StateProviderProps } from '../types'

const IssueIdContext = createContext(
  {} as {
    issueId: string
    setIssueId: Dispatch<SetStateAction<string>>
  }
)

const IssueIdProvider = ({ children }: StateProviderProps) => {
  const [issueId, setIssueId] = useState<string>('')

  return (
    <IssueIdContext.Provider value={{ issueId, setIssueId }}>
      {children}
    </IssueIdContext.Provider>
  )
}

export default IssueIdProvider
export const useIssueIdContext = () => useContext(IssueIdContext)
