import { Icon } from "@iconify/react";
import { showNotification } from "@mantine/notifications";
import {
  Anchor,
  Button,
  Group,
  Modal,
  TextInput,
  Text,
  ActionIcon,
  Textarea,
  Select,
  Notification,
} from "@mantine/core";
import React, { useEffect, useState } from "react";
import { TabProps } from ".";
import {
  useEditIssue,
  useGetIssue,
  useGetProject,
} from "../../../../shared/service";
import { issueTypes } from "../../../../shared/data";
import AccordionTemplate from "../../accordion";
import EditRow from "../edit-row";
import Row from "../row";
import { useFigmaContext } from "../../../../shared/context/figma/figmaProvider";

const Details = ({ updateIssue, loading, issue, editAll }: TabProps) => {
  const [opened, setOpened] = useState(false);
  const { state } = useFigmaContext();
  const { refetch } = useGetIssue(issue?.id ?? "");
  const { refetch: refetchProjects } = useGetProject(issue?.project?.id ?? "");
  // console.log(issue);
  const {
    editIssue,
    loading: editLoading,
    data,
    error: editError,
  } = useEditIssue();

  const [selectedNode, setSelectedNode] = useState<any>(
    issue?.elementId
      ? {
          id: issue?.elementId,
          name: issue?.elementName.name,
          parent: {
            id: issue?.linkedFrame?.id,
            name: issue?.linkedFrame?.name,
          },
        }
      : undefined
  );

  useEffect(() => {
    if (issue?.elementName) {
      setSelectedNode({
        id: issue?.elementId,
        name: issue?.elementName.name,
        parent: {
          id: issue?.linkedFrame?.id,
          name: issue?.linkedFrame?.name,
        },
      });
    }
  }, [issue]);

  const [error, setError] = useState("");
  onmessage = (event) => {
    if (event.data.pluginMessage.type === "send-selection" && opened) {
      if (event.data.pluginMessage.selectionNodes.length > 1) {
        return setError(
          "You cannot link more than one Figma element to an issue"
        );
      }

      if (event.data.pluginMessage.selectionNodes.length === 1) {
        setError("");
        updateIssue({
          elementId: event.data.pluginMessage.selectionNodes[0]?.id,
          elementName: {
	    fileId: event.data.pluginMessage.selectionNodes[0]?.id,
            name: event.data.pluginMessage.selectionNodes[0]?.name,
          },
          linkedFrame: {
            id: event.data.pluginMessage.selectionNodes[0]?.parent?.id,
            name: event.data.pluginMessage.selectionNodes[0]?.parent?.name,
          },
          children: event.data.pluginMessage.selectionNodes[0].children,
          fileName: event.data.pluginMessage.selectionNodes[0].fileName,
          file: state?.file,
        });
        return setSelectedNode(event.data.pluginMessage.selectionNodes[0]);
      }

      setError("");
    }
  };

  // console.log(issue, "this is issue");
  useEffect(() => {
    if (data) {
      refetch();
      refetchProjects();
      showNotification({
        message: "Successfully update the field",
        color: "green",
      });
      setOpened(false);
    }
  }, [data, editError]);

  return (
    <div>
      <Modal
        opened={opened && !loading}
        onClose={() => setOpened(false)}
        title={
          issue?.__typename
            ? "Change connected Figma Element"
            : "Connect a Figma Element"
        }
      >
        {editError && (
          <Notification
            icon={<Icon width={18} height={18} icon={"ph:x"} />}
            color="red"
            disallowClose
          >
            {editError.message}
          </Notification>
        )}
        <Text>
          {issue?.__typename
            ? "Please select an element on Figma canvas to change the linked element"
            : "Please select an element on Figma canvas to link to this issue"}
        </Text>

        <Text size={"md"} style={{ marginBottom: 10 }}>
          {selectedNode ? (
            <span>
              Selected Element: <strong>{selectedNode?.name}</strong>
            </span>
          ) : (
            <strong>No Selection</strong>
          )}
        </Text>

        {error && (
          <Text color={"red"} size={"sm"}>
            {error}
          </Text>
        )}

        <Group
          align={"flex-end"}
          style={{
            display: "flex",
            justifyContent: "flex-end",
          }}
        >
          <Button
            variant="light"
            color={"red"}
            onClick={() => setOpened(false)}
          >
            Cancel
          </Button>
          <Button
            variant="light"
            disabled={!!error}
            loading={editLoading}
            onClick={() => {
              if (!issue?.__typename || editAll) {
                setOpened(false);
                return;
              }
              if (selectedNode?.name && issue?.id) {
                editIssue({
                  variables: {
                    updateIssueId: issue?.id,
                    issue: {
                      elementId: selectedNode?.id,
                      elementName: selectedNode?.name,
                    },
                  },
                });
              }
            }}
          >
            Save
          </Button>
        </Group>
      </Modal>

      <AccordionTemplate title="Figma Information">
        <Row
          title="Figma Element Name"
          name="linkedElement"
          required
          info="Figma element name"
          childRowStyle={{
            padding: 7,
          }}
        >
          {selectedNode ? (
            <div
              style={{
                display: "flex",
                justifyContent: "space-between",
                alignItems: "center",
              }}
            >
              <Anchor
                onClick={() => {
                  if (issue?.file === state?.file) {
                    parent.postMessage(
                      {
                        pluginMessage: {
                          type: "select",
                          nodeId: selectedNode.id,
                        },
                      },
                      "*"
                    );
                  } else {
                    showNotification({
                      message:
                        "The issue wasn't connected to an element on this file",
                      color: "red",
                    });
                  }
                }}
              >
                {selectedNode.name}
              </Anchor>
              <ActionIcon
                onClick={() => {
                  setOpened(true);
                }}
              >
                <Icon icon="ph:note-pencil" height="14px" width="14px" />
              </ActionIcon>
            </div>
          ) : (
            <Anchor
              onClick={() => {
                parent.postMessage(
                  {
                    pluginMessage: {
                      type: "get-selection",
                    },
                  },
                  "*"
                );
                setOpened(true);
              }}
            >
              Connect Figma Element
            </Anchor>
          )}
        </Row>
      </AccordionTemplate>
    </div>
  );
};
export default Details;
