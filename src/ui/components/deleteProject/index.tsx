import React from "react";
import { Text, Button, Notification } from "@mantine/core";
import { useDeleteProject } from "../../../shared/service";
import { Icon } from "@iconify/react";

const DeleteProject = ({
  id,
  close,
  refetch,
}: {
  id: string;
  close: (success?: boolean) => void;
  refetch: () => void;
}) => {
  const { deleteProject, loading, error } = useDeleteProject();
  return (
    <>
      {error && (
        <Notification
          icon={<Icon width={18} height={18} icon={"ph:x"} />}
          color="red"
          disallowClose
        >
          {error?.message}
        </Notification>
      )}
      <Text>
        Are you sure you want to delete this project? All its issues will be
        deleted.
      </Text>

      <div
        style={{
          display: "flex",
          width: "100%",
          alignItems: "center",
          justifyContent: "flex-end",
          gap: "10px",
          marginTop: "10px",
        }}
      >
        <Button
          variant="outline"
          onClick={() => close(false)}
          disabled={loading}
          className="cancel"
        >
          Cancel
        </Button>
        <Button
          color={"red"}
          onClick={() => {
            deleteProject({
              variables: {
                deleteProjectId: id,
              },
            }).then(() => {
              refetch();
              close(true);
            });
          }}
          loading={loading}
          className="danger"
        >
          Delete
        </Button>
      </div>
    </>
  );
};
export default DeleteProject;
