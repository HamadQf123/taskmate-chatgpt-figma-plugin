import { Select, Text, Modal, Anchor } from "@mantine/core";
import React, { useEffect, useState } from "react";
import { TabProps } from ".";
import AccordionTemplate from "../accordion";
import EditRow from "./edit-row";
import Row from "./row";
import useCategoryOptions from "../../../shared/hooks/useCategoryOptions";
import useSubCategoryOptions from "./../../../shared/hooks/useSubCategoryOptions";
import useFeatureOptions from "./../../../shared/hooks/useFeatureOptions";
import { showNotification } from "@mantine/notifications";
import CategorySubcategoryFeature from "../category-subcategory-feature";

const Hierachy = ({ updateIssue, loading, issue, editAll }: TabProps) => {
  const { categoriesError, loadingCategories, categoryOptions } =
    useCategoryOptions();
  const { subCategoriesError, loadingSubCategories, subCategoryOptions } =
    useSubCategoryOptions();
  const { featuresError, loadingFeatures, featureOptions } =
    useFeatureOptions();

  const [opened, setOpened] = useState("");

  useEffect(() => {
    {
      categoriesError &&
        showNotification({
          message: categoriesError.message,
          color: "red",
        });
    }
    {
      subCategoriesError &&
        showNotification({
          message: subCategoriesError.message,
          color: "red",
        });
    }

    {
      featuresError &&
        showNotification({
          message: featuresError.message,
          color: "red",
        });
    }
  }, [categoriesError, subCategoriesError, featuresError]);

  return (
    <>
      <Modal
        opened={!!opened}
        onClose={() => setOpened("")}
        title="Project Settings"
      >
        <CategorySubcategoryFeature defaultValue={opened} />
      </Modal>
      <AccordionTemplate title="Hierarchy">
        <Row title="Category" name="category" required info="Category">
          {(!issue?.__typename || editAll) && (
            <Select
              disabled={loading || loadingCategories}
              name="category"
              id="category"
              value={
                categoryOptions?.find(
                  (option) => option.value === issue?.category?.id
                )?.value
              }
              nothingFound={
                <div
                  style={{
                    padding: "1rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <Text color={"gray"}>No data found</Text>
                  <Anchor onClick={() => setOpened("categories")}>
                    Create
                  </Anchor>
                </div>
              }
              searchable
              placeholder="Select Category"
              data={categoryOptions}
              onChange={(value) => {
                updateIssue({
                  category: {
                    id: parseInt(value ?? ""),
                  },
                });
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="category"
              type="select"
              placeholder="Category"
              required
              initialValue={
                issue?.category?.name ??
                categoryOptions?.find(
                  (category) => category?.value === issue?.category?.id
                )?.label
              }
              selectOptions={categoryOptions}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
        <Row title="Subcategory" name="subCategory" required info="Subcategory">
          {(!issue?.__typename || editAll) && (
            <Select
              disabled={loading || loadingSubCategories}
              name="subCategory"
              id="subCategory"
              value={
                subCategoryOptions?.find(
                  (option) => option.value === issue?.subCategory?.id
                )?.value
              }
              nothingFound={
                <div
                  style={{
                    padding: "1rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <Text color={"gray"}>No data found</Text>
                  <Anchor onClick={() => setOpened("subcategories")}>
                    Create
                  </Anchor>
                </div>
              }
              searchable
              placeholder="Select"
              data={subCategoryOptions}
              onChange={(value) => {
                updateIssue({
                  subCategory: {
                    id: parseInt(value ?? ""),
                  },
                });
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="subCategory"
              type="select"
              placeholder="Subcategory"
              required
              initialValue={
                issue?.subCategory?.name ??
                subCategoryOptions?.find(
                  (subcategory) => subcategory?.value === issue?.subCategory?.id
                )?.label
              }
              selectOptions={subCategoryOptions}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
        <Row title="Feature" name="feature" required isLastChild info="Feature">
          {(!issue?.__typename || editAll) && (
            <Select
              disabled={loading || loadingFeatures}
              name="feature"
              id="feature"
              value={
                featureOptions?.find(
                  (option) => option.value === issue?.feature?.id
                )?.value
              }
              nothingFound={
                <div
                  style={{
                    padding: "1rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <Text color={"gray"}>No data found</Text>
                  <Anchor onClick={() => setOpened("features")}>Create</Anchor>
                </div>
              }
              searchable
              placeholder="Features"
              data={featureOptions}
              onChange={(value) => {
                updateIssue({
                  feature: {
                    id: parseInt(value ?? ""),
                  },
                });
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="feature"
              type="select"
              placeholder="Feature"
              required
              initialValue={
                issue?.feature?.name ??
                featureOptions?.find(
                  (feature) => feature?.value === issue?.feature?.id
                )?.label
              }
              selectOptions={featureOptions}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
      </AccordionTemplate>
    </>
  );
};
export default Hierachy;
