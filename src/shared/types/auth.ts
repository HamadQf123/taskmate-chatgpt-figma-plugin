export interface LoginInput {
  email: string
  password: string
}

export interface LoginData {
  login: LoginResponse
}

export interface LoginResponse {
  id: string
  accessToken: string
  refreshToken: string
}

export interface RegisterInput {
  name: string
  email: string
  username: string
  password: string
}

export interface AuthToken {
  accessToken: string | null
  refreshToken: string | null
}

export interface AuthTokenStateType {
  tokenType: 'access' | 'refresh'
  token: AuthToken
}

export interface AuthTokenAction {
  type: 'save_auth_token' | 'get_auth_token' | 'delete_auth_token'
  payload: AuthTokenStateType
}

export interface AuthTokenContextType {
  state: AuthTokenStateType
  dispatch: (action: AuthTokenAction) => void
}
