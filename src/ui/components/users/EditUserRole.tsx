import { Icon } from '@iconify/react'
import { Select, Text, Group, Button, Notification } from '@mantine/core'
import { showNotification } from '@mantine/notifications'
import React, { useEffect, useState } from 'react'
import { useUpdateUserRole } from '../../../shared/service'

const EditUserRole = ({
  user,
  refetch,
}: {
  user:
    | { id: string; role: 'ADMIN' | 'MEMBER'; name: string; roleId: string }
    | undefined
  refetch: any
  close: () => void
}) => {
  const [role, setRole] = useState(user?.role ?? '')
  const { loading, data, error, updateRole } = useUpdateUserRole()

  useEffect(() => {
    if (data) {
      showNotification({
        message: 'Successfully update the role',
        color: 'green',
      })
      refetch()
      close()
    }
  }, [data])

  return (
    <form
      className="role-selector"
      onSubmit={(e) => {
        e.preventDefault()
        // submit data
        updateRole({
          variables: {
            accessId: user?.roleId,
            user: {
              id: user?.id,
            },
            role,
          },
        })
      }}
    >
      {error && (
        <Notification
          icon={<Icon width={18} height={18} icon={'ph:x'} />}
          color="red"
          disallowClose
        >
          {error?.message}
        </Notification>
      )}
      <Select
        disabled={loading}
        onChange={(data) => {
          setRole(data ?? '')
        }}
        defaultValue={user?.role ?? ''}
        id="type"
        name="type"
        style={{
          border: 'none',
          borderRadius: 0,
        }}
        nothingFound={
          <div
            style={{
              padding: '1rem',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text color={'gray'}>No data found</Text>
          </div>
        }
        searchable
        placeholder="Select Type"
        data={[
          { label: 'ADMIN', value: 'ADMIN' },
          {
            label: 'MEMBER',
            value: 'MEMBER',
          },
        ]}
      />
      <Group
        align={'right'}
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          marginTop: 10,
        }}
      >
        <Button type="submit" variant="light" color={'gray'} disabled={loading}>
          Cancel
        </Button>
        <Button type="submit" variant="light" loading={loading}>
          Update
        </Button>
      </Group>
    </form>
  )
}
export default EditUserRole
