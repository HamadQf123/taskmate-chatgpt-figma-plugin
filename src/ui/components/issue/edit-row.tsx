import React, { useEffect, useState } from "react";
import {
  Group,
  Loader,
  TextInput,
  Text,
  ActionIcon,
  Textarea,
  Select,
  Popover,
  Paper,
  Modal,
  Anchor,
  MultiSelect,
} from "@mantine/core";
import { showNotification } from "@mantine/notifications";
import {
  useEditIssue,
  useGetIssue,
  useGetProjects,
} from "../../../shared/service";
import { Icon } from "@iconify/react";
import { format } from "date-fns";
import { DayPicker } from "react-day-picker";
import "react-day-picker/dist/style.css";
import CategorySubcategoryFeature from "../category-subcategory-feature";

interface Props {
  type: "textarea" | "select" | "input" | "date";
  required?: boolean;
  initialValue: string;
  projectId: string;
  issueId: string;
  selectOptions?: { label: string; value: string }[];
  placeholder: string;
  name:
    | "description"
    | "label"
    | "status"
    | "type"
    | "acceptanceCriteria"
    | "externalLink"
    | "associatedCr"
    | "notes"
    | "category"
    | "subCategory"
    | "feature"
    | "assignee"
    | "started"
    | "completedAt"
    | "persona";
}

const EditRow = ({
  type,
  required,
  initialValue,
  issueId,
  name,
  selectOptions,
  placeholder,
}: Props) => {
  const [opened, setOpened] = useState("");
  const [editMode, setEditMode] = useState(false);
  const [value, setValue] = useState<any>(initialValue);
  const { refetch } = useGetIssue(issueId);
  const { refetch: refetchProjects } = useGetProjects({
    params: {
      page: 1,
      pageSize: 10,
      paging: true,
    },
  });
  const { editIssue, loading, data, error } = useEditIssue();

  const submitEdits = () => {
    if (value) {
      editIssue({
        variables: {
          updateIssueId: issueId,
          issue: {
            [name]:
              name !== "assignee"
                ? name === "category" ||
                  name === "subCategory" ||
                  name === "feature"
                  ? {
                      id: selectOptions
                        ?.find(
                          (option) =>
                            option.label === value || option.value === value
                        )
                        ?.value.toString(),
                      name: selectOptions?.find(
                        (option) =>
                          option.label === value || option.value === value
                      )?.label,
                    }
                  : value
                : {
                    id: selectOptions?.find(
                      (option) =>
                        option.label === value || option.value === value
                    )?.value,
                  },
          },
        },
      });
    }
  };

  useEffect(() => {
    if (data) {
      refetch();
      refetchProjects();
      showNotification({
        message: "Successfully update the field",
        color: "green",
      });
      setEditMode(false);
    }
    if (error) {
      showNotification({
        title: "Failed to update field",
        message: error.message,
        color: "red",
      });
    }
  }, [data, error]);

  return (
    <Paper
      className="issue-row"
      style={{
        display: "flex",
        justifyContent: "space-between",
        height: "100%",
        width: "100%",
        gap: 7,
      }}
    >
      <Modal
        opened={!!opened}
        onClose={() => setOpened("")}
        title="Project Settings"
      >
        <CategorySubcategoryFeature defaultValue={opened} />
      </Modal>
      {editMode ? (
        <>
          {selectOptions && type === "select" && name !== "persona" && (
            <Select
              disabled={loading}
              onChange={(data) => {
                setValue(data);
              }}
              id="type"
              required={required}
              name={name}
              autoFocus
              style={{
                border: "none",
                display: "block",
                width: "100%",
                height: "100%",
              }}
              defaultValue={
                selectOptions?.find(
                  (option) => option.label === value || option.value === value
                )?.value
              }
              nothingFound={
                <div
                  style={{
                    padding: "1rem",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    flexDirection: "column",
                  }}
                >
                  <Text color={"gray"}>No data found</Text>
                  <Anchor
                    onClick={() =>
                      setOpened(
                        name === "category"
                          ? "categories"
                          : name === "subCategory"
                          ? "subcategories"
                          : name === "feature"
                          ? "featires"
                          : ""
                      )
                    }
                  >
                    Create
                  </Anchor>
                </div>
              }
              searchable
              placeholder="Select Type"
              data={selectOptions}
            />
          )}
          {selectOptions && type === "select" && name === "persona" && (
            <MultiSelect
              disabled={loading}
              onChange={(data) => {
                setValue(data);
              }}
              id="type"
              required={required}
              name={name}
              autoFocus
              style={{
                border: "none",
                display: "block",
                width: "100%",
                height: "100%",
              }}
              defaultValue={value}
              searchable
              placeholder="Select Type"
              data={selectOptions}
            />
          )}
          {type === "input" && (
            <TextInput
              disabled={loading}
              name={name}
              required={required}
              id={name}
              value={value}
              autoFocus
              style={{
                borderRadius: 0,
                display: "block",
                width: "100%",
                height: "100%",
              }}
              type={name === "externalLink" ? "url" : "text"}
              placeholder={placeholder}
              onChange={(e) => {
                setValue(e.target.value);
              }}
            />
          )}
          {type === "textarea" && (
            <Textarea
              disabled={loading}
              name={name}
              id={name}
              value={value}
              required={required}
              autoFocus
              style={{
                borderRadius: 0,
                display: "block",
                width: "100%",
                height: "100%",
              }}
              placeholder={placeholder}
              onChange={(e) => {
                setValue(e.target.value);
              }}
            />
          )}
          {type === "date" && (
            <Popover
              width={"100%"}
              position="bottom"
              withArrow
              shadow="md"
              disabled={loading}
            >
              <Popover.Target>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    alignItems: "center",
                    width: "100%",
                    height: "100%",
                    padding: "0 7px",
                  }}
                >
                  {!value && (
                    <Text size={"md"} color={"lightgray"}>
                      Select Date
                    </Text>
                  )}
                  {value && (
                    <span>{format(new Date(value ?? ""), "dd-MM-yyyy")}</span>
                  )}
                  <Icon icon="ph:calendar" />
                </div>
              </Popover.Target>
              <Popover.Dropdown
                style={{
                  width: "100%",
                }}
              >
                <div
                  style={{
                    position: "relative",
                    width: "100%",
                    cursor: "pointer",
                  }}
                >
                  <DayPicker
                    mode="single"
                    selected={value ?? null}
                    onSelect={(selected) => {
                      setValue(selected);
                    }}
                  />
                </div>
              </Popover.Dropdown>
            </Popover>
          )}

          <Group
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              flexWrap: "nowrap",
              gap: 5,
              paddingRight: 7,
            }}
          >
            {loading && <Loader size={16} />}
            {!loading && (
              <>
                <ActionIcon
                  variant="light"
                  size="sm"
                  color="blue"
                  onClick={() => setEditMode(false)}
                  title="Close"
                  className="danger"
                >
                  <Icon
                    icon="ph:x"
                    height="14px"
                    width="14px"
                    className="danger"
                  />
                </ActionIcon>
                <ActionIcon
                  onClick={() => submitEdits()}
                  variant="light"
                  size="sm"
                  color="green"
                  title="Save"
                  className="success"
                >
                  <Icon
                    icon="ph:check"
                    height="14px"
                    width="14px"
                    className="success"
                  />
                </ActionIcon>
              </>
            )}
          </Group>
        </>
      ) : (
        <Group
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "space-between",
            flexWrap: "nowrap",
            gap: 5,
            width: "100%",
            padding: 7,
          }}
        >
          <Text
            style={{ width: "100%" }}
            opacity={!value ? "0.5" : "1"}
            size="sm"
          >
            {type === "date" && value
              ? format(new Date(value ?? ""), "dd-MM-yyyy")
              : type === "select"
              ? selectOptions?.find(
                  (option) => option.label === value || option.value === value
                )?.label
              : !value
              ? "—"
              : value}
          </Text>
          <ActionIcon onClick={() => setEditMode(true)} className="link">
            <Icon
              icon="ph:note-pencil"
              height="14px"
              width="14px"
              className="link"
            />
          </ActionIcon>
        </Group>
      )}
    </Paper>
  );
};
export default EditRow;
