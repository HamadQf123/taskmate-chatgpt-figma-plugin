import React from 'react'
import { TabProps } from '..'
import IssueInformation from './issue-information'
import FigmaInformation from './figma-information'

const Details = ({ updateIssue, loading, issue, editAll }: TabProps) => {
  return (
    <div style={{ display: 'flex', flexDirection: 'column', gap: 8 }}>
      <FigmaInformation
        issue={issue}
        editAll={editAll}
        loading={loading}
        updateIssue={updateIssue}
      />

      <IssueInformation
        issue={issue}
        editAll={editAll}
        loading={loading}
        updateIssue={updateIssue}
      />
    </div>
  )
}
export default Details
