import React, { ReactNode } from 'react'
import Header from '../components/header'
import { useViewportSize } from '@mantine/hooks'

const Layout = ({ children }: { children: ReactNode }) => {
  const { height } = useViewportSize()

  return (
    <>
      <Header />
      {height !== 63 && children}
    </>
  )
}
export default Layout
