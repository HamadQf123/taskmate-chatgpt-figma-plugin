import {
  ApolloClient,
  ApolloProvider as Provider,
  createHttpLink,
  InMemoryCache,
} from '@apollo/client'
import React, { useEffect } from 'react'
import { StateProviderProps } from './../../types'
import { useAuthTokenState } from './../authToken/AuthTokenState'
import { setContext } from '@apollo/client/link/context'
import { API } from '../../utils/api'

const createApolloClient = (token: any) => {
  const httpLink = createHttpLink({
    uri: API,
  })

  const authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        authorization: `Bearer ${token}`,
      },
    }
  })
  return new ApolloClient({
    link: authLink.concat(httpLink),
    cache: new InMemoryCache(),
  })
}

const ApolloProvider = ({ children }: StateProviderProps) => {
  const { state } = useAuthTokenState()

  const client = createApolloClient(state.token.accessToken)

  return <Provider client={client}>{children}</Provider>
}

export default ApolloProvider
