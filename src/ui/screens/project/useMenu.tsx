import React, { useState } from 'react'
import { Menu, Button } from '@mantine/core'
import { API_DOWNLOAD } from '../../../shared/utils/api'
import { useAuthTokenState } from '../../../shared/context/authToken/AuthTokenState'
import { useMe } from '../../../shared/service'
import { TheProject } from '../../../shared/types'

const useMenu = (data: any, open: () => void) => {
  const [fetching, setFetching] = useState(false)
  const [defaultValue, setDefaultValue] = useState<
    'categories' | 'subcategories' | 'features'
  >('categories')
  const accessToken = useAuthTokenState().state.token.accessToken
  const { data: me } = useMe()
  const [projectToEdit, setProjectToEdit] = useState<TheProject | undefined>()
  const [projectToDelete, setProjectToDelete] = useState<
    TheProject | undefined
  >()

  return {
    menu: (
      <Menu shadow="md" position="left-start" width={150}>
        <Menu.Target>
          <Button variant="light" color="gray" loading={fetching}>
            {fetching ? 'Downloding...' : 'More'}
          </Button>
        </Menu.Target>

        <Menu.Dropdown>
          <Menu.Label>Hierachy</Menu.Label>
          <Menu.Item
            onClick={() => {
              open()
              setDefaultValue('categories')
            }}
          >
            Categories
          </Menu.Item>
          <Menu.Item
            onClick={() => {
              open()
              setDefaultValue('subcategories')
            }}
          >
            Subcategories
          </Menu.Item>
          <Menu.Item
            onClick={() => {
              open()
              setDefaultValue('features')
            }}
          >
            Features
          </Menu.Item>

          <Menu.Divider />

          <Menu.Label>Project</Menu.Label>
          <Menu.Item
            onClick={() => {
              setFetching(true)

              try {
                fetch(`${API_DOWNLOAD}/${data?.getProject?.id}`, {
                  method: 'get',
                  headers: {
                    Authorization: `Bearer ${accessToken}`,
                  },
                })
                  .then((res) => res.blob())
                  .then((blob) => {
                    const link = document.createElement('a')
                    link.href = window.URL.createObjectURL(blob)
                    link.download = `${
                      data?.getProject?.name
                    }_${new Date().getTime()}.xlsx`
                    link.click()
                  })
                  .finally(() => {
                    setFetching(false)
                  })
              } catch (err) {
              }
            }}
          >
            Export to Excel
          </Menu.Item>
          <Menu.Item onClick={() => setProjectToEdit(data?.getProject)}>
            Edit
          </Menu.Item>

          {!!data?.getProject?.access?.find(
            (user: any) =>
              user?.user?.id === me?.me?.id && user?.role === 'ADMIN'
          ) && (
            <Menu.Item
              color="red"
              onClick={() => setProjectToDelete(data?.getProject)}
            >
              Delete
            </Menu.Item>
          )}
        </Menu.Dropdown>
      </Menu>
    ),

    defaultValue,
    projectToEdit,
    setProjectToEdit,
    projectToDelete,
    setProjectToDelete,
  }
}

export default useMenu
