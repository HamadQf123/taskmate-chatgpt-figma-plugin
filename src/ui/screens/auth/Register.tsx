import React, { useState, useEffect } from "react";
import {
  TextInput,
  Button,
  Group,
  Paper,
  PasswordInput,
  Text,
  Anchor,
  Modal,
  Notification,
  Space,
} from "@mantine/core";
import { useForm } from "@mantine/form";
import { passwordRequirements } from "./../../../shared/utils";
import { useRoutingState } from "../../../shared/context/routing/RoutingState";
import { useMutation } from "@apollo/client";
import { REGISTER } from "../../../graphql/mutations";
import { Icon } from "@iconify/react";

const Register = () => {
  const { dispatch } = useRoutingState();
  const [register, { loading, data, error }] = useMutation(REGISTER);
  const [opened, setOpened] = useState(false);

  useEffect(() => {
    parent.postMessage(
      {
        pluginMessage: {
          type: "resize-plugin",
          size: { width: 750, height: 500 },
        },
      },
      "*"
    );
  }, []);

  useEffect(() => {
    if (data) {
      setOpened(true);
    }
  }, [data]);

  const form = useForm({
    initialValues: {
      name: "",
      email: "",
      username: "",
      password: "",
    },

    validate: {
      name: (value) =>
        value.length < 2 ? "Name must have at least 2 letters" : null,
      email: (value) => (/^\S+@\S+$/.test(value) ? null : "Invalid email"),
      username: (value) =>
        /[ `!@#$%^&*()+\-=\[\]{};':"\\|,<>\/?~]/.test(value)
          ? "Username can only contain any special character except underscore"
          : null,
      password: (value) =>
        passwordRequirements.validate(value)
          ? null
          : "Password must contain atleast 8 characters including a number, a capital letter, a small letter, and a special character",
    },
  });

  return (
    <>
      <Modal
        opened={opened}
        onClose={() => setOpened(false)}
        title="You have successfully registered! Log in using your newly created account"
      >
        <Button
          style={{ width: "100%" }}
          onClick={() =>
            dispatch({ type: "routing", payload: { route: "login" } })
          }
        >
          Login
        </Button>
      </Modal>
      <Paper
        sx={{
          height: "calc(100% - 63px)",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          justifyContent: "center",
          gap: "10px",
          padding: "1rem 15%",
        }}
      >
        <Text size={"lg"} fw={"bold"}>
          Welcome to Dash
        </Text>

        {error ? (
          <Notification
            icon={<Icon width={18} height={18} icon={"ph:x"} />}
            color="red"
            disallowClose
          >
            {error.message}
          </Notification>
        ) : null}

        <form
          style={{
            width: "100%",
          }}
          onSubmit={form.onSubmit((values) =>
            register({
              variables: {
                user: {
                  name: values.name,
                  email: values.email,
                  username: values.username,
                  password: values.password,
                },
              },
            })
          )}
        >
          <TextInput
            withAsterisk
            label="Name"
            placeholder="Tony Stark"
            type={"text"}
            {...form.getInputProps("name")}
          />

          <Space h="sm" />

          <TextInput
            withAsterisk
            label="Email"
            placeholder="tony@stark.com"
            type={"email"}
            {...form.getInputProps("email")}
          />

          <Space h="sm" />

          <TextInput
            withAsterisk
            label="Username"
            placeholder="your unique username"
            type={"text"}
            {...form.getInputProps("username")}
          />

          <Space h="sm" />

          <Group
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-end",
              justifyContent: "center",
            }}
          >
            <PasswordInput
              withAsterisk
              label="Password"
              placeholder="Password"
              {...form.getInputProps("password")}
              style={{ width: "100%" }}
            />
          </Group>

          <Space h="sm" />

          <Group
            position="center"
            mt="20px"
            style={{ flexDirection: "column" }}
          >
            <Button type="submit" style={{ width: "50%" }} loading={loading}>
              Register
            </Button>
            <Text>
              Already has an account?&nbsp;
              <Anchor
                href="#"
                onClick={() =>
                  dispatch({ type: "routing", payload: { route: "login" } })
                }
              >
                Login
              </Anchor>
            </Text>
          </Group>
        </form>
      </Paper>
    </>
  );
};
export default Register;
