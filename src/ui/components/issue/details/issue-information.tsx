import { TextInput, Select, Textarea, Text } from '@mantine/core'
import React from 'react'
import { issueTypes } from '../../../../shared/data'
import AccordionTemplate from '../../accordion'
import EditRow from '../edit-row'
import Row from '../row'

const IssueInformation = ({
  issue,
  editAll,
  loading,
  updateIssue,
}: {
  issue: any
  editAll: boolean
  loading: boolean
  updateIssue: (value: any) => void
}) => {
  return (
    <>
      <AccordionTemplate title="Issue Information">
        <Row title="Label" name="label" required info="Issue label/title">
          {(!issue?.__typename || editAll) && (
            <TextInput
              disabled={loading}
              name="label"
              id="label"
              value={issue?.label ?? ''}
              placeholder="Label"
              onChange={(e) => {
                updateIssue({
                  label: e.target.value,
                })
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="label"
              type="input"
              placeholder="Label"
              required
              initialValue={issue?.label}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
        <Row title="Type" name="type" required info="Type of an issue">
          {(!issue?.__typename || editAll) && (
            <Select
              disabled={loading}
              onChange={(data) => {
                updateIssue({
                  type: data,
                })
              }}
              defaultValue={issue?.type ?? ''}
              id="type"
              name="type"
              style={{
                border: 'none',
                borderRadius: 0,
                display: 'block',
                width: '100%',
                height: '100%',
              }}
              nothingFound={
                <div
                  style={{
                    padding: '1rem',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                >
                  <Text color={'gray'}>No data found</Text>
                </div>
              }
              searchable
              placeholder="Select Type"
              data={issueTypes}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              selectOptions={issueTypes}
              name="type"
              type="select"
              placeholder="Select Type"
              required
              initialValue={issue?.type}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
        <Row
          title="Description"
          name="description"
          required
          info="Description of an issue"
        >
          {(!issue?.__typename || editAll) && (
            <Textarea
              disabled={loading}
              value={issue.description ?? ''}
              name="description"
              id="description"
              placeholder="Description"
              required
              onChange={(e) => {
                updateIssue({
                  description: e.target.value,
                })
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="description"
              type="textarea"
              placeholder="Description"
              required
              initialValue={issue?.description}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
        <Row
          title="Definition of Done"
          name="acceptanceCriteria"
          required
          info="Criteria for validate issue on resolution"
        >
          {(!issue?.__typename || editAll) && (
            <Textarea
              disabled={loading}
              name="acceptanceCriteria"
              id="acceptanceCriteria"
              required
              value={issue?.acceptanceCriteria ?? ''}
              placeholder="Definition of Done"
              onChange={(e) => {
                updateIssue({
                  acceptanceCriteria: e.target.value,
                })
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="type"
              type="textarea"
              placeholder="Definition of Done"
              required
              initialValue={issue?.acceptanceCriteria}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
        <Row
          title="External Link"
          name="externalLink"
          info="External link (such as jira or clickup) that relates to the issue"
        >
          {(!issue?.__typename || editAll) && (
            <TextInput
              disabled={loading}
              name="externalLink"
              id="externalLink"
              type={'url'}
              value={issue?.externalLink ?? ''}
              placeholder="External Link"
              onChange={(e) => {
                updateIssue({
                  externalLink: e.target.value,
                })
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="externalLink"
              type="input"
              placeholder="External Link"
              initialValue={issue?.externalLink}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
        <Row title="Associated CR" name="associatedCr" isLastChild>
          {(!issue?.__typename || editAll) && (
            <TextInput
              disabled={loading}
              name="associatedCr"
              id="associatedCr"
              value={issue?.associatedCr ?? ''}
              placeholder="Associated CR"
              onChange={(e) => {
                updateIssue({
                  associatedCr: e.target.value,
                })
              }}
            />
          )}
          {issue?.__typename && !editAll && (
            <EditRow
              name="associatedCr"
              type="input"
              placeholder="Associated CR"
              initialValue={issue?.associatedCr}
              projectId={issue?.project?.id}
              issueId={issue?.id}
            />
          )}
        </Row>
      </AccordionTemplate>
    </>
  )
}
export default IssueInformation
