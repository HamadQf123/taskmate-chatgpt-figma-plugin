import React, { useState } from 'react'
import { isExpired } from 'react-jwt'
import { showNotification } from '@mantine/notifications'
import { Container } from '@mantine/core'
import { useAuthTokenState } from '../shared/context/authToken/AuthTokenState'
import { useFigmaContext } from '../shared/context/figma/figmaProvider'
import { AuthToken } from '../shared/types'
import './App.css'
import Screens from './screens'
import { useRoutingState } from '../shared/context/routing/RoutingState'

const App = () => {
  const { dispatch: authTokenDispatch } = useAuthTokenState()
  const { state: routingState, dispatch: routingDispatch } = useRoutingState()
  const { state, dispatch } = useFigmaContext()

  onmessage = (event) => {
    if (event.data.pluginMessage.type === 'receiveFileName') {
      dispatch({
        type: 'ATTACH_FILE_NAME',
        payload: {
          ...state,
          fileName: event.data.pluginMessage.fileName,
        },
      })
    }

    if (event.data.pluginMessage.type === 'get-file-key') {
      const data = event.data.pluginMessage
      dispatch({
        type: 'ATTACH_FILE_KEY',
        payload: {
          ...state,
          fileKey: data?.fileKey,
        },
      })
    }

    if (event.data.pluginMessage.type === 'no-node-error') {
      const data = event.data.pluginMessage
      showNotification({
        message: data?.message ?? 'Element not found',
        color: 'red',
      })
    }

    // TODO: refactor and handle more edge cases
    if (event.data.pluginMessage.type === 'get-auth-token') {
      const tokenFromLocal: { token: string; type: string } =
        event.data.pluginMessage
      if (tokenFromLocal.token) {
        const token: AuthToken = JSON.parse(JSON.parse(tokenFromLocal.token))
        const { accessToken, refreshToken } = token
        if (accessToken && refreshToken) {
          if (isExpired(accessToken)) {
            return
          }
          if (isExpired(refreshToken)) {
            authTokenDispatch({
              type: 'delete_auth_token',
              payload: {
                tokenType: 'access',
                token: {
                  accessToken: null,
                  refreshToken: null,
                },
              },
            })
            return
          }
          authTokenDispatch({
            type: 'get_auth_token',
            payload: { tokenType: 'access', token: token },
          })
          routingDispatch({
            type: 'routing',
            payload: { ...routingState, loading: false },
          })
          return
        }
        return
      }
      routingDispatch({
        type: 'routing',
        payload: { ...routingState, loading: false },
      })
    }
  }

  return <Screens />
}

export default App
