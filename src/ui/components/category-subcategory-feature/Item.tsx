import { Icon } from "@iconify/react";
import { ActionIcon, Group, Paper, Text, TextInput } from "@mantine/core";
import React, { useEffect, useState } from "react";

export interface ItemType {
  name: string;
  id: string;
}

export interface ItemProps extends ItemType {
  editItem: (name: string, id: string) => void;
  deleteItem: (id: string) => void;
  loadingEdit?: boolean;
  loadingDelete?: boolean;
  updatedCategory?: any;
  updatedSubCategory?: any;
  updatedFeature?: any;
}

function Item({
  name,
  id,
  editItem,
  deleteItem,
  loadingEdit,
  loadingDelete,
  updatedCategory,
  updatedSubCategory,
  updatedFeature,
}: ItemProps) {
  const [editMode, setEditMode] = useState(false);
  const [value, setValue] = useState(name);
  const [isDeleteLoading, setIsDeleteLoading] = useState(false);
  const [isEditLoading, setIsEditLoading] = useState(false);

  useEffect(() => {
    (updatedCategory || updatedSubCategory || updatedFeature) &&
      !loadingEdit &&
      setEditMode(false);
  }, [updatedCategory, updatedCategory, updatedFeature]);

  return (
    <Paper
      style={{
        display: "flex",
        gap: 8,
        justifyContent: "space-between",
        width: "100%",
      }}
    >
      {editMode && (
        <>
          <TextInput
            required
            autoFocus
            style={{
              borderRadius: 0,
              display: "block",
              width: "100%",
              height: "100%",
            }}
            value={value}
            onChange={(e) => {
              setValue(e.currentTarget.value);
            }}
          />
          <Group style={{ display: "flex", gap: 4 }} noWrap>
            <ActionIcon
              variant="light"
              color="blue"
              onClick={() => {
                setEditMode(false);
              }}
              title="Cancel"
              className="danger"
            >
              <Icon icon="ph:x" height="14px" width="14px" className="danger" />
            </ActionIcon>
            <ActionIcon
              loading={isEditLoading && loadingEdit}
              disabled={loadingEdit}
              onClick={() => {
                editItem(value.trim().length > 0 ? value : name, id);
                setIsEditLoading(true);
              }}
              variant="light"
              color="green"
              className="success"
              title="Save"
            >
              <Icon
                icon="ph:check"
                height="14px"
                width="14px"
                className="success"
              />
            </ActionIcon>
          </Group>
        </>
      )}

      {!editMode && (
        <>
          <Text>{name}</Text>

          <Group style={{ display: "flex", gap: 4 }} noWrap>
            <ActionIcon
              loading={isDeleteLoading && loadingDelete}
              disabled={loadingDelete}
              variant="light"
              color="red"
              className="danger"
              onClick={() => {
                deleteItem(id);
                setIsDeleteLoading(true);
              }}
            >
              <Icon
                icon="ph:trash"
                height="16px"
                width="16px"
                className="danger"
              />
            </ActionIcon>

            <ActionIcon
              variant="light"
              color="blue"
              className="link"
              onClick={() => {
                setEditMode(true);
              }}
            >
              <Icon
                icon="ph:note-pencil"
                height="14px"
                width="14px"
                className="link"
              />
            </ActionIcon>
          </Group>
        </>
      )}
    </Paper>
  );
}
export default Item;
