import { gql } from '@apollo/client'

export const LOGOUT = gql`
  mutation Logout {
    logout {
      message
    }
  }
`

export const REGISTER = gql`
  mutation Register($user: UserInput!) {
    register(user: $user) {
      id
    }
  }
`

export const UPDATE_USER = gql`
  mutation UpdateUser($user: UserUpdate!) {
    updateUser(user: $user) {
      id
    }
  }
`

export const UPDATE_PASSWORD = gql`
  mutation UpdatePassword($user: UpdatePassword!) {
    updatePassword(user: $user) {
      id
    }
  }
`

export const CREATE_PROJECT = gql`
  mutation CreateProject($project: ProjectInput!) {
    createProject(project: $project) {
      id
    }
  }
`

export const UPDATE_PROJECT = gql`
  mutation UpdateProject($project: UpdateProject!, $updateProjectId: Float!) {
    updateProject(project: $project, id: $updateProjectId) {
      id
      name
      description
    }
  }
`

export const DELETE_PROJECT = gql`
  mutation DeleteProject($deleteProjectId: Float!) {
    deleteProject(id: $deleteProjectId) {
      message
    }
  }
`

export const CREATE_ISSUE = gql`
  mutation CreateIssue($issue: IssueInput!) {
    createIssue(issue: $issue) {
      id
    }
  }
`

export const EDIT_ISSUE = gql`
  mutation UpdateIssue($updateIssueId: Float!, $issue: IssueUpdateInput!) {
    updateIssue(id: $updateIssueId, issue: $issue) {
      id
    }
  }
`

export const DELETE_ISSUE = gql`
  mutation Mutation($deleteIssueId: Float!) {
    deleteIssue(id: $deleteIssueId) {
      message
    }
  }
`

export const CREATE_INVITE = gql`
  mutation Invite($invitation: InvitationInput!) {
    invite(invitation: $invitation) {
      id
    }
  }
`

export const REVOKE_INVITE = gql`
  mutation Revoke($revokeId: Float!) {
    revoke(id: $revokeId) {
      message
    }
  }
`

export const ACCEPT_INVITE = gql`
  mutation Accept($acceptId: Float!) {
    accept(id: $acceptId) {
      message
    }
  }
`

export const REJECT_INVITE = gql`
  mutation Decline($declineId: Float!) {
    decline(id: $declineId) {
      message
    }
  }
`

export const UPDATE_ROLE = gql`
  mutation UpdateRole($accessId: Float!, $user: UserRelation!, $role: String!) {
    updateRole(accessId: $accessId, user: $user, role: $role) {
      message
    }
  }
`

export const REMOVE_ACCESS = gql`
  mutation RemoveAccess($project: ProjectRelation!, $accessId: Float!) {
    removeAccess(project: $project, accessId: $accessId) {
      message
    }
  }
`

export const CREATE_CATEGORY = gql`
  mutation CreateCategory($category: CategoryInput!) {
    createCategory(category: $category) {
      id
      name
    }
  }
`

export const UPDATE_CATEGORY = gql`
  mutation UpdateCategory(
    $updateCategoryId: Float!
    $category: CategoryInputUpdate!
  ) {
    updateCategory(id: $updateCategoryId, category: $category) {
      name
    }
  }
`

export const DELETE_CATEGORY = gql`
  mutation DeleteCategory($deleteCategoryId: Float!) {
    deleteCategory(id: $deleteCategoryId) {
      message
    }
  }
`

export const CREATE_SUBCATEGORY = gql`
  mutation CreateSubCategory($subcategory: SubCategoryInput!) {
    createSubCategory(subcategory: $subcategory) {
      id
      name
    }
  }
`

export const UPDATE_SUBCATEGORY = gql`
  mutation UpdateSubCategory(
    $updateSubCategoryId: Float!
    $subcategory: SubCategoryInputUpdate!
  ) {
    updateSubCategory(id: $updateSubCategoryId, subcategory: $subcategory) {
      name
    }
  }
`

export const DELETE_SUBCATEGORY = gql`
  mutation DeleteSubCategory($deleteSubCategoryId: Float!) {
    deleteSubCategory(id: $deleteSubCategoryId) {
      message
    }
  }
`

export const CREATE_FEATURE = gql`
  mutation CreateFeature($feature: FeatureInput!) {
    createFeature(feature: $feature) {
      id
      name
    }
  }
`

export const UPDATE_FEATURE = gql`
  mutation UpdateFeature(
    $updateFeatureId: Float!
    $feature: FeatureInputUpdate!
  ) {
    updateFeature(id: $updateFeatureId, feature: $feature) {
      name
    }
  }
`

export const DELETE_FEATURE = gql`
  mutation DeleteFeature($deleteFeatureId: Float!) {
    deleteFeature(id: $deleteFeatureId) {
      message
    }
  }
`
