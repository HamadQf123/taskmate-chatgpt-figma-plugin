import { RouteStateType, RoutingAction } from './../../types'

export const routingReducer = (
  state: RouteStateType,
  action: RoutingAction
) => {
  const { type, payload } = action

  switch (type) {
    case 'routing':
      return {
        ...state,
        ...payload,
      }
    default:
      return state
  }
}
