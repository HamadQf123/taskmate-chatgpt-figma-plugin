import { useMutation, useQuery } from '@apollo/client'
import {
  CREATE_SUBCATEGORY,
  DELETE_SUBCATEGORY,
  UPDATE_SUBCATEGORY,
} from '../../graphql/mutations'
import { GET_SUBCATEGORIES } from '../../graphql/queries'

export const useCreateSubCategory = () => {
  const [createSubCategory, { loading, data, error }] =
    useMutation(CREATE_SUBCATEGORY)

  return {
    createSubCategory,
    loading,
    data,
    error,
  }
}

export const useUpdateSubCategory = () => {
  const [updateSubCategory, { loading, data, error }] =
    useMutation(UPDATE_SUBCATEGORY)

  return {
    updateSubCategory,
    loading,
    data,
    error,
  }
}

export const useDeleteSubCategory = () => {
  const [deleteSubCategory, { loading, data, error }] =
    useMutation(DELETE_SUBCATEGORY)

  return {
    deleteSubCategory,
    loading,
    data,
    error,
  }
}

export const useGetSubCategories = ({
  params,
}: {
  params: {
    id: number
  }
}) => {
  const { loading, data, error, refetch } = useQuery(GET_SUBCATEGORIES, {
    variables: { params },
    fetchPolicy: 'network-only',
    notifyOnNetworkStatusChange: true,
  })

  return {
    loading,
    data,
    error,
    refetch,
  }
}
