import React, { useEffect } from 'react'
import Template from './Template'
import { Stack, Loader } from '@mantine/core'
import NewItem from './NewItem'
import {
  useCreateSubCategory,
  useGetSubCategories,
} from './../../../shared/service/subCategories'
import { useProjectIdContext } from '../../../shared/context/ProjectIdProvider'
import { showNotification } from '@mantine/notifications'

const SubCategories = () => {
  const { createSubCategory, loading, data, error } = useCreateSubCategory()
  const { projectId } = useProjectIdContext()
  const {
    loading: loadingSubCategories,
    data: subCategories,
    error: subCategoriesError,
    refetch: refetchSubCategories,
  } = useGetSubCategories({
    params: {
      id: parseInt(projectId),
    },
  })

  useEffect(() => {
    data && refetchSubCategories()
  }, [data])

  useEffect(() => {
    {
      error &&
        showNotification({
          message: error.message,
          color: 'red',
        })
    }

    {
      data &&
        showNotification({
          message: 'Subcategory created successfully',
          color: 'green',
        })
    }

    {
      subCategoriesError &&
        showNotification({
          message: subCategoriesError.message,
          color: 'red',
        })
    }
  }, [error, data, subCategoriesError])

  return (
    <Stack spacing={'sm'} align="center" justify="center" w={'100%'}>
      {loadingSubCategories && <Loader size={'sm'} />}

      {subCategories && !loadingSubCategories && (
        <Template
          items={subCategories.getSubCategories}
          type="Subcategories"
          refetchSubCategories={refetchSubCategories}
        />
      )}

      {!loadingSubCategories && (
        <NewItem
          name="New Subcategory"
          loading={loading}
          dispatch={(value) => {
            createSubCategory({
              variables: {
                subcategory: {
                  name: value,
                  project: projectId,
                },
              },
            })
          }}
        />
      )}
    </Stack>
  )
}
export default SubCategories
