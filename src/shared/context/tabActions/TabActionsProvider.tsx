import React, { Dispatch, createContext, useContext, useReducer } from 'react'
import {
  StateProviderProps,
  TabActionsStateType,
  TabReducerActions,
} from './../../types'

const TabActionsContext = createContext(
  {} as {
    state: TabActionsStateType
    dispatch: Dispatch<TabReducerActions>
  }
)

const tabActionsReducer = (
  state: TabActionsStateType,
  action: TabReducerActions
) => {
  const { type, payload } = action
  switch (type) {
    case 'ATTACH_ISSUE':
      return {
        ...state,
        issues: payload.issues,
      }
    case 'ATTACH_USER':
      return {
        ...state,
        users: payload.users,
      }
    default:
      return state
  }
}

const TabActionsProvider = ({ children }: StateProviderProps) => {
  const initialState = { issues: undefined, users: undefined }
  const [state, dispatch] = useReducer(tabActionsReducer, initialState)

  return (
    <TabActionsContext.Provider value={{ state, dispatch }}>
      {children}
    </TabActionsContext.Provider>
  )
}
export default TabActionsProvider

export const useTabActionsContext = () => useContext(TabActionsContext)
