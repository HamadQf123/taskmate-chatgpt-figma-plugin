import React, { useState, useEffect } from 'react'
import {
  Paper,
  TextInput,
  Text,
  Loader,
  Group,
  Button,
  Badge,
  Popover,
  Anchor,
} from '@mantine/core'
import { showNotification } from '@mantine/notifications'
import { Icon } from '@iconify/react'
import {
  useCreateInvite,
  useGetProject,
  useGetUsers,
  useMe,
  useRevokeInvite,
} from '../../../shared/service'
import { useDebouncedValue } from '@mantine/hooks'

const InviteUserForm = ({ projectId }: { projectId: string }) => {
  const [keyword, setKeyword] = useState('')
  const [debouncedKeyword] = useDebouncedValue(keyword, 200)
  const { getUsers, data, loading, error, refetch } = useGetUsers()
  const { data: me } = useMe()

  useEffect(() => {
    if (debouncedKeyword.trim()) {
      getUsers({
        variables: {
          params: {
            search: debouncedKeyword,
          },
        },
      })
    }
  }, [debouncedKeyword])

  return (
    <div>
      <form>
        <TextInput
          autoFocus
          icon={<Icon icon="ph:magnifying-glass" />}
          placeholder="Search user by name, username, or username"
          value={keyword}
          onChange={(event) => setKeyword(event.currentTarget.value)}
        />

        <Group
          style={{
            marginTop: 10,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          {data &&
            data.getUsers
              ?.filter(
                (user: { name: string; username: string; id: any }) =>
                  me.me?.id !== user.id
              )
              .map((user: any) => {
                return <User user={user} projectId={projectId} key={user?.id} />
              })}
          {data &&
            data.getUsers?.filter(
              (user: { name: string; username: string; id: any }) =>
                me.me?.id !== user.id
            )?.length === 0 && (
              <>
                <Text size={'md'} fw="bold" color={'gray'}>
                  No users found
                </Text>
              </>
            )}

          {loading && <Loader size={'sm'} />}
          {error && (
            <>
              <Text size={'md'} color="red">
                {error.message}
              </Text>
              <Button variant="light" size="xs" onClick={() => refetch()}>
                Retry
              </Button>
            </>
          )}
        </Group>
      </form>
    </div>
  )
}
export default InviteUserForm

export const User = ({
  user,
  projectId,
}: {
  user: { id: string; name: string; username: string }
  projectId: string
  invitationId?: string
}) => {
  const { data: projectData, refetch } = useGetProject(projectId)

  const { createInvite, loading, data, error } = useCreateInvite()

  // revoke invite fn
  const {
    revokeInvite,
    loading: revokeLoading,
    data: revokeData,
    error: revokeError,
  } = useRevokeInvite()

  useEffect(() => {
    if (data) {
      refetch()
    }
    if (error) {
      showNotification({
        message: error.message,
        color: 'red',
      })
    }
  }, [data, error])

  useEffect(() => {
    if (revokeData) {
      refetch()
    }
    if (revokeError) {
      showNotification({
        message: revokeError.message,
        color: 'red',
      })
    }
  }, [revokeData, revokeError])

  return (
    <Paper
      withBorder
      style={{
        padding: 7,
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        gap: 10,
      }}
    >
      <div>
        <Text size={'sm'}>{user.username}</Text>
        {/* <Text size={'xs'} color="gray">
          {user.name}
        </Text> */}
      </div>

      <div>
        {!projectData?.getProject?.invitations?.find(
          (invite: any) => invite?.user?.id === user?.id
        ) &&
          !projectData?.getProject?.access?.find(
            (access: any) => access?.user?.id === user?.id
          ) && (
            <Popover shadow="md">
              <Popover.Target>
                <div
                  style={{
                    position: 'relative',
                  }}
                >
                  <Button variant="light" size="sm" compact loading={loading}>
                    Invite
                  </Button>
                </div>
              </Popover.Target>
              <Popover.Dropdown>
                <Anchor
                  onClick={() => {
                    createInvite({
                      variables: {
                        invitation: {
                          user: {
                            id: user?.id,
                          },
                          project: {
                            id: projectId,
                          },
                          role: 'MEMBER',
                        },
                      },
                    })
                  }}
                >
                  Invite As A Member
                </Anchor>
                <br />
                <Anchor
                  onClick={() => {
                    createInvite({
                      variables: {
                        invitation: {
                          user: {
                            id: user?.id,
                          },
                          project: {
                            id: projectId,
                          },
                          role: 'ADMIN',
                        },
                      },
                    })
                  }}
                >
                  Invite As An Admin
                </Anchor>
              </Popover.Dropdown>
            </Popover>
          )}

        {projectData?.getProject?.invitations?.find(
          (invite: any) => invite?.user?.id === user?.id
        ) && (
          <Button
            variant="light"
            color={'red'}
            size="sm"
            compact
            loading={revokeLoading}
            className='danger'
            onClick={() => {
              revokeInvite({
                variables: {
                  revokeId: projectData?.getProject?.invitations?.find(
                    (invite: any) => invite?.user?.id === user?.id
                  )?.id,
                },
              })
            }}
          >
            Uninvite
          </Button>
        )}

        {projectData?.getProject?.access?.find(
          (access: any) => access?.user?.id === user?.id
        ) && <Badge color={'green'}>Member</Badge>}
      </div>
    </Paper>
  )
}
