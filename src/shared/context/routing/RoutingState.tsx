import React, { createContext, useContext, useReducer } from "react";
import {
  RouteStateType,
  RoutingContextType,
  StateProviderProps,
} from "./../../types";
import { routingReducer } from "./routingReducer";

const RoutingContext = createContext<RoutingContextType>({
  state: { route: "", loading: true },
  dispatch: () => {},
});

const RoutingState = ({ children }: StateProviderProps) => {
  const initialState: RouteStateType = { route: "" };
  const [state, dispatch] = useReducer(routingReducer, initialState);

  return (
    <RoutingContext.Provider value={{ state, dispatch }}>
      {children}
    </RoutingContext.Provider>
  );
};
export default RoutingState;

export const useRoutingState = () => useContext(RoutingContext);
