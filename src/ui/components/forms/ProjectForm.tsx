import { TextInput, Textarea, Group, Button, Space } from "@mantine/core";
import { useForm } from "@mantine/form";
import React from "react";
import { useCreateProject, useUpdateProject } from "../../../shared/service";
import { ProjectFormProps } from "../../../shared/types";
import { showNotification } from "@mantine/notifications";

const ProjectForm = ({ project, close, done, refetch }: ProjectFormProps) => {
  const { createProject, loading } = useCreateProject();
  const { updateProject, loading: updateLoading } = useUpdateProject();
  const form = useForm({
    initialValues: {
      name: project?.name ?? "",
      description: project?.description ?? "",
    },

    validate: {
      name: (value) =>
        value.length >= 1 ? null : "Project name cannot be empty",
    },
  });

  return (
    <form
      style={{
        width: "100%",
      }}
      onSubmit={form.onSubmit((values) => {
        project?.id
          ? updateProject({
              variables: {
                project: {
                  name: values.name,
                  description: values.description,
                },
                updateProjectId: project.id,
              },
            })
              .then((data) => {
                showNotification({
                  message: "Project updated successfully",
                  color: "green",
                });
                refetch?.();
                done?.();
                close?.(data?.data?.updateProject);
              })
              .catch((err) => {
                showNotification({
                  message: err?.message,
                  color: "red",
                });
              })
          : createProject({
              variables: {
                project: {
                  name: values.name,
                  description: values.description,
                  type: "none",
                },
              },
            })
              .then((data) => {
                showNotification({
                  message: "Project created successfully",
                  color: "green",
                });
                refetch?.();
                done?.();
                close?.();
              })
              .catch((err) => {
                showNotification({
                  message: err?.message,
                  color: "red",
                });
              });
      })}
    >
      <TextInput
        withAsterisk
        label="Project Name"
        placeholder="Your project name"
        type={"name"}
        {...form.getInputProps("name")}
      />
      <Space h="sm" />

      <Textarea
        placeholder="Your description"
        label="Description"
        {...form.getInputProps("description")}
      />

      <Group position="center" mt="20px" style={{ flexDirection: "column" }}>
        <Button
          type="submit"
          style={{ width: "50%" }}
          loading={loading || updateLoading}
        >
          {project?.id ? "Update" : "Create"}
        </Button>
      </Group>
    </form>
  );
};
export default ProjectForm;
