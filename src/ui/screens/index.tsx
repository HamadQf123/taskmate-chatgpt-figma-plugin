import { LoadingOverlay, Text, Button } from '@mantine/core'
import React, { useEffect } from 'react'
import { useRoutingState } from '../../shared/context/routing/RoutingState'
import { useMe } from '../../shared/service'
import Issue from '../components/issue'
import Layout from '../layout'
import Login from './auth/Login'
import Register from './auth/Register'
import Dashboard from './dashboard'
import Project from './project'

const Screens = () => {
  const { state, dispatch } = useRoutingState()
  const { data, error, loading, refetch } = useMe()

  useEffect(() => {
    if (!data && !error) {
      dispatch({ type: 'routing', payload: { route: 'login' } })
    }

    if (data) {
      dispatch({ type: 'routing', payload: { route: 'dashboard' } })
    }

    if (error && !error.networkError) {
      dispatch({
        type: 'routing',
        payload: {
          route: 'login',
        },
      })
    }
  }, [data, error])

  if (state.loading || loading) {
    return <LoadingOverlay visible={true} title="Opening Dash" />
  }

  if (error?.networkError) {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          gap: 7,
          height: '100%',
        }}
      >
        <Text size={'md'} color={'red'}>
          {error.message}
        </Text>{' '}
        <Button variant="light" onClick={() => refetch()}>
          Retry
        </Button>
      </div>
    )
  }

  const authScreen = (route: string) => {
    switch (route) {
      case 'login':
        return <Login />
      case 'register':
        return <Register />
      case 'dashboard':
        return <Dashboard />
      case 'project':
        return <Project />
      case 'issue':
        return <Issue />
      case 'invitations':
        return <Issue />
      default:
        return <LoadingOverlay visible={true} title="Opening Dash" />
    }
  }

  return <Layout>{authScreen(state.route)}</Layout>
}

export default Screens
