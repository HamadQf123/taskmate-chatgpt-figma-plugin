import { useMutation, useQuery } from '@apollo/client'
import {
  CREATE_CATEGORY,
  DELETE_CATEGORY,
  UPDATE_CATEGORY,
} from '../../graphql/mutations'
import { GET_CATEGORIES } from '../../graphql/queries'


export const useCreateCategory = () => {
  const [createCategory, { loading, data, error }] =
    useMutation(CREATE_CATEGORY)
  

  return {
    createCategory,
    loading,
    data,
    error,
  }
}

export const useUpdateCategory = () => {
  const [updateCategory, { loading, data, error }] =
    useMutation(UPDATE_CATEGORY)

  return {
    updateCategory,
    loading,
    data,
    error,
  }
}

export const useDeleteCategory = () => {
  const [deleteCategory, { loading, data, error }] =
    useMutation(DELETE_CATEGORY)

  return {
    deleteCategory,
    loading,
    data,
    error,
  }
}

export const useGetCategories = ({
  params,
}: {
  params: {
    id: number
  }
}) => {
  const { loading, data, error, refetch } = useQuery(GET_CATEGORIES, {
    variables: { params },
    fetchPolicy: 'network-only',
    notifyOnNetworkStatusChange: true,
  })

  return {
    loading,
    data,
    error,
    refetch,
  }
}
