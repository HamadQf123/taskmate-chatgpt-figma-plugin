import React, { useEffect, useState } from "react";
import { Anchor, Modal, Notification, Button, Text } from "@mantine/core";
import "react-day-picker/dist/style.css";
import { DataTable, DataTableSortStatus } from "mantine-datatable";
import { Icon } from "@iconify/react";
import { useGetIssues } from "../../../shared/service";
import { useRoutingState } from "../../../shared/context/routing/RoutingState";
import DeleteIssue from "../deleteIssue";
import { useDebouncedValue } from "@mantine/hooks";
import { useTabActionsContext } from "../../../shared/context/tabActions/TabActionsProvider";
import { ProjectInfoType } from "../../../shared/types";
import { useIssueColumns } from "./IssuesColumn";
import { useIssueIdContext } from "./../../../shared/context/IssueIdProvider";

const IssuesTable = (projectInfo: ProjectInfoType) => {
  const { dispatch } = useRoutingState();
  const { setIssueId } = useIssueIdContext();
  const [keyword, setKeyword] = useState("");
  const [filters, setFilters] = useState<string[]>([]);
  const [sortStatus, setSortStatus] = useState<DataTableSortStatus>({
    columnAccessor: "created",
    direction: "desc",
  });

  const {
    IssueColumns,
    issueToDelete,
    setIssueToDelete,
    keywords,
    setKeywords,
    filter,
    setFilter,
    setRange,
  } = useIssueColumns(projectInfo);

  const [debouncedKeyword] = useDebouncedValue(keyword, 200);
  const [debounceKeywords] = useDebouncedValue(keywords, 200);

  const [page, setPage] = useState(1);

  const { loading, data, error, refetch } = useGetIssues({
    params: {
      page,
      pageSize: 10,
      paging: true,
      id: projectInfo.id,
      ...(debouncedKeyword && filters?.length === 0
        ? { search: debouncedKeyword }
        : {}),
      ...(sortStatus
        ? {
            property: sortStatus?.columnAccessor,
            direction: sortStatus?.direction,
          }
        : {}),
      ...(filter && Object.keys(filter)?.length > 0
        ? {
            filter,
          }
        : {}),
      ...(filters?.length > 0 && debouncedKeyword
        ? {
            filters: filters?.map((filter) => {
              return {
                key: filter,
                value: debouncedKeyword,
              };
            }),
          }
        : {}),
      ...(Object.values(debounceKeywords)
        ? {
            filters: Object.keys(debounceKeywords)
              ?.filter(
                (keyword) =>
                  !(keyword === "id" && !debounceKeywords["id"]) &&
                  !(
                    keyword === "assignee" && !debounceKeywords["assignee"]?.id
                  ) &&
                  !(keyword === "created" && !debounceKeywords["created"])
              )
              ?.map((keyword) => {
                return {
                  key: keyword,
                  value: keyword,
                };
              }),
          }
        : {}),
    },
  });

  useEffect(() => {
    parent.postMessage(
      {
        pluginMessage: {
          type: "get-selections",
        },
      },
      "*"
    );
  }, []);

  onmessage = (event) => {
    if (event.data.pluginMessage.type === "send-selections") {
      if (event.data.pluginMessage.selectionNodes) {
        if (keywords.elementName) {
          return setKeywords({
            ...keywords,
            elementName: [
              {
                ...keywords?.elementName[0],
                id: `[${event.data.pluginMessage.selectionNodes}]`,
              },
            ],
          });
        }
      } else {
        // setFilter((prev: any) => {
        //   const prevCopy = prev
        //   delete prevCopy?.elementId
        //   return { ...prevCopy }
        // })
      }
    }
  };

  const { state: tabActionsState, dispatch: tabActionsDispatch } =
    useTabActionsContext();

  useEffect(() => {
    tabActionsDispatch({
      type: "ATTACH_ISSUE",
      payload: {
        ...tabActionsState,
        issues: (
          <>
            {((filter && Object.keys(filter)?.length > 0) ||
              keyword ||
              filters?.length > 0 ||
              Object.values(debounceKeywords)) && (
              <Anchor
                onClick={() => {
                  setFilter(undefined);
                  setFilters([]);
                  setKeyword("");
                  setRange(undefined);
                  setKeywords({
                    id: "",
                    description: "",
                    created: undefined,
                  });
                }}
              >
                Clear Filters
              </Anchor>
            )}

            <Button
              variant="light"
              onClick={() => {
                dispatch({
                  type: "routing",
                  payload: {
                    route: "issue",
                  },
                });

                setIssueId("");
              }}
            >
              Create Issue
            </Button>
          </>
        ),
      },
    });
  }, []);

  return (
    <>
      <Modal
        opened={!!issueToDelete}
        onClose={() => setIssueToDelete(undefined)}
        title="Delete Issue"
      >
        <DeleteIssue
          id={issueToDelete?.id ?? ""}
          refetch={refetch}
          close={() => setIssueToDelete(undefined)}
        />
      </Modal>

      {error && (
        <Notification
          icon={<Icon width={18} height={18} icon={"ph:x"} />}
          color="red"
          disallowClose
          style={{ paddingBottom: "10px" }}
        >
          {error?.message}
        </Notification>
      )}

      <DataTable
        withBorder
        minHeight={200}
        withColumnBorders
        fetching={loading}
        className={`issue-table ${
          data?.getIssues?.length > 0 ? "" : "no-data"
        }`}
        records={[
          { id: "search" },
          ...(data?.getIssues ? data?.getIssues : []),
        ]}
        columns={IssueColumns}
        onCellClick={({ record }: { record: any }) => {
          if (record?.id !== "search")
            dispatch({
              type: "routing",
              payload: {
                route: "issue",
              },
            });

          setIssueId(record.id);
        }}
        style={{
          height: "fit-content",
          ...(data?.getIssues?.length > 0
            ? { minHeight: "100px" }
            : { color: "transparent" }),
        }}
        page={data?.getIssues?.length > 0 ? page : 0}
        onPageChange={(page) => {
          setPage(page);
        }}
        totalRecords={data?.getIssues ? data?.getIssues[0]?.total : 0}
        recordsPerPage={10}
        sortStatus={sortStatus}
        onSortStatusChange={setSortStatus}
        noRecordsText=""
      />
      {data?.getIssues?.length === 0 && (
        <div
          style={{
            position: "relative",
            zIndex: 5,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            top: "-90px",
            width: "100%",
          }}
        >
          <div>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="icon icon-tabler icon-tabler-database-off"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              stroke="currentColor"
              stroke-width="2"
              fill="none"
              strokeLinecap="round"
              strokeLinejoin="round"
            >
              <path stroke="none" d="M0 0h24v24H0z" fill="none"></path>
              <path d="M12.983 8.978c3.955 -.182 7.017 -1.446 7.017 -2.978c0 -1.657 -3.582 -3 -8 -3c-1.661 0 -3.204 .19 -4.483 .515m-2.783 1.228c-.471 .382 -.734 .808 -.734 1.257c0 1.22 1.944 2.271 4.734 2.74"></path>
              <path d="M4 6v6c0 1.657 3.582 3 8 3c.986 0 1.93 -.067 2.802 -.19m3.187 -.82c1.251 -.53 2.011 -1.228 2.011 -1.99v-6"></path>
              <path d="M4 12v6c0 1.657 3.582 3 8 3c3.217 0 5.991 -.712 7.261 -1.74m.739 -3.26v-4"></path>
              <line x1="3" y1="3" x2="21" y2="21"></line>
            </svg>
          </div>
          <Text>No record found</Text>
        </div>
      )}
    </>
  );
};
export default IssuesTable;
