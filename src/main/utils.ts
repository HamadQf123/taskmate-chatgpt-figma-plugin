export const manageToken = async (msg: any) => {
  if (msg.type === "new-auth-token") {
    await figma.clientStorage.setAsync(
      "auth-token",
      JSON.stringify(msg.tokenStr)
    );
  } else if (msg.type === "delete-auth-token") {
    await figma.clientStorage.deleteAsync("auth-token");
  }
};

interface MyObject {
  name: string;
  child?: MyObject[];
}

function processObject(obj: any): MyObject[] {
  const result: MyObject[] = [];
  const { children } = obj;

  if (children) {
    children?.forEach((item: any) => {
      const obj: MyObject = { name: item.name };

      if (item.children) {
        obj.child = processObject(item);
      }
      result.push(obj);
    });
    return result;
  }

  return result;
}

export const getToken = async () => {
  const token = await figma.clientStorage.getAsync("auth-token");
  figma.ui.postMessage({ type: "get-auth-token", token });
};

export const getFileKey = () => {
  figma.ui.postMessage({ type: "get-file-key", fileKey: figma.fileKey });
};
const getSelectionNodes = () =>
  figma.currentPage.selection.map((n) => {
    return {
      id: n.id,
      name: n.name,
      parent: {
        id: n.parent?.id,
        name: n.parent?.name,
        type: n.parent?.type,
      },
      children: processObject(n),
      fileName: figma.root.name,
      fileId: figma.fileKey,
    };
  });

const getIds = (arr: any[], ids: any[]) => {
  return arr.map((a) => {
    ids.push(a.id);
    if (a.children) {
      getIds(a.children, ids);
      return ids;
    }
    return ids;
  });
};

export const getSelection = (msg: any) => {
  if (msg.type === "get-selection") {
    let selectionNodes = getSelectionNodes();

    figma.ui.postMessage({ type: "send-selection", selectionNodes });

    figma.on("selectionchange", () => {
      selectionNodes = getSelectionNodes();
      figma.ui.postMessage({ type: "send-selection", selectionNodes });
    });
  }
};

export const getSelections = (msg: any) => {
  if (msg.type === "get-selections") {
    //@ts-ignore
    let selectionNodes = getIds(figma.currentPage.selection, [])[0];
    figma.ui.postMessage({ type: "send-selections", selectionNodes });

    figma.on("selectionchange", () => {
      //@ts-ignore
      selectionNodes = getIds(figma.currentPage.selection, [])[0];
      figma.ui.postMessage({ type: "send-selections", selectionNodes });
    });
  }
};

export const selectNode = (msg: any) => {
  if (msg.type === "select") {
    const node = figma.getNodeById(msg.nodeId);
    if (node) {
      const page = figma.root.findChild((page) => {
        if (page.findOne((n) => n.id === node.id)) {
          return true;
        }
        return false;
      });

      if (page) {
        const selectNode: any[] = [];
        selectNode.push(node);
        figma.currentPage = page;
        figma.currentPage.selection = selectNode;
        figma.viewport.scrollAndZoomIntoView(selectNode);
      }
      return;
    }

    figma.ui.postMessage({
      type: "no-node-error",
      message: "Element not found (might have been deleted)",
    });
  }
};

export const getFileName = (msg: any) => {
  if (msg.type === "getFileName") {
    if (msg.fileKey === figma.fileKey) {
      figma.ui.postMessage({
        type: "receiveFileName",
        fileName: figma.root.name,
      });

      figma.clientStorage.setAsync("fileName", figma.root.name);
      return;
    }
    return;
  }
};

export interface PluginSizeType {
  width: number;
  height: number;
}

export const resizePlugin = (msg: any) => {
  if (msg.type === "resize-plugin") {
    const pluginSize: PluginSizeType = msg.size;

    const zoom = figma.viewport.zoom;
    let centerX = figma.viewport.center.x - pluginSize.width / zoom / 2;
    let centerY = figma.viewport.center.y - pluginSize.height / zoom / 2;

    //@ts-ignore
    figma.ui.reposition(centerX, centerY);
    figma.ui.resize(pluginSize.width, pluginSize.height);
  }
};

export const minimizePlugin = (msg: any) => {
  if (msg.type === "minimize-plugin") {
    const pluginSize: PluginSizeType = msg.size;

    const { x: x_c, y: y_c } = figma.viewport.center;
    const z = figma.viewport.zoom;
    const { x, y, width: w, height: h } = figma.viewport.bounds;

    const half_h = h * 16 * 0.02;

    //@ts-ignore
    figma.ui.reposition(x, y_c + half_h);

    figma.ui.resize(200, pluginSize.height);
  }
};

export const maximizePlugin = (msg: any) => {
  if (msg.type === "maximize-plugin") {
    const pluginSize: PluginSizeType = msg.size;

    const zoom = figma.viewport.zoom;
    let centerX = figma.viewport.center.x - pluginSize.width / zoom / 2;
    let centerY = figma.viewport.center.y - pluginSize.height / zoom / 2;

    //@ts-ignore
    figma.ui.reposition(centerX, centerY);
    figma.ui.resize(pluginSize.width, pluginSize.height);
  }
};
