import { Group, Text } from '@mantine/core'
import React from 'react'
import { useGetProject } from '../../../shared/service'
import { User } from '../forms/InviteUserForm'

const Invitations = ({ projectId }: { projectId: string }) => {
  const { data: projectData } = useGetProject(projectId)
  return (
    <Group
      style={{
        marginTop: 10,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      {projectData?.getProject?.invitations?.length === 0 && (
        <Text size={'md'} fw="bold" color={'gray'}>
          No pending invitations
        </Text>
      )}
      {projectData?.getProject?.invitations?.map((user: any) => (
        <User
          user={user?.user}
          invitationId={user?.id}
          projectId={projectId}
          key={user?.id}
        />
      ))}
    </Group>
  )
}
export default Invitations
