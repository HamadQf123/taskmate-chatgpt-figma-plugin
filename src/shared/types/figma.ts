export interface ElementNameType {
  fileId: string
  fileName: string
  id: string
  name: string
}

interface SelectedNodeParentType {
  id: string
  name: string
  type: string
}

export interface SelectedNodeType {
  id: string
  name: string
  parent: SelectedNodeParentType
  children: any[]
  fileName: string
  fileId: string
}

export interface FigmaStateType {
  fileKey: string
  fileName: string
}

export interface FigmaReducerActions {
  type: 'ATTACH_FILE_KEY' | 'ATTACH_FILE_NAME'
  payload: FigmaStateType
}


