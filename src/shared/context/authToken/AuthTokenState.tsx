import React, { createContext, useContext, useReducer } from 'react'
import {
  AuthTokenContextType,
  AuthTokenStateType,
  StateProviderProps,
} from '../../types'
import { authTokenReducer } from './authTokenReducer'

const AuthTokenContext = createContext<AuthTokenContextType>({
  state: {
    tokenType: 'access',
    token: {
      accessToken: null,
      refreshToken: null,
    },
  },
  dispatch: () => {},
})

const AuthTokenState = ({ children }: StateProviderProps) => {
  const initialState: AuthTokenStateType = {
    tokenType: 'access',
    token: {
      accessToken: null,
      refreshToken: null,
    },
  }
  const [state, dispatch] = useReducer(authTokenReducer, initialState)
  return (
    <AuthTokenContext.Provider value={{ state, dispatch }}>
      {children}
    </AuthTokenContext.Provider>
  )
}
export default AuthTokenState

export const useAuthTokenState = () => useContext(AuthTokenContext)
