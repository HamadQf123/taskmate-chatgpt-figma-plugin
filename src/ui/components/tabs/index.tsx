import React, { useEffect, useState } from 'react'
import { TabContent } from '../../../shared/types'
import { Tabs } from '@mantine/core'
import { Icon } from '@iconify/react'
import { useTabActionsContext } from '../../../shared/context/tabActions/TabActionsProvider'

const TabsTemplate = ({
  tabs,
  noActions,
  defaultValue,
}: {
  tabs: TabContent[]
  noActions?: boolean
  defaultValue?: string
}) => {
  const { state } = useTabActionsContext()
  const [tabsActionsValue, setTabsActionsValue] = useState(state.issues)

  useEffect(() => {
    if (tabs[0].value === 'issues') {
      setTabsActionsValue(state.issues)
    }
  }, [state.issues])

  return (
    <Tabs
      defaultValue={defaultValue ?? tabs[0].value}
      onTabChange={(value) => {
        if (value === 'issues') {
          setTabsActionsValue(state.issues)
        } else if (value === 'users') {
          setTabsActionsValue(state.users)
        }
      }}
    >
      <Tabs.List>
        {tabs.map((tab, index) => (
          <Tabs.Tab
            key={index}
            value={tab.value}
            icon={
              tab.icon ? <Icon icon={tab.icon} height={14} width={14} /> : null
            }
          >
            {tab.title}
          </Tabs.Tab>
        ))}

        {!noActions && tabsActionsValue && (
          <div className="tabs-actions">{tabsActionsValue}</div>
        )}
      </Tabs.List>

      {tabs.map((tab, index) => (
        <Tabs.Panel key={index} value={tab.value} pt="xs">
          {tab.content}
        </Tabs.Panel>
      ))}
    </Tabs>
  )
}
export default TabsTemplate
