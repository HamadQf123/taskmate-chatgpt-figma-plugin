import { IssueTypesType, IssueOptionType } from '../types'

export const issueTypes: IssueTypesType[] = [
  {
    label: 'Task',
    value: 'Task',
  },
  {
    label: 'Bug',
    value: 'Bug',
  },
]

export const statusOptions: IssueOptionType[] = [
  {
    label: 'New',
    value: 'New',
  },
  {
    label: 'In Design',
    value: 'In Design',
  },
  {
    label: 'In Develop',
    value: 'In Develop',
  },
  {
    label: 'In Testing',
    value: 'In Testing',
  },
  {
    label: 'Design Complete',
    value: 'Design Complete',
  },
  {
    label: 'Develop Complete',
    value: 'Develop Complete',
  },
  {
    label: 'Testing Complete',
    value: 'Testing Complete',
  },
  {
    label: 'Lauched',
    value: 'Lauched',
  },
]
