import { useGetSubCategories } from './../service/subCategories'
import { useProjectIdContext } from './../context/ProjectIdProvider'
import { useEffect, useState } from 'react'

const useCategoryOptions = () => {
  const { projectId } = useProjectIdContext()
  const {
    loading: loadingSubCategories,
    data: subCategories,
    error: subCategoriesError,
  } = useGetSubCategories({
    params: {
      id: parseInt(projectId),
    },
  })

  const [subCategoryOptions, setCategoryOptions] = useState<
    Array<{ value: string; label: string }>
  >([])

  useEffect(() => {
    if (subCategories && subCategories.getSubCategories.length > 0) {
      setCategoryOptions([]);
      subCategories.getSubCategories.forEach(
        (subCategory: { id: string; name: string }) => {
          setCategoryOptions((prev) => [
            ...prev,
            { value: subCategory.id, label: subCategory.name },
          ])
        }
      )
    }
  }, [subCategories])

  return {
    subCategoriesError,
    loadingSubCategories,
    subCategoryOptions,
  }
}

export default useCategoryOptions
