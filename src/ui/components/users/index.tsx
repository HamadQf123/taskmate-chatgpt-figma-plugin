import React, { useEffect, useState } from 'react'
import { Button, Modal, Text, Anchor } from '@mantine/core'
import UsersTable from '../table/UsersTable'
import InviteUserForm from '../forms/InviteUserForm'
import Invitations from './invitations'
import { useGetProject, useMe } from '../../../shared/service'
import { useTabActionsContext } from '../../../shared/context/tabActions/TabActionsProvider'

const Users = ({ projectId }: { projectId: string }) => {
  const { state, dispatch } = useTabActionsContext()
  const [opened, setOpened] = useState(false)
  const { data: me } = useMe()
  const [viewInvitations, setViewInvitations] = useState(false)
  const { data: projectData } = useGetProject(projectId)

  let isOwner = projectData?.getProject?.createdBy?.id === me?.me?.id

  let isAdmin = !!projectData?.getProject?.access?.find(
    (user: any) => user?.user?.id === me?.me?.id && user?.role === 'ADMIN'
  )

  useEffect(() => {
    ;(isAdmin || isOwner) &&
      dispatch({
        type: 'ATTACH_USER',
        payload: {
          ...state,
          users: (
            <>
              <Anchor onClick={() => setViewInvitations(true)}>
                Invitations ({projectData?.getProject?.invitations?.length})
              </Anchor>
              <Button variant="light" onClick={() => setOpened(true)}>
                Add User
              </Button>
            </>
          ),
        },
      })
  }, [isOwner, isAdmin])

  return (
    <div>
      <Modal opened={opened} onClose={() => setOpened(false)} title="Add User">
        <InviteUserForm projectId={projectId} />
      </Modal>
      <Modal
        opened={viewInvitations}
        onClose={() => setViewInvitations(false)}
        title="Pending Invitations"
      >
        <Invitations projectId={projectId} />
      </Modal>
      <UsersTable projectId={projectId} />
    </div>
  )
}
export default Users
