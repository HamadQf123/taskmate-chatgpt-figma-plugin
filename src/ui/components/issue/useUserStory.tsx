import { useEffect, useState } from "react";
import { useProjectIdContext } from "../../../shared/context/ProjectIdProvider";
import { useGetUserStory, useLazyGetProject } from "../../../shared/service";

const useUserStory = (issue: any) => {
  const {
    loadStory,
    loading: storyloading,
    data: chatAnswer,
    error,
  } = useGetUserStory();
  const { projectId } = useProjectIdContext();
  const { loading, data, getProject } = useLazyGetProject(projectId);

  // User Story
  const [modal, setModal] = useState(false);

  useEffect(() => {
    if (modal && !data && projectId) {
      getProject();
    }
  }, [modal]);

  let childs: any = [];
  interface MyObject {
    name: string;
    child?: MyObject[];
  }

  function printHierarchy(arr: MyObject[], indent = ""): void {
    arr?.forEach((item: MyObject) => {
      childs?.push(`\n ${indent} - ${item?.name}`);

      if (item?.child && item?.child?.length > 0) {
        printHierarchy(item?.child, `${indent}    `);
      }
    });
  }
  const GetUserStory = async () => {
    let children = issue?.children;
    printHierarchy(children);
    const personas = issue?.persona.replace(",", " and ");

    await getProject();

    const message = `Please act as an expert Business Analyst and write a user story as a <persona> persona for the <objects selected> in the project titled <Figma file name>. The user story should include a clear and concise title, detailed description with atleast 3000 words, detailed acceptance criteria with atleast 1500 words, 1 category, 1 subcategory and 1 feature.The user story should be clearly personas oriented.

Please also review the suggested features recorded in the <notes>, and incorporate any relevant information. When writing the user story, please ensure that it follows best practices for effective user stories, such as being detailed, actionable, and testable. The user story should also align with the project description, which is as follows: <project description>, and expand upon the product description based on its knowledge of modern products.
    
In the list below, tags indicated by these symbols "[]", describe the properties, or functionality of the item. Do not include these tags in the item labels, instead view them as item descriptors. 
    
Child objects of the <object selected>. Notice the hierarchy of the objects below. Don't reference the hierarchy directly in the user story but instead describe the function of these objects that the hierarchy suggests and provide breif description for each node in the list:  
${childs}
Parent objects of the <object selected>. Notice the hierarchy of the objects below. Don't reference the hierarchy directly in the user story but instead describe the function of these objects that the hierarchy suggests. :
${issue?.linkedFrame.name}
    
<objects selected> = ${issue?.elementName.name}
    
<Figma File Name> = ${issue?.fileName}
    
<notes> = ${issue?.notes} 

<persona> = ${personas} 

<project description> = ${data?.description}

The Provided Response Should be at least in 2000 words.`;


    loadStory({
      variables: {
        prompt: { message },
      },
    });
  };

  const ExtractDescription = (userStory: String) => {
    const descriptionRegex = /Description:(.*?)Acceptance Criteria:/s;
    const descriptionMatch = userStory.match(descriptionRegex);
    const description = descriptionMatch ? descriptionMatch[1].trim() : '';
    return description;
  };

  const ExtractDefinition = (userStory: String) => {
    const acceptanceCriteriaRegex = /^\d+\.\s(.*)$/gm;
    const acceptanceCriteriaMatches = userStory.matchAll(acceptanceCriteriaRegex);
    const acceptanceCriteria = [];

    for (const match of acceptanceCriteriaMatches) {
      const criterion = match[1].trim();
      acceptanceCriteria.push(criterion);
    }
    return acceptanceCriteria;
  };

  const ExtractCategory = (userStory: String) => {
    const categoryMatch = userStory.match(/Category:\s*(.*)/);
    const category = categoryMatch ? categoryMatch[1].trim() : '';
    return category;
  };

  const ExtractSubCategory = (userStory: String) => {
    const subcategoryMatch = userStory.match(/Subcategory:\s*(.*)/);
    const subcategory = subcategoryMatch ? subcategoryMatch[1].trim() : '';
    return subcategory;
  };

  const ExtractFeature = (userStory: String) => {
    const featureMatch = userStory.match(/Feature:\s*(.*)/);
    const feature = featureMatch ? featureMatch[1].trim() : '';
    return feature;
  };

  return {
    chatGTPLoading: loading,
    chatAnswer: chatAnswer?.completions?.response?.text,
    modal,
    setModal,
    storyloading,
    data: data?.completions?.response?.text,
    ExtractDescription,
    ExtractDefinition,
    ExtractCategory,
    ExtractSubCategory,
    ExtractFeature,
    error,
    GetUserStory,
  };
};

export default useUserStory;
