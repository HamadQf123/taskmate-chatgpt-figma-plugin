import React, { CSSProperties, FC, ReactNode } from "react";
import { useDisclosure } from "@mantine/hooks";
import { Popover, Text, Button, Paper, useMantineTheme } from "@mantine/core";
import { Icon } from "@iconify/react";

interface Props {
  title: string;
  required?: boolean;
  name: string;
  info?: string;
  children: ReactNode;
  childRowStyle?: CSSProperties;
  isLastChild?: boolean;
}
const Row: FC<Props> = ({
  title,
  name,
  required,
  info,
  isLastChild,
  childRowStyle,
  children,
}) => {
  const [opened, { close, open }] = useDisclosure(false);
  const theme = useMantineTheme();

  return (
    <Paper
      className="issue-row"
      style={{
        display: "flex",
      }}
    >
      <Paper
        style={{
          width: "50%",
          border: `1px solid ${theme.colors.dark[4]}`,
          borderBottomWidth: isLastChild ? "1px" : 0,
          borderRightWidth: 0,
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
        }}
        p={"xs"}
        radius={0}
      >
        <label htmlFor={name}>
          <Text>{title}</Text>
        </label>
        <div
          style={{
            display: "flex",
            alignItems: "center",
            gap: "0.7rem",
          }}
        >
          {info && (
            <Popover
              width={200}
              position="bottom"
              withArrow
              shadow="md"
              opened={opened}
            >
              <Popover.Target>
                <span onMouseEnter={open} onMouseLeave={close}>
                  <Icon
                    color="lightgray"
                    className="light"
                    icon={"ph:info-fill"}
                    height={14}
                    width={14}
                  />
                </span>
              </Popover.Target>
              <Popover.Dropdown
                style={{
                  padding: "5px 7px",
                }}
                sx={{ pointerEvents: "none" }}
              >
                <Text size="xs">{info}</Text>
              </Popover.Dropdown>
            </Popover>
          )}
          <Text
            color={"red"}
            className="red"
            dangerouslySetInnerHTML={{
              __html: required
                ? "*"
                : `<span style="color: rgba(0,0,0,0);display: none;" class="transparent">*</span>`,
            }}
          />
        </div>
      </Paper>
      <Paper
        style={{
          width: "50%",
          border: `1px solid ${theme.colors.dark[4]}`,
          borderBottomWidth: isLastChild ? "1px" : 0,
          ...childRowStyle,
        }}
        radius={0}
      >
        {children}
      </Paper>
    </Paper>
  );
};
export default Row;
