import { useGetCategories } from '../service/categories'
import { useProjectIdContext } from '../context/ProjectIdProvider'
import { useEffect, useState } from 'react'

const useCategoryOptions = () => {
  const { projectId } = useProjectIdContext()
 
  const {
    loading: loadingCategories,
    data: categories,
    error: categoriesError,
  } = useGetCategories({
    params: {
      id: parseInt(projectId),
    },
  })


  const [categoryOptions, setCategoryOptions] = useState<
    Array<{ value: string; label: string }>
  >([])

  useEffect(() => {
    if (categories && categories.getCategories.length > 0) {
      setCategoryOptions([]);
      categories.getCategories.forEach(
        (category: { id: string; name: string }) => {
          setCategoryOptions((prev) => [
            ...prev,
            { value: category.id, label: category.name },
          ])
        }
      )
    }
  }, [categories])

  return {
    categoriesError,
    loadingCategories,
    categoryOptions,
  }
}

export default useCategoryOptions
