import React, { useEffect } from 'react'
import Template from './Template'
import { Loader, Stack } from '@mantine/core'
import NewItem from './NewItem'
import {
  useCreateFeature,
  useGetFeatures,
} from './../../../shared/service/features'
import { useProjectIdContext } from '../../../shared/context/ProjectIdProvider'
import { showNotification } from '@mantine/notifications'

const Features = () => {
  const { createFeature, loading, data, error } = useCreateFeature()
  const { projectId } = useProjectIdContext()
  const {
    loading: loadingFeatures,
    data: features,
    error: featuresError,
    refetch: refetchFeatures,
  } = useGetFeatures({
    params: {
      id: parseInt(projectId),
    },
  })

  useEffect(() => {
    data && refetchFeatures()
  }, [data])

  useEffect(() => {
    {
      error &&
        showNotification({
          message: error.message,
          color: 'red',
        })
    }

    {
      data &&
        showNotification({
          message: 'Feature created successfully',
          color: 'green',
        })
    }

    {
      featuresError &&
        showNotification({
          message: featuresError.message,
          color: 'red',
        })
    }
  }, [error, data, featuresError])

  return (
    <Stack spacing={'sm'} align="center" justify="center" w={'100%'}>
      {loadingFeatures && <Loader size={'sm'} />}

      {features && !loadingFeatures && (
        <Template
          items={features?.getFeatures}
          type="Features"
          refetchFeatures={refetchFeatures}
        />
      )}

      {!loadingFeatures && (
        <NewItem
          name="New Feature"
          loading={loading}
          dispatch={(value) => {
            createFeature({
              variables: {
                feature: {
                  name: value,
                  project: projectId,
                },
              },
            })
          }}
        />
      )}
    </Stack>
  )
}
export default Features
